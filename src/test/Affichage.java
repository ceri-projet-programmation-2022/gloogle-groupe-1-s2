package test;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Duration;

public class Affichage extends Application {

	static test.Road route;

	private static Itineraire itineraireTrouvee;

	static Button arriveePopUp ;
	static Button departPopUp ;
	static GraphicsContext gc,gcEau,gcZone,gcBuil,gcParc,gcRoute ;

	private double canvasX;
	private double canvasY;

	static Canvas canvas;
	static Canvas canvasRoute;
	static Canvas canvasParc;
	static Canvas canvasBuil;
	static Canvas canvasZone;
	static Canvas canvasEau;
	static Scene scene;
	static AutoComplete fieldRueDep;
	static AutoComplete fieldRueArr;

	public void start(Stage primaryStage)  throws Exception{
		// on a en gros 2 partie dans la scene inséré dans on UI HBox qui les ordonnent de maniere Horizontale
		// le searchPanel contient tout les elements lié a la recherche
		// le canvas accueille la carte		


		StackPane  root = new StackPane();
		root.getStylesheets().add(getClass().getResource("application.css").toExternalForm());

		canvas = new Canvas();
		canvasRoute = new Canvas();
		canvasParc = new Canvas();
		canvasBuil = new Canvas();
		canvasZone = new Canvas();
		canvasEau = new Canvas();

		departPopUp = new Button("Depart");
		arriveePopUp = new Button("Arrive");

		test.DraggableNode.makeDraggable(arriveePopUp, canvas);
		test.DraggableNode.makeDraggable(departPopUp,canvas);

		departPopUp.setUserData(departPopUp);
		arriveePopUp.setUserData(arriveePopUp);

		VBox searchPanel = new VBox();// VBox place les ui les un en dessous des autre
		HBox villeDepartLayout = new HBox();// HBox place les ui les un a cote des autre
		HBox villeArriveLayout = new HBox();// un HBox par paramettre de la recherche
		searchPanel.setPadding(new Insets(20));/* Le padding */
		searchPanel.setSpacing(10);/* L'�cart entre les cases */
		gc = canvas.getGraphicsContext2D();
		gcRoute = canvasRoute.getGraphicsContext2D();
		gcParc = canvasParc.getGraphicsContext2D();
		gcBuil = canvasBuil.getGraphicsContext2D();
		gcZone = canvasZone.getGraphicsContext2D();
		gcEau = canvasEau.getGraphicsContext2D();


		Label labelVilleRech = new Label("recherche de ville"); 
		// champ de recherche d'une ville 
		AutoComplete fieldVilleRech = new AutoComplete();
		fieldVilleRech.getEntries().addAll(InitAutoCompleteEntries.getCitiesEntries());

		searchPanel.getChildren().add(labelVilleRech);
		searchPanel.getChildren().add(fieldVilleRech);
		/*
	    Label labelPaysVilleRech = new Label("Pays de la ville"); 
	    TextField fieldPaysVilleRech = new TextField(); // champ de recherche d'une ville 
	    root.getChildren().addAll(labelVilleRech,fieldVilleRech,labelPaysVilleRech,fieldPaysVilleRech);
		 */

		Button switchView = new Button("Show/Hide");
		Button zoomIn = new Button(" + ");
		Button zoomOut = new Button(" - ");
		scene = new Scene(root, 900, 650);/*Taille de la fenetre*/

		canvasX = canvas.getHeight();
		canvasY = canvas.getWidth();

		Button rechVilleButton = new Button("rechercher"); // bouton 
		searchPanel.getChildren().add(rechVilleButton);
		rechVilleButton.setOnAction(new EventHandler<ActionEvent>() { // Recuperation de l'action sur un bouton qui execute la methode handle

			// methode appelant la creation de la requete.
			@Override

			public void handle(ActionEvent event) {

				try {

					if(!MyTest.isAlpha(fieldVilleRech.getText())|| MyTest.isBlank(fieldVilleRech.getText())|| !MyTest.exist(fieldVilleRech.getText())) 



						// if(!MyTest.isAlpha(fieldVilleRech.getText())|| MyTest.isBlank(fieldVilleRech.getText())||!MyTest.isAlpha(fieldPaysVilleRech.getText())|| MyTest.isBlank(fieldPaysVilleRech.getText()))



					{
						throw new Exception("le nom de ville est invalide ");
					}
					else
					{
						itineraireTrouvee = null;

						gc.clearRect(0, 0, canvas.getWidth(),canvas.getHeight());
						gcRoute.clearRect(0, 0, canvasRoute.getWidth(),canvasRoute.getHeight());
						gcParc.clearRect(0, 0, canvasParc.getWidth(),canvasParc.getHeight());
						gcBuil.clearRect(0, 0, canvasBuil.getWidth(),canvasBuil.getHeight());
						gcZone.clearRect(0, 0, canvasZone.getWidth(),canvasZone.getHeight());
						gcEau.clearRect(0, 0, canvasEau.getWidth(),canvasEau.getHeight());

						route = new Road(gc,gcRoute,gcParc,gcBuil,gcZone,gcEau,fieldVilleRech,canvas);

						canvas.toFront();
						canvasEau.toFront();
						canvasZone.toFront();
						canvasParc.toFront();
						canvasRoute.toFront();
						canvasBuil.toFront();
						canvasZone.toFront();
						searchPanel.setVisible(false);
						StackPane.setMargin(switchView,new Insets(scene.getHeight()-30,0, 0, scene.getWidth()-80)); // top , right , bottom ,left
						StackPane.setMargin(zoomOut,new Insets(scene.getHeight()-70,0, 20, scene.getWidth()-80)); // top , right , bottom ,left
						StackPane.setMargin(zoomIn,new Insets(scene.getHeight()-140,0, 10, scene.getWidth()-80)); // top , right , bottom ,left
						switchView.toFront();
						zoomOut.toFront();
						zoomIn.toFront();
					}


				}catch(Exception e)
				{
					MyException.erreurSyntaxe(e); 
				}
			}
		});



		//region  recherche itinaire
		Label labelRechercheItineraire = new Label("Recherche d'itineraire : ");
		searchPanel.getChildren().add(labelRechercheItineraire);

		//je crée une hbox
		HBox hbox = new HBox(10);
		searchPanel.getChildren().add(hbox);

		ToggleGroup group = new ToggleGroup();
		RadioButton voiture = new RadioButton("Voiture");
		voiture.setToggleGroup(group);
		voiture.setSelected(true);
		RadioButton velo = new RadioButton("Vélo");
		velo.setToggleGroup(group);
		RadioButton pieton = new RadioButton("Piéton");
		pieton.setToggleGroup(group);
		hbox.getChildren().addAll(voiture,velo,pieton);


		Label labelVilleDep = new Label("Zone de depart");

		fieldRueDep = new AutoComplete();
		fieldRueDep.getEntries().addAll(InitAutoCompleteEntries.getStreetEntries());

		AutoComplete fieldVilleDep = new AutoComplete();
		fieldVilleDep.getEntries().addAll(InitAutoCompleteEntries.getCitiesEntries());

		TextField fieldNumDep = new TextField();
		TextField fieldPaysVilleDep = new TextField(); // champ de recherche d'une ville 
		fieldNumDep.setPromptText("Numéro"); // ecrit dans le textfield la valeur par defaut lorsque le textfield n'est pas selectione
		fieldRueDep.setPromptText("Rue");
		fieldVilleDep.setPromptText("Ville");
		fieldPaysVilleDep.setPromptText("Pays");
		fieldPaysVilleDep.setText("France");
		villeDepartLayout.getChildren().addAll(fieldNumDep,fieldRueDep,fieldVilleDep,fieldPaysVilleDep); // on place les 3 text field dans le layout HBox pour les placer de maniere horizontal
		searchPanel.getChildren().addAll(labelVilleDep,villeDepartLayout); // on place le label et le layout HBox dans le layout VBox pour les placer de maniere vertical

		Label labelVilleArr = new Label("Zone d'arrivee"); //même bloc qu'au dessus

		fieldRueArr = new AutoComplete();
		AutoComplete fieldVilleArr = new AutoComplete();



		fieldRueArr.getEntries().addAll(InitAutoCompleteEntries.getStreetEntries());
		fieldVilleArr.getEntries().addAll(InitAutoCompleteEntries.getCitiesEntries());

		TextField fieldNumArr = new TextField();
		TextField fieldPaysVilleArr = new TextField();
		fieldNumArr.setPromptText("Numéro");
		fieldRueArr.setPromptText("Rue");
		fieldVilleArr.setPromptText("Ville");
		fieldPaysVilleArr.setPromptText("Pays");
		fieldPaysVilleArr.setText("France");
		villeArriveLayout.getChildren().addAll(fieldNumArr,fieldRueArr,fieldVilleArr,fieldPaysVilleArr);
		searchPanel.getChildren().addAll(labelVilleArr,villeArriveLayout);

		//test
		fieldRueArr.setText("Chemin de Lirac");
		fieldRueDep.setText("Chemin du Grillet");
		fieldVilleArr.setText("Roquemaure");
		fieldVilleDep.setText("Roquemaure");
		//


		// bouton popup itineraire

		Tooltip toolTipDepart = new Tooltip("je donne des info sur le depart");
		departPopUp.setTooltip(toolTipDepart);
		departPopUp.setStyle("-fx-background-color: orange; -fx-border-color: grey; -fx-border-radius: 5;");


		Tooltip toolTipArrivee = new Tooltip("je donne des info sur l'arrive");
		arriveePopUp.setTooltip(toolTipArrivee);
		arriveePopUp.setStyle("-fx-background-color: orange; -fx-border-color: grey; -fx-border-radius: 5;");


		root.getChildren().addAll(departPopUp,arriveePopUp);
		departPopUp.setVisible(false);
		arriveePopUp.setVisible(false);

		Button rechItineraireButton = new Button("rechercher de l'itineraire");
		searchPanel.getChildren().add(rechItineraireButton);





		Button arrItineraireButton = new Button("Arreter l'affichage");
		searchPanel.getChildren().add(arrItineraireButton);

		Label labelInfoItineraire = new Label("");
		searchPanel.getChildren().add(labelInfoItineraire);

		ListView<String> listEtapes = new ListView<String>();
		searchPanel.getChildren().add(listEtapes);
		listEtapes.setVisible(false);

		arrItineraireButton.setOnAction(new EventHandler<ActionEvent>() { // Recuperation de l'action sur un bouton qui execute la methode handle
			@Override
			public void handle(ActionEvent event) {
				if (itineraireTrouvee!=null) {

					canvas.setHeight(scene.getHeight());
					canvas.setWidth(scene.getWidth());
					canvasRoute.setHeight(scene.getHeight());
					canvasRoute.setWidth(scene.getWidth());
					canvasParc.setHeight(scene.getHeight());
					canvasParc.setWidth(scene.getWidth());
					canvasBuil.setWidth(scene.getWidth());
					canvasBuil.setHeight(scene.getHeight());
					canvasZone.setWidth(scene.getWidth());
					canvasZone.setHeight(scene.getHeight());
					canvasEau.setWidth(scene.getWidth());
					canvasEau.setHeight(scene.getHeight());
					itineraireTrouvee=null;
					gc.clearRect(0, 0, canvas.getWidth(),canvas.getHeight());
					gcRoute.clearRect(0, 0, canvasRoute.getWidth(),canvasRoute.getHeight());
					gcParc.clearRect(0, 0, canvasParc.getWidth(),canvasParc.getHeight());
					gcBuil.clearRect(0, 0, canvasBuil.getWidth(),canvasBuil.getHeight());
					gcZone.clearRect(0, 0, canvasZone.getWidth(),canvasZone.getHeight());
					gcEau.clearRect(0, 0, canvasEau.getWidth(),canvasEau.getHeight());


					route.resize(gc,gcRoute,gcParc,gcBuil,gcZone,gcEau,canvas);

					departPopUp.setVisible(false);
					arriveePopUp.setVisible(false);
					labelInfoItineraire.setText("");
					listEtapes.setVisible(false);
				}
			}
		});

		rechItineraireButton.setOnAction(new EventHandler<ActionEvent>() { // Recuperation de l'action sur un bouton qui execute la methode handle

			// methode appelant la creation de la requete.
			@Override
			public void handle(ActionEvent event) {
				try {
					if(!MyTest.isValid(fieldNumDep.getText(), fieldRueDep.getText(), fieldVilleDep.getText(),fieldPaysVilleDep.getText()) || !MyTest.isValid(fieldNumArr.getText(), fieldRueArr.getText(), fieldVilleArr.getText(),fieldPaysVilleArr.getText()))
					{
						//System.out.println(fieldNumDep.getText());



						throw new Exception("les informations sont invalide ");
					}
					else
					{
						int moyenDeplacement;
						if( voiture.isSelected()) {
							moyenDeplacement=1;
						}
						else if( velo.isSelected()) {
							moyenDeplacement=2;
						}
						else {
							moyenDeplacement=0;
						}
						// itieraireTrouve possede les coordonnees de l'itineraire 
						Thread threadItineraire = new Thread() {
							public void run() {
								try {
									itineraireTrouvee = RequeteItineraire.itineraire(fieldNumDep.getText(), fieldRueDep.getText(), fieldVilleDep.getText(), fieldPaysVilleDep.getText(), fieldNumArr.getText(), fieldRueArr.getText(), fieldVilleArr.getText(), fieldPaysVilleArr.getText(),moyenDeplacement);

									if (itineraireTrouvee!=null) {

										//itieraireTrouve.affichage();

										Platform.runLater(new Runnable() {
											public void run() {
												gc.clearRect(0, 0, canvas.getWidth(),canvas.getHeight());
												gcRoute.clearRect(0, 0, canvasRoute.getWidth(),canvasRoute.getHeight());
												gcParc.clearRect(0, 0, canvasParc.getWidth(),canvasParc.getHeight());
												gcBuil.clearRect(0, 0, canvasBuil.getWidth(),canvasBuil.getHeight());
												gcZone.clearRect(0, 0, canvasZone.getWidth(),canvasZone.getHeight());
												gcEau.clearRect(0, 0, canvasEau.getWidth(),canvasEau.getHeight());


												route = new Road(gc,gcRoute,gcParc,gcBuil,gcZone,gcEau,fieldVilleDep,fieldVilleArr,itineraireTrouvee,canvas,departPopUp,arriveePopUp);

												canvas.toFront();
												canvasEau.toFront();
												canvasParc.toFront();
												canvasRoute.toFront();
												canvasBuil.toFront();
												canvasZone.toFront();

												searchPanel.setVisible(false);
												StackPane.setMargin(switchView,new Insets(scene.getHeight()-30,0, 0, scene.getWidth()-80)); // top , right , bottom ,left
												StackPane.setMargin(zoomOut,new Insets(scene.getHeight()-70,0, 20, scene.getWidth()-80)); // top , right , bottom ,left
												StackPane.setMargin(zoomIn,new Insets(scene.getHeight()-140,0, 10, scene.getWidth()-80)); // top , right , bottom ,left
												switchView.toFront();
												zoomOut.toFront();
												zoomIn.toFront();
												departPopUp.toFront();
												arriveePopUp.toFront();
												departPopUp.setVisible(true);
												arriveePopUp.setVisible(true);

												labelInfoItineraire.setText("Taille : "+(float)itineraireTrouvee.getTaille()/1000+"km | Durée : "+(float)itineraireTrouvee.getTemps()/60+"m");
												String [] etapesIti = itineraireTrouvee.getEtapes();
												changeTooltipDep(itineraireTrouvee.getSdepart());
												changeTooltipArr(itineraireTrouvee.getSArrivee());
												ObservableList<String> etapes =FXCollections.observableArrayList ();
												listEtapes.setItems(etapes);
												listEtapes.setVisible(true);
												for (int i=0; i<etapesIti.length; i++) {
													if (!etapesIti[i].equals("null")) {
														String label = new String(etapesIti[i]);
														etapes.add(label);
													}
												}
												MyTest.getPixel(root);
											}
										});
									}			
								} catch (Exception e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
						};
						threadItineraire.start();

					}
				}catch(Exception e)
				{
					MyException.erreurSyntaxe(e);   
				}

			}
		});



		//RadioButton fond = new RadioButton("Fond carte");
		RadioButton way = new RadioButton("Routes, rails, ...");
		RadioButton zone = new RadioButton("Zonnes commerciales, parkings");
		RadioButton eau = new RadioButton("Eaux");
		RadioButton parc = new RadioButton("Parcs, forets");
		RadioButton buil = new RadioButton("Batiments");
		//fond.setSelected(true);
		way.setSelected(true);
		zone.setSelected(true);
		eau.setSelected(true);
		parc.setSelected(true);
		buil.setSelected(true);


		searchPanel.getChildren().addAll(/*fond,*/way,buil,zone,eau,parc);

		/*fond.setOnAction(e -> {
			canvas.setVisible(!canvas.isVisible());
		});*/
		way.setOnAction(e -> {
			canvasRoute.setVisible(!canvasRoute.isVisible());
		});
		parc.setOnAction(e -> {
			canvasParc.setVisible(!canvasParc.isVisible());
		});
		buil.setOnAction(e -> {
			canvasBuil.setVisible(!canvasBuil.isVisible());
		});
		zone.setOnAction(e -> {
			canvasZone.setVisible(!canvasZone.isVisible());
		});
		eau.setOnAction(e -> {
			canvasEau.setVisible(!canvasEau.isVisible());
		});




		searchPanel.setPrefWidth(scene.getWidth()/4);
		searchPanel.setPrefHeight(scene.getHeight());

		searchPanel.setStyle("-fx-background-color: Gainsboro");

		canvas.setWidth(scene.getWidth());
		canvas.setHeight(scene.getHeight());
		canvasRoute.setWidth(scene.getWidth());
		canvasRoute.setHeight(scene.getHeight());
		canvasParc.setWidth(scene.getWidth());
		canvasParc.setHeight(scene.getHeight());
		canvasBuil.setWidth(scene.getWidth());
		canvasBuil.setHeight(scene.getHeight());
		canvasZone.setWidth(scene.getWidth());
		canvasZone.setHeight(scene.getHeight());
		canvasEau.setWidth(scene.getWidth());
		canvasEau.setHeight(scene.getHeight());






		root.getChildren().addAll(searchPanel,canvas,canvasRoute,canvasParc,canvasBuil,canvasZone,canvasEau);
		StackPane.setMargin(searchPanel,new Insets(0,scene.getWidth()*3/4, 0, 0)); // top , right , bottom ,left
		searchPanel.toFront();


		root.getChildren().addAll(switchView,zoomIn,zoomOut);
		StackPane.setMargin(switchView,new Insets(scene.getWidth()-30,0, 0, scene.getWidth()-80)); // top , right , bottom ,left
		StackPane.setMargin(zoomOut,new Insets(scene.getWidth()-70,0, 20, scene.getWidth()-80)); // top , right , bottom ,left
		StackPane.setMargin(zoomIn,new Insets(scene.getWidth()-140,0, 10, scene.getWidth()-80)); // top , right , bottom ,left

		// creer un event qui redimensionne les elements prensent dans la scene
		EventHandler<MouseEvent> eventHandler = new EventHandler<MouseEvent>() { 
			@Override 
			public void handle(MouseEvent e) { 
				// System.out.println("Hello World"); 
				searchPanel.setMinWidth(scene.getWidth()/4);
				searchPanel.setMinHeight(scene.getHeight());
				StackPane.setMargin(searchPanel,new Insets(0,scene.getWidth()*3/4, 0, 0)); // top , right , bottom ,left
				StackPane.setMargin(switchView,new Insets(scene.getHeight()-30,0, 0, scene.getWidth()-80)); // top , right , bottom ,left
				StackPane.setMargin(zoomOut,new Insets(scene.getHeight()-70,0, 20, scene.getWidth()-80)); // top , right , bottom ,left
				StackPane.setMargin(zoomIn,new Insets(scene.getHeight()-140,0, 10, scene.getWidth()-80)); // top , right , bottom ,left
				switchView.toFront();
				zoomOut.toFront();
				zoomIn.toFront();
				//System.out.print("searchPanel.getWidth = "+searchPanel.getWidth()+ "\n");
				//System.out.print("scene.getWidth = "+scene.getWidth()+ "\n");
				//System.out.print("scne.getWidth/4 = "+scene.getWidth()/4 + "\n");
				canvas.setHeight(scene.getHeight());
				canvas.setWidth(scene.getWidth());
				canvasRoute.setHeight(scene.getHeight());
				canvasRoute.setWidth(scene.getWidth());
				canvasParc.setHeight(scene.getHeight());
				canvasParc.setWidth(scene.getWidth());
				canvasBuil.setWidth(scene.getWidth());
				canvasBuil.setHeight(scene.getHeight());
				canvasZone.setWidth(scene.getWidth());
				canvasZone.setHeight(scene.getHeight());
				canvasEau.setWidth(scene.getWidth());
				canvasEau.setHeight(scene.getHeight());

				gc.clearRect(0, 0, canvas.getWidth(),canvas.getHeight());
				gcRoute.clearRect(0, 0, canvasRoute.getWidth(),canvasRoute.getHeight());
				gcParc.clearRect(0, 0, canvasParc.getWidth(),canvasParc.getHeight());
				gcBuil.clearRect(0, 0, canvasBuil.getWidth(),canvasBuil.getHeight());
				gcZone.clearRect(0, 0, canvasZone.getWidth(),canvasZone.getHeight());
				gcEau.clearRect(0, 0, canvasEau.getWidth(),canvasEau.getHeight());

				if(route != null)
				{

					if(itineraireTrouvee != null)

					{
						route.resize(gc,gcRoute,gcParc,gcBuil,gcZone,gcEau,canvas,itineraireTrouvee ,departPopUp,arriveePopUp);
						//System.out.println(" depart itineraire : "+itieraireTrouve.getDepart()+" arrivee itineraire : "+ itieraireTrouve.getArrivee());
						departPopUp.setVisible(true);
						arriveePopUp.setVisible(true);
					}
					else
					{
						route.resize(gc,gcRoute,gcParc,gcBuil,gcZone,gcEau,canvas);
					}
				}


			} 
		};   


		//Adding event Filter 
		// execute l'evenement de redimensionnement lorsque la souris entre dans la scene
		scene.addEventFilter(MouseEvent.MOUSE_ENTERED, eventHandler);
		scene.setOnKeyPressed(new EventHandler<KeyEvent>()
		{
			@Override
			public void handle(KeyEvent ke)
			{
				if (ke.getCode().equals(KeyCode.ESCAPE))
				{
					if(searchPanel.isVisible())
					{

						searchPanel.setVisible(false);
						//departPopUp.toFront();
						canvas.setHeight(scene.getHeight());
						canvas.setWidth(scene.getWidth());

						canvasRoute.setHeight(scene.getHeight());
						canvasRoute.setWidth(scene.getWidth());

						canvasParc.setHeight(scene.getHeight());
						canvasParc.setWidth(scene.getWidth());

						canvasBuil.setHeight(scene.getHeight());
						canvasBuil.setWidth(scene.getWidth());

						canvasZone.setHeight(scene.getHeight());
						canvasZone.setWidth(scene.getWidth());

						canvasEau.setHeight(scene.getHeight());
						canvasEau.setWidth(scene.getWidth());

						canvas.toFront();
						canvasEau.toFront();
						canvasParc.toFront();
						canvasRoute.toFront();
						canvasBuil.toFront();
						canvasZone.toFront();


						StackPane.setMargin(switchView,new Insets(scene.getHeight()-30,0, 0, scene.getWidth()-80)); // top , right , bottom ,left
						StackPane.setMargin(zoomOut,new Insets(scene.getHeight()-70,0, 20, scene.getWidth()-80)); // top , right , bottom ,left
						StackPane.setMargin(zoomIn,new Insets(scene.getHeight()-140,0, 10, scene.getWidth()-80)); // top , right , bottom ,left
						switchView.toFront();
						zoomOut.toFront();
						zoomIn.toFront();

						departPopUp.toFront();
						arriveePopUp.toFront();

					}
					else
					{
						searchPanel.setVisible(true);
						searchPanel.setMinWidth(scene.getWidth()/4);
						searchPanel.setMinHeight(scene.getHeight());
						StackPane.setMargin(searchPanel,new Insets(0,scene.getWidth()*3/4, 0, 0)); // top , right , bottom ,left
						searchPanel.toFront();
						StackPane.setMargin(switchView,new Insets(scene.getHeight()-30,0, 0, scene.getWidth()-80)); // top , right , bottom ,left
						StackPane.setMargin(zoomOut,new Insets(scene.getHeight()-70,0, 20, scene.getWidth()-80)); // top , right , bottom ,left
						StackPane.setMargin(zoomIn,new Insets(scene.getHeight()-140,0, 10, scene.getWidth()-80)); // top , right , bottom ,left
						switchView.toFront();
						zoomOut.toFront();
						zoomIn.toFront();

						canvas.setHeight(scene.getHeight());
						canvas.setWidth(scene.getWidth());

						canvasRoute.setHeight(scene.getHeight());
						canvasRoute.setWidth(scene.getWidth());

						canvasParc.setHeight(scene.getHeight());
						canvasParc.setWidth(scene.getWidth());

						canvasBuil.setHeight(scene.getHeight());
						canvasBuil.setWidth(scene.getWidth());

						canvasZone.setHeight(scene.getHeight());
						canvasZone.setWidth(scene.getWidth());

						canvasEau.setHeight(scene.getHeight());
						canvasEau.setWidth(scene.getWidth());

						departPopUp.toFront();
						arriveePopUp.toFront();
					}
				}
			}
		});


		switchView.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent arg0) {
				// TODO Auto-generated method stub
				if(searchPanel.isVisible())
				{

					searchPanel.setVisible(false);

					canvas.setHeight(scene.getHeight());
					canvas.setWidth(scene.getWidth());

					canvasRoute.setHeight(scene.getHeight());
					canvasRoute.setWidth(scene.getWidth());

					canvasParc.setHeight(scene.getHeight());
					canvasParc.setWidth(scene.getWidth());

					canvasBuil.setHeight(scene.getHeight());
					canvasBuil.setWidth(scene.getWidth());

					canvasZone.setHeight(scene.getHeight());
					canvasZone.setWidth(scene.getWidth());

					canvasEau.setHeight(scene.getHeight());
					canvasEau.setWidth(scene.getWidth());

					canvas.toFront();
					canvasEau.toFront();
					canvasParc.toFront();
					canvasRoute.toFront();
					canvasBuil.toFront();
					canvasZone.toFront();


					StackPane.setMargin(switchView,new Insets(scene.getHeight()-30,0, 0, scene.getWidth()-80)); // top , right , bottom ,left
					StackPane.setMargin(zoomOut,new Insets(scene.getHeight()-70,0, 20, scene.getWidth()-80)); // top , right , bottom ,left
					StackPane.setMargin(zoomIn,new Insets(scene.getHeight()-140,0, 10, scene.getWidth()-80)); // top , right , bottom ,left
					switchView.toFront();
					zoomOut.toFront();
					zoomIn.toFront();
					departPopUp.toFront();
					arriveePopUp.toFront();
				}
				else
				{
					searchPanel.setVisible(true);
					searchPanel.setMinWidth(scene.getWidth()/4);
					searchPanel.setMinHeight(scene.getHeight());
					StackPane.setMargin(searchPanel,new Insets(0,scene.getWidth()*3/4, 0, 0)); // top , right , bottom ,left
					searchPanel.toFront();
					StackPane.setMargin(switchView,new Insets(scene.getHeight()-30,0, 0, scene.getWidth()-80)); // top , right , bottom ,left
					StackPane.setMargin(zoomOut,new Insets(scene.getHeight()-70,0, 20, scene.getWidth()-80)); // top , right , bottom ,left
					StackPane.setMargin(zoomIn,new Insets(scene.getHeight()-140,0, 10, scene.getWidth()-80)); // top , right , bottom ,left
					switchView.toFront();
					zoomOut.toFront();
					zoomIn.toFront();

					canvas.setHeight(scene.getHeight());
					canvas.setWidth(scene.getWidth());

					canvasRoute.setHeight(scene.getHeight());
					canvasRoute.setWidth(scene.getWidth());

					canvasParc.setHeight(scene.getHeight());
					canvasParc.setWidth(scene.getWidth());

					canvasBuil.setHeight(scene.getHeight());
					canvasBuil.setWidth(scene.getWidth());

					canvasZone.setHeight(scene.getHeight());
					canvasZone.setWidth(scene.getWidth());

					canvasEau.setHeight(scene.getHeight());
					canvasEau.setWidth(scene.getWidth());
					//departPopUp.toFront();
					//arriveePopUp.toFront();
				}
			}

		});


		//detection par le - sur l'affichage (dezoom)
		zoomOut.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent arg0) {
				// TODO Auto-generated method stub
				float zoom=Road.zoomer(false);
				canvas.setScaleY(zoom);
				canvas.setScaleX(zoom);
				canvasRoute.setScaleY(zoom);
				canvasRoute.setScaleX(zoom);
				canvasBuil.setScaleX(zoom);
				canvasBuil.setScaleY(zoom);
				canvasParc.setScaleX(zoom);
				canvasParc.setScaleY(zoom);
				canvasZone.setScaleX(zoom);
				canvasZone.setScaleY(zoom);
				canvasEau.setScaleX(zoom);
				canvasEau.setScaleY(zoom);

			}

		});


		// detection par le + sur l'affichage (zoom)
		zoomIn.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent arg0) {
				// TODO Auto-generated method stub

				//route.zoomer(gc, true);


				float zoom=Road.zoomer(true);
				if (zoom!=1) {
					departPopUp.setVisible(false);
					arriveePopUp.setVisible(false);
				}
				else {
					departPopUp.setVisible(true);
					arriveePopUp.setVisible(true);
				}
				canvas.setScaleX(zoom);
				canvas.setScaleY(zoom);
				canvasRoute.setScaleX(zoom);
				canvasRoute.setScaleY(zoom);
				canvasBuil.setScaleX(zoom);
				canvasBuil.setScaleY(zoom);
				canvasParc.setScaleX(zoom);
				canvasParc.setScaleY(zoom);
				canvasZone.setScaleX(zoom);
				canvasZone.setScaleY(zoom);
				canvasEau.setScaleX(zoom);
				canvasEau.setScaleY(zoom);


			}

		});

		root.addEventHandler(ScrollEvent.SCROLL,event->{

			double scale = canvasRoute.getScaleY(); 
			double oldScaleY = scale;
			scale = canvasRoute.getScaleX(); 
			double oldScaleX = scale;



			if (event.getDeltaY() < 0)
				scale=Road.zoomer(true);
			else
				scale=Road.zoomer(false);

			//Le zoom max
			if (scale<=128) {

				double fx = (scale / oldScaleX)-1;
				double fy = (scale / oldScaleY)-1;
				//Si zoom normale alors on remet la carte en adéquation avec la canvas
				if (scale==1) {
					canvas.getBoundsInParent().getWidth();
					canvas.getBoundsInParent().getMinX();
					canvas.getBoundsInParent().getHeight();
					canvas.getBoundsInParent().getMinY();


					zoomerCanvas(canvas, scale, canvasX, canvasY);
					zoomerCanvas(canvasRoute, scale, canvasX, canvasY);
					zoomerCanvas(canvasBuil, scale, canvasX, canvasY);
					zoomerCanvas(canvasParc, scale, canvasX, canvasY);
					zoomerCanvas(canvasZone, scale, canvasX, canvasY);
					zoomerCanvas(canvasEau, scale, canvasX, canvasY);
					if (itineraireTrouvee!=null) {
						departPopUp.setVisible(true);
						arriveePopUp.setVisible(true);
						Road.traitsNodesPourItineraire(gc,itineraireTrouvee.getLonLatNodes().length,itineraireTrouvee.getLonLatNodes(), departPopUp, arriveePopUp);
					}
				}
				//Sinon on permet le zoom et on bouge le centre du canvas sur le pointeur de la souris
				else {
					double dx = (event.getSceneX() - (canvas.getBoundsInParent().getWidth()/2 + canvas.getBoundsInParent().getMinX()));
					double dy = (event.getSceneY() - (canvas.getBoundsInParent().getHeight()/2 + canvas.getBoundsInParent().getMinY()));

					zoomerCanvas(canvas, scale, canvas.getTranslateX()-fx*dx, canvas.getTranslateY()-fy*dy);
					zoomerCanvas(canvasRoute, scale, canvasRoute.getTranslateX()-fx*dx, canvasRoute.getTranslateY()-fy*dy);
					zoomerCanvas(canvasBuil, scale, canvasBuil.getTranslateX()-fx*dx, canvasBuil.getTranslateY()-fy*dy);
					zoomerCanvas(canvasParc, scale, canvasParc.getTranslateX()-fx*dx, canvasParc.getTranslateY()-fy*dy);
					zoomerCanvas(canvasZone, scale, canvasZone.getTranslateX()-fx*dx, canvasZone.getTranslateY()-fy*dy);
					zoomerCanvas(canvasEau, scale, canvasEau.getTranslateX()-fx*dx, canvasEau.getTranslateY()-fy*dy);
					departPopUp.setVisible(false);
					arriveePopUp.setVisible(false);

				}
			}


			event.consume();
		});


		//Action clavier touche entrer 



		// gestion ecran de verouillage mdp = ""

		Label labelVer = new Label("Écran verrouillé appuyé pour parler/deverouiler");
		Button enregistrer = new Button("enregistrer");
		enregistrer.setPrefSize(200, 50);
		Image image = new Image("file:./Image/image.jpeg");
		BackgroundImage backgroundImage = new BackgroundImage(image,BackgroundRepeat.REPEAT, BackgroundRepeat.REPEAT, BackgroundPosition.CENTER, BackgroundSize.DEFAULT);
		Background background = new Background(backgroundImage);
		root.setBackground(background);
		labelVer.setTranslateX(0);
		labelVer.setTranslateY(-50);

		enregistrer.setTranslateX(0);
		enregistrer.setTranslateY(100);
		root.getChildren().addAll(labelVer,enregistrer);

		switchView.setVisible(false);
		zoomIn.setVisible(false);
		zoomOut.setVisible(false);
		canvas.setVisible(false);
		searchPanel.setVisible(false);

		labelVer.setVisible(true);

		enregistrer.setVisible(true);



		EventHandler<ActionEvent> login = new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent arg0) {
				// TODO Auto-generated method stub
			
					try {
						URL url = new URL("http://localhost:8000/");
						HttpURLConnection con = (HttpURLConnection) url.openConnection();
						con.setRequestMethod("GET");
						
						BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
						String inputLine;
						StringBuffer content = new StringBuffer();
						while ((inputLine = in.readLine())!=null) {
							content.append(inputLine);
						}
						in.close();
						
						System.out.println(content.toString());
						if (!content.toString().equals("Utilisateur non reconnu")) {

							
							
							
							labelVer.setText(content.toString());
							switchView.setVisible(true);
							zoomIn.setVisible(true);
							zoomOut.setVisible(true);
							canvas.setVisible(true);					
							searchPanel.setVisible(true);
							labelVer.setVisible(false);
						
							enregistrer.setVisible(false);
							Stage popupStage = new Stage();
							popupStage.initModality(Modality.APPLICATION_MODAL);
							popupStage.initOwner(primaryStage);
							popupStage.setTitle("Connexion Réussie");
							
							Button closeButton = new Button("Fermer");
							closeButton.setOnAction(event -> popupStage.close());
							Label label = new Label(content.toString());
							VBox layout = new VBox(10);
				            layout.setPadding(new Insets(10));
				            layout.setAlignment(Pos.CENTER);
				            layout.getChildren().addAll(label,closeButton);

				            Scene popupScene = new Scene(layout, 250, 150);
				            popupStage.setScene(popupScene);

				            popupStage.showAndWait();
						}
						else {
							labelVer.setText(content.toString());
						}
					} catch (MalformedURLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			/*				


				switchView.setVisible(true);
				zoomIn.setVisible(true);
				zoomOut.setVisible(true);
				canvas.setVisible(true);					
				searchPanel.setVisible(true);
				labelVer.setVisible(false);
				enregistrer.setVisible(false);
				*/
				}
					
		};

		enregistrer.setOnAction(login);




		primaryStage.setTitle("GLOOGLE"); /*titre de la fenetre*/
		primaryStage.setScene(scene); 
		primaryStage.show();
		primaryStage.centerOnScreen(); /*la fenetre apparait au milieu de l'�cran*/



		// TODO Auto-generated method stub
		//System.out.print("scene.rootProperty() : "+scene.rootProperty());

	}


	private void zoomerCanvas(Canvas canvas, double scale, double x, double y) {
		canvas.setScaleX( scale);
		canvas.setScaleY( scale);

		canvas.setTranslateX(x);
		canvas.setTranslateY(y);

	}


	static void moveDepArr(double scale,Button  dep,Button arr, float listePix,float listePix2,float listePix3, float listePix4)
	{

		dep.setTranslateX(listePix);
		dep.setTranslateY(listePix2);



		arr.setTranslateX(listePix3);
		arr.setTranslateY(listePix4);

	}

	static void changeTooltipDep(String toolTipDep)
	{
		departPopUp.setTooltip(new Tooltip( toolTipDep));
		//arriveePopUp.setTooltip( new Tooltip( toolTipArr));
		System.out.print(toolTipDep );//+" / "+ toolTipArr);
	}

	static void changeTooltipArr(String toolTipArr)
	{
		//departPopUp.setTooltip(new Tooltip( toolTipDep));
		arriveePopUp.setTooltip( new Tooltip( toolTipArr));
		System.out.print(toolTipArr);
	}

	static void popUpReleased(Double x,Double y,Canvas canvas ,String who, Itineraire it)
	{
		y = (y+(canvas.getHeight()/2));
		x = (x+(canvas.getWidth()/2));
		Double[] a = new Double[2];
		a =Road.foundLonLat(x,y);
		x=a[0];
		y=a[1];
		float newX = x.floatValue();
		float newY = y.floatValue();
		System.out.print("voici les nouvelle Longitude et latitude de "+ who+" lon : "+x+" lat : "+y);
		if(who == "Depart")
		{
			/////////////////////////////////////////////////////////////////////////
			try {

				itineraireTrouvee = RequeteItineraire.RequeteAPiItineraire(  newX,  newY,  it.getArrivee()[0],  it.getArrivee()[1],  1);

				canvas.setHeight(scene.getHeight());
				canvas.setWidth(scene.getWidth());
				canvasRoute.setHeight(scene.getHeight());
				canvasRoute.setWidth(scene.getWidth());
				canvasParc.setHeight(scene.getHeight());
				canvasParc.setWidth(scene.getWidth());
				canvasBuil.setWidth(scene.getWidth());
				canvasBuil.setHeight(scene.getHeight());
				canvasZone.setWidth(scene.getWidth());
				canvasZone.setHeight(scene.getHeight());
				canvasEau.setWidth(scene.getWidth());
				canvasEau.setHeight(scene.getHeight());

				gc.clearRect(0, 0, canvas.getWidth(),canvas.getHeight());
				gcRoute.clearRect(0, 0, canvasRoute.getWidth(),canvasRoute.getHeight());
				gcParc.clearRect(0, 0, canvasParc.getWidth(),canvasParc.getHeight());
				gcBuil.clearRect(0, 0, canvasBuil.getWidth(),canvasBuil.getHeight());
				gcZone.clearRect(0, 0, canvasZone.getWidth(),canvasZone.getHeight());
				gcEau.clearRect(0, 0, canvasEau.getWidth(),canvasEau.getHeight());

				route.resize(gc,gcRoute,gcParc,gcBuil,gcZone,gcEau,canvas,itineraireTrouvee ,departPopUp,arriveePopUp);
				//System.out.println(" depart itineraire : "+itieraireTrouve.getDepart()+" arrivee itineraire : "+ itieraireTrouve.getArrivee());
				departPopUp.setVisible(true);
				arriveePopUp.setVisible(true);

			}


			//				
			//route.traitsNodesPourItineraire(gc, itineraireTrouvee.getLonLatNodes().length, itineraireTrouvee.getLonLatNodes(),  departPopUp, arriveePopUp);

			//System.out.println(" depart itineraire : "+itieraireTrouve.getDepart()+" arrivee itineraire : "+ itieraireTrouve.getArrivee());
			//departPopUp.setVisible(true);
			//arriveePopUp.setVisible(true);
			catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			changeTooltipDep(itineraireTrouvee.getSdepart());
			setNewDepOnSearchPanel(itineraireTrouvee.getSdepart());

		}
		else
		{
			try {

				itineraireTrouvee =RequeteItineraire.RequeteAPiItineraire( it.getDepart()[0], it.getDepart()[1],  newX,  newY,  1);

				canvas.setHeight(scene.getHeight());
				canvas.setWidth(scene.getWidth());
				canvasRoute.setHeight(scene.getHeight());
				canvasRoute.setWidth(scene.getWidth());
				canvasParc.setHeight(scene.getHeight());
				canvasParc.setWidth(scene.getWidth());
				canvasBuil.setWidth(scene.getWidth());
				canvasBuil.setHeight(scene.getHeight());
				canvasZone.setWidth(scene.getWidth());
				canvasZone.setHeight(scene.getHeight());
				canvasEau.setWidth(scene.getWidth());
				canvasEau.setHeight(scene.getHeight());

				gc.clearRect(0, 0, canvas.getWidth(),canvas.getHeight());
				gcRoute.clearRect(0, 0, canvasRoute.getWidth(),canvasRoute.getHeight());
				gcParc.clearRect(0, 0, canvasParc.getWidth(),canvasParc.getHeight());
				gcBuil.clearRect(0, 0, canvasBuil.getWidth(),canvasBuil.getHeight());
				gcZone.clearRect(0, 0, canvasZone.getWidth(),canvasZone.getHeight());
				gcEau.clearRect(0, 0, canvasEau.getWidth(),canvasEau.getHeight());

				route.resize(gc,gcRoute,gcParc,gcBuil,gcZone,gcEau,canvas,itineraireTrouvee ,departPopUp,arriveePopUp);
				//System.out.println(" depart itineraire : "+itieraireTrouve.getDepart()+" arrivee itineraire : "+ itieraireTrouve.getArrivee());
				departPopUp.setVisible(true);
				arriveePopUp.setVisible(true);


				changeTooltipArr(itineraireTrouvee.getSArrivee());
				setNewArrOnSearchPanel(itineraireTrouvee.getSArrivee());
				//route.traitsNodesPourItineraire(gc, itineraireTrouvee.getLonLatNodes().length, itineraireTrouvee.getLonLatNodes(),  departPopUp, arriveePopUp);

				//System.out.println(" depart itineraire : "+itieraireTrouve.getDepart()+" arrivee itineraire : "+ itieraireTrouve.getArrivee());
				//departPopUp.setVisible(true);
				//arriveePopUp.setVisible(true);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	public static void setNewDepOnSearchPanel(String depart)
	{
		fieldRueDep.setText(depart);
	}
	public static void setNewArrOnSearchPanel(String arrivee)
	{
		fieldRueArr.setText(arrivee);
	}

	public static Itineraire getIt() {
		// TODO Auto-generated method stub
		return itineraireTrouvee;
	}

}
