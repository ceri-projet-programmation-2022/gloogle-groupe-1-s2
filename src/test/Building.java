package test;


public class Building {
	//tab 2d de lat/lon de chaque node pour ce Building
	private float[][] lonLatNodes;
	// id du Building
	private String idBuilding;
	
	//true si un batiment commercial
	private boolean commercial;
	
	//true si un batiment restaurant
	private boolean restaurant;
	
	public Building(String id) {
		this.setIdBuilding(id);
	}
	
	//fonction permettant de stocker le nb de node et leur id dans un tableau 
	public void setNodes(float[][] lonLatNodes2) {
		this.lonLatNodes = lonLatNodes2;
	}
	public float[][] getLonLatNodes(){
		return this.lonLatNodes;
	}
	
	public String getIdBuilding() {
		return idBuilding;
	}
	public void setIdBuilding(String idBuilding) {
		this.idBuilding = idBuilding;
	}

	public boolean isRestaurant() {
		return restaurant;
	}

	public void setRestaurant(boolean restaurant) {
		this.restaurant = restaurant;
	}

	public boolean isCommercial() {
		return commercial;
	}

	public void setCommercial(boolean commercial) {
		this.commercial = commercial;
	}
	

}