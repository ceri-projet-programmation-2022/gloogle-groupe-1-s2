package test;

import javafx.application.Platform;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

public class MyException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static void erreurSyntaxe(Exception s)
	{
		Alert alert = new Alert(AlertType.ERROR);
        alert.setTitle("Error");
        alert.setHeaderText("An error has occured");
        alert.setContentText(s.getMessage());

        alert.showAndWait();
	}
	
	public static void erreurPersonnalise(Exception s, String headerMess, String mess)
	{
		Platform.runLater(() -> {
			Alert alert = new Alert(AlertType.ERROR);
	        alert.setTitle("Erreur");
	        alert.setHeaderText(headerMess);
	        alert.setContentText(mess);		
	        
	        alert.showAndWait();

		});

	}
}
