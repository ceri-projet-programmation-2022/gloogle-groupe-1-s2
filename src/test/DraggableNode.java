package test;

import javafx.scene.Node;
import javafx.scene.canvas.Canvas;

public class DraggableNode {
    

    private static double posX;
    private static double posY;
    private static double releasedPosX;
    private static double releasedPosY;
    private static Canvas canvas;


 public static void makeDraggable(Node n,Canvas c)
 {
     canvas =c;
     n.setOnMousePressed(e-> {
         
         posX = e.getSceneX() - n.getTranslateX();
         posY = e.getSceneY() - n.getTranslateY();
         //System.out.print("setOnMousePressed");
     });
     
     
     n.setOnMouseDragged(e -> {
          n.setTranslateX(e.getSceneX() - posX);
          n.setTranslateY(e.getSceneY() - posY);
          
     });
     n.setOnMouseReleased(e->{
        // System.out.print("mouse posX : "+ n.getTranslateX()+" mouse posY : "+n.getTranslateY());//+" id du popup : "+n.getId());
         releasedPosX = n.getTranslateX();
         releasedPosY = n.getTranslateY();
        // System.out.print(" user data "+n.getUserData().toString());
         System.out.print("on relache le bouton");
         System.out.print("userdata"+n.getUserData().toString());
         if(n.getUserData().toString().indexOf("Depart") >0)
         {
             System.out.print("Depart");
             Affichage.popUpReleased(n.getTranslateX(),  n.getTranslateY(),canvas,"Depart", Affichage.getIt());
         }
         else if(n.getUserData().toString().indexOf("Arrive") >0)
         {
             System.out.print("Arrive");
             Affichage.popUpReleased(n.getTranslateX(),  n.getTranslateY(),canvas,"Arrive",Affichage.getIt());
         }
         
     });
 }


public double getReleasedPosX() {
    return releasedPosX;
}


public static double getReleasedPosy() {
    return releasedPosY;
}
 
 
}