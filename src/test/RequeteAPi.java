package test;


import java.lang.String;
import java.util.ArrayList;
import java.util.List;
import java.io.*;
import java.net.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;


public class RequeteAPi {

	RequeteAPi(){}
	Document document = null;
	//fonction de recuperation du carr� � afficher selon la ville 
	public static float[] requeteCarreAffichage(String ville) {
		// cote = tableau contenant toute les max et min lon/lat pour la ville sous forme de flotant.
		float[] cote ;
		cote = new float[4];
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder loader = factory.newDocumentBuilder();

			final Document document = loader.parse(new URL("https://maps.mail.ru/osm/tools/overpass/api/interpreter?data=relation[boundary=administrative][name=\""+ville+"\"][admin_level=8][postal_code];out geom;").openStream());
			//final Document document = loader.parse(new URL("https://maps.mail.ru/osm/tools/overpass/api/interpreter?data=(area[name=France];)->.a;(relation[boundary=administrative][name="+ville+"][admin_level=8](area.a);)->.ville;.ville out geom skel;").openStream());
			final Element racine = document.getDocumentElement();
			//System.out.println(racine.getNodeName());
			final NodeList racineNoeuds = racine.getChildNodes();
			final int nbRacineNoeuds = racineNoeuds.getLength();

			for (int i = 0; i<nbRacineNoeuds; i++) 
			{
				//System.out.println(racineNoeuds.item(i).getNodeName());
				if (racineNoeuds.item(i).getNodeName()=="relation") {
					final Element relation = (Element) racineNoeuds.item(i);
					//System.out.println(relation.getNodeName());
					final Element bounds = (Element) relation.getElementsByTagName("bounds").item(0);





					//System.out.println(bounds.getAttribute("minlat"));
					cote[0] = Float.parseFloat(bounds.getAttribute("minlat"));
					//System.out.println("changement : "+cote[0]);
					//System.out.println(bounds.getAttribute("maxlat"));
					cote[1] = Float.parseFloat(bounds.getAttribute("maxlat"));
					//System.out.println("changement : "+cote[1]);
					//System.out.println(bounds.getAttribute("minlon"));
					cote[2] = Float.parseFloat(bounds.getAttribute("minlon"));
					//System.out.println("changement : "+cote[2]);
					//System.out.println(bounds.getAttribute("maxlon"));
					cote[3] = Float.parseFloat(bounds.getAttribute("maxlon"));
					//System.out.println("changement : "+cote[3]);
					break;
				}
			}




		} catch (ConnectException | UnknownHostException e) {
			MyException.erreurPersonnalise(e, "Problème de connexion", "Vérifiez votre connexion internet");
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			//throw new Exception("Problème lors des chargement des données");
		}
		//System.out.println("affichage des r�sultats avant sortie");

		//System.out.println(cote[0]);
		//System.out.println(cote[1]);
		//System.out.println(cote[2]);
		//System.out.println(cote[3]);
		return cote;


	}


	//foncgtion de r�cuperation des routes contenu dans le carr� � afficher.
	// prend en parametre les lat/lon Max/min du carr�

	public  Way[] requeteRoute(float latMax, float latMin , float lonMax, float lonMin) {



		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder loader = null;
		Way []tabWay = null;
		try {
			/*
			float latMax = (float) 43.917286;
			float latMin = (float) 43.907826;
			float lonMax = (float) 4.892422;
			float lonMin = (float) 4.882251;
			 */
			loader = factory.newDocumentBuilder();


			//document = loader.parse(new URL("https://overpass-api.de/api/interpreter?data=(way(poly: '"+ latMin+" "+lonMin +" "+latMin +" "+lonMax +" "+latMax +" "+lonMax +" "+latMax +" "+lonMin +"')[highway];);out geom body;").openStream());
			//https://maps.mail.ru/osm/tools/overpass/api/interpreter?data=[out:xml][timeout:25];(way(poly: '43.8866433 4.7396309 43.8866433 4.9271468 43.9967419 4.9271468 43.9967419 4.7396309')[highway];);out geom body;
			//https://maps.mail.ru/osm/tools/overpass/api/interpreter?data=[out:xml][timeout:25];(way(poly: '43.886654 4.7396307 43.886654 4.927147 43.996742 4.927147 43.996742 4.7396307')[highway];);out geom body;
			//document = loader.parse(new URL("https://overpass-api.de/api/interpreter?data=(way(poly:%20%27"+latMin+"%20"+lonMin+"%20"+latMin+"%20"+lonMax+"%20"+latMax+"%20"+lonMax+"%20"+latMax+"%20"+lonMin+"%27)[highway];);out%20geom%20body;").openStream());
			document = loader.parse(new URL("https://overpass.kumi.systems/api/interpreter?data=(way(poly:%20%27"+latMin+"%20"+lonMin+"%20"+latMin+"%20"+lonMax+"%20"+latMax+"%20"+lonMax+"%20"+latMax+"%20"+lonMin+"%27)[highway];);out%20geom%20body;").openStream());



			final Element racine  = document.getDocumentElement();

			// 
			final NodeList racineNoeuds = racine.getChildNodes();

			final int nbRacineNoeuds = racineNoeuds.getLength();
			tabWay = new Way[nbRacineNoeuds];

			//k = nombre de way dans le tableau;
			int k=0;

			// passage dans tous les way.
			for(int i=0; i<nbRacineNoeuds;i++) {

				if(racineNoeuds.item(i).getNodeType()==Node.ELEMENT_NODE) {

					final Element way = (Element) racineNoeuds.item(i);
					if(way.getNodeName()=="way") {				
						String idWay  = way.getAttribute("id");
						tabWay[k] = new Way(idWay);

						//r�cuperer tous les tags
						final NodeList tag = way.getElementsByTagName("tag");
						final int nbTag = tag.getLength();

						//r�cuperer tous les nd :
						final NodeList nd = way.getElementsByTagName("nd");
						final int nbNd = nd.getLength();
						float [][] tabNode = new float[nbNd][2];

						//System.out.println("way : ");
						//Affichage de toute les nodes :

						for(int x =0;x<nbNd;x++) {

							final Element node = (Element) nd.item(x);
							/*
							System.out.println("lat : "+node.getAttribute("lat"));
							System.out.println("lon : "+node.getAttribute("lon"));
							 */
							tabNode[x][0] = Float.parseFloat(node.getAttribute("lon")); //recupere la lat de la node
							tabNode[x][1] = Float.parseFloat(node.getAttribute("lat")); // recupere la lon de la node 



						}




						// Affichage de tous les tags :
						String max = null;
						String type = null;
						String nom = null;
						String oneWay = null;
						String junction = null;
						for(int v= 0;v<nbTag;v++) {
							final Element attrTag = (Element) tag.item(v);
							String k11 = attrTag.getAttribute("k");
							if (k11.equals("highway")) {
								type = attrTag.getAttribute("v");
								//System.out.println("type de voie : "+attrTag.getAttribute("v"));
							}
							else if (k11.equals("name")) {
								nom = attrTag.getAttribute("v");
								//System.out.println("nom de la voie : "+attrTag.getAttribute("v"));
							}
							else if (k11.equals("maxspeed")) {
								max = attrTag.getAttribute("v");
								//System.out.println("vitesse max  : "+attrTag.getAttribute("v"));
							}
							else if (k11.equals("oneway")) {
								oneWay = "yes";
								//System.out.println("vitesse max  : "+attrTag.getAttribute("v"));
							}
							else if (k11.equals("junction")) {
								junction = attrTag.getAttribute("v");
								//System.out.println("vitesse max  : "+attrTag.getAttribute("v"));
							}
						}

						//ajout des infos du way
						tabWay[k].setNodes(tabNode);
						tabWay[k].setMaxSpeed(max);
						tabWay[k].setName(nom);
						tabWay[k].setType(type);	
						tabWay[k].setOneWay(oneWay);
						tabWay[k].setJunction(junction);

						k=k+1;
					}		
				}
			}

		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return tabWay;




	}



	//fonction de r�cuperation des coordonnee d'une ville 
	// !!!!!!!!! Pas encore fait le code � l'interieur ne correspond pas.
	public static String getCoordonneeVille(String ville) {
		String coord = null;
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder loader = factory.newDocumentBuilder();
			final Document document = loader.parse(new URL("https://maps.mail.ru/osm/tools/overpass/api/interpreter?data=relation[boundary=administrative][name="+ville+"][admin_level=8];node(r:admin_centre);out body;").openStream());
			final Element racine = document.getDocumentElement();
			//System.out.println(racine.getNodeName());

			final NodeList racineNoeuds = racine.getChildNodes();
			racineNoeuds.getLength();
			int i=0;
			while(racineNoeuds.item(i).getNodeName()!="node") {
				i++;
			}
			if(racineNoeuds.item(i).getNodeType() == Node.ELEMENT_NODE) {
				final Element Node = (Element) racineNoeuds.item(i);
				coord = Node.getAttribute("lat")+","+Node.getAttribute("lon");
				//System.out.println(Node.getNodeName());
				//System.out.println("coord : " + coord);

			}

		}catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}




		return coord;


	}



	public static Way[] requeteParc(float latMax, float latMin , float lonMax, float lonMin) {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder loader = null;
		Way []tabWay = null;

		try {
			loader = factory.newDocumentBuilder();

			final Document document = loader.parse(new URL("https://maps.mail.ru/osm/tools/overpass/api/interpreter?data=way(poly:\""+ latMin+" "+lonMin +" "+latMin +" "+lonMax +" "+latMax +" "+lonMax +" "+latMax +" "+lonMin +"\")[leisure=park];out geom;").openStream());
			//final Document document = loader.parse(new URL("https://overpass-api.de/api/interpreter?data=(way(poly:%20%27"+latMin+"%20"+lonMin+"%20"+latMin+"%20"+lonMax+"%20"+latMax+"%20"+lonMax+"%20"+latMax+"%20"+lonMin+"%27)[leaisure=park];);out%20geom;").openStream());

			final org.w3c.dom.Element racine  = document.getDocumentElement();


			final NodeList racineNoeuds = racine.getChildNodes();
			final int nbRacineNoeuds = racineNoeuds.getLength();
			tabWay = new Way[nbRacineNoeuds];
			// nb de way dans tabWay
			int k=0;
			for(int i=0;i<nbRacineNoeuds;i++) {

				if(racineNoeuds.item(i).getNodeType()==Node.ELEMENT_NODE) {

					final Element way = (Element) racineNoeuds.item(i);
					if(way.getNodeName()=="way") {				
						String idWay  = way.getAttribute("id");
						tabWay[k] = new Way(idWay);

						//r�cuperer tous les nd :
						final NodeList nd = way.getElementsByTagName("nd");
						final int nbNd = nd.getLength();
						float [][] tabNode = new float[nbNd][2];

						//System.out.println("way : ");
						//Affichage de toute les nodes :

						for(int x =0;x<nbNd;x++) {

							final Element node = (Element) nd.item(x);
							//System.out.println("lat : "+node.getAttribute("lat"));
							//System.out.println("lon : "+node.getAttribute("lon"));
							if(!node.getAttribute("lon").isEmpty() && !node.getAttribute("lat").isEmpty()) {
								tabNode[x][0] = Float.parseFloat(node.getAttribute("lon")); //recupere la lat de la node
								tabNode[x][1] = Float.parseFloat(node.getAttribute("lat")); // recupere la lon de la node 
							}

						}
						tabWay[k].setNodes(tabNode);
						k++;
						//System.out.println("-------------------------------------------------------");
					}

				}
			}
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return tabWay;

	}

	//On recupere les parking
	public static Way[] requeteParking(float latMax, float latMin , float lonMax, float lonMin) {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder loader = null;
		Way []tabWay = null;

		try {
			loader = factory.newDocumentBuilder();

			final Document document = loader.parse(new URL("https://maps.mail.ru/osm/tools/overpass/api/interpreter?data=[out:xml][timeout:25];(way[\"amenity\"=\"parking\"]("+latMax+","+lonMax+","+latMin+","+lonMin+"););out geom;>;").openStream());
			//final Document document = loader.parse(new URL("https://overpass-api.de/api/interpreter?data=(way(poly:%20%27"+latMin+"%20"+lonMin+"%20"+latMin+"%20"+lonMax+"%20"+latMax+"%20"+lonMax+"%20"+latMax+"%20"+lonMin+"%27)[leaisure=park];);out%20geom;").openStream());

			final org.w3c.dom.Element racine  = document.getDocumentElement();


			final NodeList racineNoeuds = racine.getChildNodes();
			final int nbRacineNoeuds = racineNoeuds.getLength();
			tabWay = new Way[nbRacineNoeuds];
			// nb de way dans tabWay
			int k=0;
			for(int i=0;i<nbRacineNoeuds;i++) {

				if(racineNoeuds.item(i).getNodeType()==Node.ELEMENT_NODE) {

					final Element way = (Element) racineNoeuds.item(i);
					if(way.getNodeName()=="way") {				
						String idWay  = way.getAttribute("id");
						tabWay[k] = new Way(idWay);

						//r�cuperer tous les nd :
						final NodeList nd = way.getElementsByTagName("nd");
						final int nbNd = nd.getLength();
						float [][] tabNode = new float[nbNd][2];

						//System.out.println("way : ");
						//Affichage de toute les nodes :

						for(int x =0;x<nbNd;x++) {

							final Element node = (Element) nd.item(x);
							//System.out.println("lat : "+node.getAttribute("lat"));
							//System.out.println("lon : "+node.getAttribute("lon"));
							if(!node.getAttribute("lon").isEmpty() && !node.getAttribute("lat").isEmpty()) {
								tabNode[x][0] = Float.parseFloat(node.getAttribute("lon")); //recupere la lat de la node
								tabNode[x][1] = Float.parseFloat(node.getAttribute("lat")); // recupere la lon de la node 
							}

						}
						tabWay[k].setNodes(tabNode);
						k++;
						//System.out.println("-------------------------------------------------------");
					}

				}
			}
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return tabWay;

	}


	//On recupere les Champs
	public static Way[] requeteFarmland(float latMax, float latMin , float lonMax, float lonMin) {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder loader = null;
		Way []tabWay = null;

		try {
			loader = factory.newDocumentBuilder();

			final Document document = loader.parse(new URL("https://maps.mail.ru/osm/tools/overpass/api/interpreter?data=[out:xml][timeout:25];(way[\"landuse\"=\"farmland\"]("+latMax+","+lonMax+","+latMin+","+lonMin+"););out geom;>;").openStream());
			//final Document document = loader.parse(new URL("https://overpass-api.de/api/interpreter?data=(way(poly:%20%27"+latMin+"%20"+lonMin+"%20"+latMin+"%20"+lonMax+"%20"+latMax+"%20"+lonMax+"%20"+latMax+"%20"+lonMin+"%27)[leaisure=park];);out%20geom;").openStream());

			final org.w3c.dom.Element racine  = document.getDocumentElement();


			final NodeList racineNoeuds = racine.getChildNodes();
			final int nbRacineNoeuds = racineNoeuds.getLength();
			tabWay = new Way[nbRacineNoeuds];
			// nb de way dans tabWay
			int k=0;
			for(int i=0;i<nbRacineNoeuds;i++) {

				if(racineNoeuds.item(i).getNodeType()==Node.ELEMENT_NODE) {

					final Element way = (Element) racineNoeuds.item(i);
					if(way.getNodeName()=="way") {				
						String idWay  = way.getAttribute("id");
						tabWay[k] = new Way(idWay);

						//r�cuperer tous les nd :
						final NodeList nd = way.getElementsByTagName("nd");
						final int nbNd = nd.getLength();
						float [][] tabNode = new float[nbNd][2];

						//Affichage de toute les nodes :

						for(int x =0;x<nbNd;x++) {

							final Element node = (Element) nd.item(x);
							if(!node.getAttribute("lon").isEmpty() && !node.getAttribute("lat").isEmpty()) {
								tabNode[x][0] = Float.parseFloat(node.getAttribute("lon")); //recupere la lat de la node
								tabNode[x][1] = Float.parseFloat(node.getAttribute("lat")); // recupere la lon de la node 
							}

						}
						tabWay[k].setNodes(tabNode);
						k++;
					}

				}
			}
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return tabWay;

	}

	//On recupere les zones Brousseuses
	public static Way[] requeteScrub(float latMax, float latMin , float lonMax, float lonMin) {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder loader = null;
		Way []tabWay = null;

		try {
			loader = factory.newDocumentBuilder();

			final Document document = loader.parse(new URL("https://maps.mail.ru/osm/tools/overpass/api/interpreter?data=[out:xml][timeout:25];(way[\"natural\"=\"scrub\"]("+latMax+","+lonMax+","+latMin+","+lonMin+"););out geom;>;").openStream());
			//final Document document = loader.parse(new URL("https://overpass-api.de/api/interpreter?data=(way(poly:%20%27"+latMin+"%20"+lonMin+"%20"+latMin+"%20"+lonMax+"%20"+latMax+"%20"+lonMax+"%20"+latMax+"%20"+lonMin+"%27)[leaisure=park];);out%20geom;").openStream());

			final org.w3c.dom.Element racine  = document.getDocumentElement();


			final NodeList racineNoeuds = racine.getChildNodes();
			final int nbRacineNoeuds = racineNoeuds.getLength();
			tabWay = new Way[nbRacineNoeuds];
			// nb de way dans tabWay
			int k=0;
			for(int i=0;i<nbRacineNoeuds;i++) {

				if(racineNoeuds.item(i).getNodeType()==Node.ELEMENT_NODE) {

					final Element way = (Element) racineNoeuds.item(i);
					if(way.getNodeName()=="way") {				
						String idWay  = way.getAttribute("id");
						tabWay[k] = new Way(idWay);

						//r�cuperer tous les nd :
						final NodeList nd = way.getElementsByTagName("nd");
						final int nbNd = nd.getLength();
						float [][] tabNode = new float[nbNd][2];

						//System.out.println("way : ");
						//Affichage de toute les nodes :

						for(int x =0;x<nbNd;x++) {

							final Element node = (Element) nd.item(x);
							//System.out.println("lat : "+node.getAttribute("lat"));
							//System.out.println("lon : "+node.getAttribute("lon"));
							if(!node.getAttribute("lon").isEmpty() && !node.getAttribute("lat").isEmpty()) {
								tabNode[x][0] = Float.parseFloat(node.getAttribute("lon")); //recupere la lat de la node
								tabNode[x][1] = Float.parseFloat(node.getAttribute("lat")); // recupere la lon de la node 
							}

						}
						tabWay[k].setNodes(tabNode);
						k++;
						//System.out.println("-------------------------------------------------------");
					}

				}
			}
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return tabWay;

	}


	//On recupere les Forets
	public static Way[] requeteForest(float latMax, float latMin , float lonMax, float lonMin) {
		System.out.println("---------------------Parc-----------------------------");
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder loader = null;
		Way []tabWay = null;

		try {
			loader = factory.newDocumentBuilder();

			final Document document = loader.parse(new URL("https://maps.mail.ru/osm/tools/overpass/api/interpreter?data=[out:xml][timeout:25];(way[\"landuse\"=\"forest\"]("+latMax+","+lonMax+","+latMin+","+lonMin+"););out geom;>;").openStream());
			//final Document document = loader.parse(new URL("https://overpass-api.de/api/interpreter?data=(way(poly:%20%27"+latMin+"%20"+lonMin+"%20"+latMin+"%20"+lonMax+"%20"+latMax+"%20"+lonMax+"%20"+latMax+"%20"+lonMin+"%27)[leaisure=park];);out%20geom;").openStream());

			final org.w3c.dom.Element racine  = document.getDocumentElement();


			final NodeList racineNoeuds = racine.getChildNodes();
			final int nbRacineNoeuds = racineNoeuds.getLength();
			tabWay = new Way[nbRacineNoeuds];
			// nb de way dans tabWay
			int k=0;
			for(int i=0;i<nbRacineNoeuds;i++) {

				if(racineNoeuds.item(i).getNodeType()==Node.ELEMENT_NODE) {

					final Element way = (Element) racineNoeuds.item(i);
					if(way.getNodeName()=="way") {				
						String idWay  = way.getAttribute("id");
						tabWay[k] = new Way(idWay);

						//r�cuperer tous les nd :
						final NodeList nd = way.getElementsByTagName("nd");
						final int nbNd = nd.getLength();
						float [][] tabNode = new float[nbNd][2];

						//System.out.println("way : ");
						//Affichage de toute les nodes :

						for(int x =0;x<nbNd;x++) {

							final Element node = (Element) nd.item(x);
							//System.out.println("lat : "+node.getAttribute("lat"));
							//System.out.println("lon : "+node.getAttribute("lon"));
							if(!node.getAttribute("lon").isEmpty() && !node.getAttribute("lat").isEmpty()) {
								tabNode[x][0] = Float.parseFloat(node.getAttribute("lon")); //recupere la lat de la node
								tabNode[x][1] = Float.parseFloat(node.getAttribute("lat")); // recupere la lon de la node 
							}

						}
						tabWay[k].setNodes(tabNode);
						k++;
						//System.out.println("-------------------------------------------------------");
					}

				}
			}
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return tabWay;

	}





	//On recupere les rails
	public static Way[] requeteRail(float latMax, float latMin , float lonMax, float lonMin) {
		System.out.println("---------------------Parc-----------------------------");
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder loader = null;
		Way []tabWay = null;

		try {
			loader = factory.newDocumentBuilder();

			final Document document = loader.parse(new URL("https://maps.mail.ru/osm/tools/overpass/api/interpreter?data=[out:xml][timeout:25];(way[\"railway\"=\"rail\"]("+latMax+","+lonMax+","+latMin+","+lonMin+"););out geom;>;").openStream());
			//final Document document = loader.parse(new URL("https://overpass-api.de/api/interpreter?data=(way(poly:%20%27"+latMin+"%20"+lonMin+"%20"+latMin+"%20"+lonMax+"%20"+latMax+"%20"+lonMax+"%20"+latMax+"%20"+lonMin+"%27)[leaisure=park];);out%20geom;").openStream());

			final org.w3c.dom.Element racine  = document.getDocumentElement();


			final NodeList racineNoeuds = racine.getChildNodes();
			final int nbRacineNoeuds = racineNoeuds.getLength();
			tabWay = new Way[nbRacineNoeuds];
			// nb de way dans tabWay
			int k=0;
			for(int i=0;i<nbRacineNoeuds;i++) {

				if(racineNoeuds.item(i).getNodeType()==Node.ELEMENT_NODE) {

					final Element way = (Element) racineNoeuds.item(i);
					if(way.getNodeName()=="way") {				
						String idWay  = way.getAttribute("id");
						tabWay[k] = new Way(idWay);

						//r�cuperer tous les nd :
						final NodeList nd = way.getElementsByTagName("nd");
						final int nbNd = nd.getLength();
						float [][] tabNode = new float[nbNd][2];

						//System.out.println("way : ");
						//Affichage de toute les nodes :

						for(int x =0;x<nbNd;x++) {

							final Element node = (Element) nd.item(x);
							//System.out.println("lat : "+node.getAttribute("lat"));
							//System.out.println("lon : "+node.getAttribute("lon"));
							if(!node.getAttribute("lon").isEmpty() && !node.getAttribute("lat").isEmpty()) {
								tabNode[x][0] = Float.parseFloat(node.getAttribute("lon")); //recupere la lat de la node
								tabNode[x][1] = Float.parseFloat(node.getAttribute("lat")); // recupere la lon de la node 
							}

						}
						tabWay[k].setNodes(tabNode);
						k++;
						//System.out.println("-------------------------------------------------------");
					}

				}
			}
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return tabWay;

	}

	//On recupere les routes des aéroports
	public static Way[] requeteAero(float latMax, float latMin , float lonMax, float lonMin) {
		System.out.println("---------------------Parc-----------------------------");
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder loader = null;
		Way []tabWay = null;

		try {
			loader = factory.newDocumentBuilder();

			final Document document = loader.parse(new URL("https://maps.mail.ru/osm/tools/overpass/api/interpreter?data=[out:xml][timeout:25];(way[\"aeroway\"=\"runway\"]("+latMax+","+lonMax+","+latMin+","+lonMin+");way[\"aeroway\"=\"taxiway\"]("+latMax+","+lonMax+","+latMin+","+lonMin+"););out geom;>;").openStream());
			//final Document document = loader.parse(new URL("https://overpass-api.de/api/interpreter?data=(way(poly:%20%27"+latMin+"%20"+lonMin+"%20"+latMin+"%20"+lonMax+"%20"+latMax+"%20"+lonMax+"%20"+latMax+"%20"+lonMin+"%27)[leaisure=park];);out%20geom;").openStream());

			final org.w3c.dom.Element racine  = document.getDocumentElement();


			final NodeList racineNoeuds = racine.getChildNodes();
			final int nbRacineNoeuds = racineNoeuds.getLength();
			tabWay = new Way[nbRacineNoeuds];
			// nb de way dans tabWay
			int k=0;
			for(int i=0;i<nbRacineNoeuds;i++) {

				if(racineNoeuds.item(i).getNodeType()==Node.ELEMENT_NODE) {

					final Element way = (Element) racineNoeuds.item(i);
					if(way.getNodeName()=="way") {				
						String idWay  = way.getAttribute("id");
						tabWay[k] = new Way(idWay);

						//r�cuperer tous les nd :
						final NodeList nd = way.getElementsByTagName("nd");
						final int nbNd = nd.getLength();
						float [][] tabNode = new float[nbNd][2];

						//System.out.println("way : ");
						//Affichage de toute les nodes :

						for(int x =0;x<nbNd;x++) {

							final Element node = (Element) nd.item(x);
							//System.out.println("lat : "+node.getAttribute("lat"));
							//System.out.println("lon : "+node.getAttribute("lon"));
							if(!node.getAttribute("lon").isEmpty() && !node.getAttribute("lat").isEmpty()) {
								tabNode[x][0] = Float.parseFloat(node.getAttribute("lon")); //recupere la lat de la node
								tabNode[x][1] = Float.parseFloat(node.getAttribute("lat")); // recupere la lon de la node 
							}

						}
						tabWay[k].setNodes(tabNode);
						k++;
						//System.out.println("-------------------------------------------------------");
					}

				}
			}
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return tabWay;

	}


	public static Way[] requeteZoneCommercial(float latMax, float latMin , float lonMax, float lonMin) {
		System.out.println("---------------------ZoneCommerciale-----------------------------");
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder loader = null;
		Way []tabWay = null;

		try {
			boolean vide =false;
			while(!vide) {
				loader = factory.newDocumentBuilder();
				final Document document = loader.parse(new URL("https://maps.mail.ru/osm/tools/overpass/api/interpreter?data=way(poly:\""+ latMin+" "+lonMin +" "+latMin +" "+lonMax +" "+latMax +" "+lonMax +" "+latMax +" "+lonMin +"\")[landuse=retail];out geom;").openStream());

				//document = loader.parse(new URL("https://overpass-api.de/api/interpreter?data=(way(poly:%20%27"+latMin+"%20"+lonMin+"%20"+latMin+"%20"+lonMax+"%20"+latMax+"%20"+lonMax+"%20"+latMax+"%20"+lonMin+"%27)[landuse=retail];out%20geom;").openStream());
				final Element racine  = document.getDocumentElement();

				// 
				final NodeList racineNoeuds = racine.getChildNodes();
				final int nbRacineNoeuds = racineNoeuds.getLength();
				tabWay = new Way[nbRacineNoeuds];
				// nb de way dans tabWay
				int k=0;
				for(int i=0;i<nbRacineNoeuds;i++) {

					if(racineNoeuds.item(i).getNodeType()==Node.ELEMENT_NODE) {

						final Element way = (Element) racineNoeuds.item(i);
						if(way.getNodeName()=="way") {				
							String idWay  = way.getAttribute("id");
							tabWay[k] = new Way(idWay);

							//r�cuperer tous les nd :
							final NodeList nd = way.getElementsByTagName("nd");
							final int nbNd = nd.getLength();
							float [][] tabNode = new float[nbNd][2];

							//Affichage de toute les nodes :

							for(int x =0;x<nbNd;x++) {

								final Element node = (Element) nd.item(x);
								//System.out.println("lat : "+node.getAttribute("lat").isEmpty());
								//System.out.println("lon : "+node.getAttribute("lon").isEmpty());
								//System.out.println(node.getNodeName());
								if(!node.getAttribute("lat").isEmpty() && !node.getAttribute("lon").isEmpty()) {
									tabNode[x][0] = Float.parseFloat(node.getAttribute("lon")); //recupere la lat de la node
									tabNode[x][1] = Float.parseFloat(node.getAttribute("lat")); // recupere la lon de la node 
									vide=true;
								}
							}
							tabWay[k].setNodes(tabNode);
							k++;

						}

					}
				}
			}
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return tabWay;

	}




	public static Building[] requeteBuilding(float latMax, float latMin , float lonMax, float lonMin) {

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder loader = null;
		try {
			loader = factory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Document document = null;
		try {
			document = loader.parse(new URL("https://maps.mail.ru/osm/tools/overpass/api/interpreter?data=[out:xml][timeout:25];(node[%22building%22=%22yes%22]("+latMin+","+lonMin+","+latMax+","+lonMax+");way[%22building%22=%22yes%22]("+latMin+","+lonMin+","+latMax+","+lonMax+"););out%20geom%20skel;%3E;").openStream());

		} catch (ConnectException e) {
			MyException.erreurPersonnalise(e, "Problème de connexion", "Vérifiez votre connexion internet");
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			//throw new Exception("Problème lors des chargement des données");
		}
		final Element racine = document.getDocumentElement();

		final NodeList racineNoeuds = racine.getChildNodes();
		final int nbRacineNoeuds = racineNoeuds.getLength();
		// tabBuilding = tableau avec toute les routes � afficher.
		Building []tabBuilding = new Building[nbRacineNoeuds];
		int k=0;

		for (int i = 0; i<nbRacineNoeuds; i++) 
		{
			if(racineNoeuds.item(i).getNodeType() == Node.ELEMENT_NODE) 
			{
				final Element Building = (Element) racineNoeuds.item(i);


				//si j'ai un id je peux continuer sinon je passe � l'element suivant.
				if(Building.getNodeName()=="way") {
					String idBuilding = Building.getAttribute("id");// id de la way courante
					//Building route = new Building(idBuilding);//creation de l'objet route de type Building 

					tabBuilding[k] = new Building(idBuilding);
					final NodeList node =  Building.getElementsByTagName("nd"); //recupere les nodes contenue dans le Building 
					final int nbNode = node.getLength();

					// tabNode = tableau � 2 dimensions ex : tabNode[0][0] = lat de la premiere node.  
					float [][]  tabNode = new float[nbNode][2];

					//boucle pour parcourir toute les balises nd de chaque way
					for (int j = 0; j<nbNode; j++) 
					{
						final Element idNode = (Element) node.item(j);
						//  System.out.println(idNode.getNodeName()); //affiche le nom de la balise ici nd
						//System.out.println("ID : " + idNode.getAttribute("ref")); // affiche ce qui est contenu dans l'attribut ref (id) de la balise nd 

						tabNode[j][0] = Float.parseFloat(idNode.getAttribute("lon")); //recupere la lat de la node
						tabNode[j][1] = Float.parseFloat(idNode.getAttribute("lat")); // recupere la lon de la node 

					}

					tabBuilding[k].setNodes(tabNode);//insertion des lat/lon des node au way correspondant.
					//System.out.println("c'est la : "+this.tabWay[i].idWay);

					//r�cuperer tous les tags
					final NodeList tag = Building.getElementsByTagName("tag");
					final int nbTag = tag.getLength();

					boolean commercial=false;
					boolean restaurant=false;
					for(int v= 0;v<nbTag;v++) {
						final Element attrTag = (Element) tag.item(v);
						String k11 = attrTag.getAttribute("k");
						if (k11.equals("shop")) {
							commercial=true;
						}
						else if (k11.equals("name")) {
							if (attrTag.getAttribute("v").equals("retail"));{
								commercial=true;
							}
						}
						else if (k11.equals("amenity")) {
							if (attrTag.getAttribute("v").equals("fast_food") || attrTag.getAttribute("v").equals("restaurant"));{
								restaurant=true;
							}
						}
					}
					//ajout des infos du way
					tabBuilding[k].setRestaurant(restaurant);
					tabBuilding[k].setCommercial(commercial);

					k=k+1;
				}
			}   			   
		}
		return tabBuilding;




	}


	public static Way[] requeteTourVille(String ville) {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder loader = null;
		Way []tabWay = null;
		try {
			loader = factory.newDocumentBuilder();
			Document document = null;
			document = loader.parse(new URL("https://maps.mail.ru/osm/tools/overpass/api/interpreter?data=relation[boundary=administrative][name="+ville+"][admin_level=8]; out geom;").openStream());
			final org.w3c.dom.Element racine  = document.getDocumentElement();


			final NodeList racineNoeuds = racine.getChildNodes();
			final int nbRacineNoeuds = racineNoeuds.getLength();

			int k=0;
			for(int i=0;i<nbRacineNoeuds;i++) {
				if(racineNoeuds.item(i).getNodeType() == Node.ELEMENT_NODE && racineNoeuds.item(i).getNodeName()=="relation") 
				{
					//System.out.println(racineNoeuds.item(i).getNodeName());

					//System.out.println("valeur  de k: "+k);
					final Element relation = (Element) racineNoeuds.item(i);
					final NodeList relationNoeuds = relation.getChildNodes();
					final int nbRelationNoeuds = relationNoeuds.getLength();
					tabWay = new Way[nbRelationNoeuds];
					for (int j=0;j<nbRelationNoeuds;j++) {
						//System.out.println(relationNoeuds.item(i).getAttributes());
						if(relationNoeuds.item(j).getNodeType() == Node.ELEMENT_NODE && relationNoeuds.item(j).getNodeName()=="member") 
						{


							final Element member = (Element) relationNoeuds.item(j);

							if(member.getAttribute("type").equals("way")) {

								final NodeList memberNoeuds = member.getChildNodes();
								String idWay = member.getAttribute("ref");
								tabWay[k] = new Way(idWay);
								final int nbNd  = memberNoeuds.getLength();
								//System.out.println(nbNd);
								//System.out.println(member.getAttribute("type"));
								//System.out.println(memberNoeuds);
								//System.out.println(member.getAttribute("type"));

								float [][] tabNode = new float[nbNd][2];	
								int v;
								for(v=0;v<nbNd;v++) {
									if(memberNoeuds.item(v).getNodeType() == Node.ELEMENT_NODE){
										final Element nd = (Element) memberNoeuds.item(v);
										/*
								    		System.out.println(nd.getNodeName());
											System.out.println("lat : "+nd.getAttribute("lat"));
											System.out.println("lon : "+nd.getAttribute("lon"));
									    	System.out.println("Valeur de V :"+v);
										 */
										tabNode[v][0] = Float.parseFloat(nd.getAttribute("lon")); //recupere la lat de la node
										// System.out.println(tabNode[0][0]);
										tabNode[v][1] = Float.parseFloat(nd.getAttribute("lat")); // recupere la lon de la node 

									}
								}

								for(int c=0;c<v;c++) {
									/*
								    	System.out.println("Valeur de c:"+c);
								    	System.out.println("affichage de tab node : "+tabNode[c][0]);
								    	System.out.println("affichage de tab node : "+tabNode[c][1]);
									 */
								}
								tabWay[k].setNodes(tabNode);
								k=k+1;


							}

						}
					}
				}      
			}

		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return tabWay;
	}









	public static void affichageConsoleBuilding(Building [] Batiments)
	{
		if (Batiments!=null) {
			for (int i=0; (i<Batiments.length) && (Batiments[i]!=null) ; i++) {
				Batiments[i].getLonLatNodes();


				for (int b=0; b<Batiments[i].getLonLatNodes().length; b++) {
					//System.out.println(batiment[b][0]+" "+batiment[b][1]);
				}
			}
			//System.out.println("nombre de batiments recensé(s) : "+j);
		}
	}

	public static List<Relation[]> requeteWater2(double latMax, double latMin , double lonMax, double lonMin) {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder loader = null;
		try {
			loader = factory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Document document = null;
		try {
			document = loader.parse(new URL("https://maps.mail.ru/osm/tools/overpass/api/interpreter?data=[out:xml][timeout:25];(relation[\"natural\"=\"water\"]("+latMin+","+lonMin+","+latMax+","+lonMax+");way[\"natural\"=\"water\"]("+latMin+","+lonMin+","+latMax+","+lonMax+"););out geom;>;out skel qt;").openStream());

		} catch (SAXException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		List<Relation[]> listRel = new ArrayList<Relation[]>();

		final Element racine = document.getDocumentElement();
		final NodeList racineNoeuds = racine.getChildNodes();
		racineNoeuds.getLength();

		int k=0;
		for (int i = 0; i<10000; i++) 
		{
			if(racineNoeuds.item(i)!=null && racineNoeuds.item(i).getNodeType() == Node.ELEMENT_NODE) 
			{
				final Element Riviere = (Element) racineNoeuds.item(i);


				//Pour chaque relation
				if(Riviere.getNodeName()=="relation") {

					final NodeList member =  Riviere.getElementsByTagName("member");  //je prends tout les member
					final int nbMember = member.getLength(); //nombres de member total dans cette relation

					Relation []  tabMember = new Relation[nbMember];

					for (int i1 = 0; i1<nbMember; i1++) 
					{
						final Element idMmeber = (Element) member.item(i1);
						String id = idMmeber.getAttribute("ref"); // recupere l'id
						String role = idMmeber.getAttribute("role"); // recupere le role

						tabMember[i1] = new Relation(id);
						tabMember[i1].setRole(role);

						final NodeList node =  idMmeber.getElementsByTagName("nd"); //recupere les nodes contenue dans le member 
						final int nbNode = node.getLength();

						// tabNode = tableau   2 dimensions ex : tabNode[0][0] = lat de la premiere node.  
						float [][]  tabNode = new float[nbNode][2];

						//boucle pour parcourir toute les balises nd de chaque way
						int j1 =0;
						for (int j = 0; j<nbNode; j++) 
						{
							final Element idNode = (Element) node.item(j);
							//  System.out.println(idNode.getNodeName()); //affiche le nom de la balise ici nd
							//System.out.println("ID : " + idNode.getAttribute("ref")); // affiche ce qui est contenu dans l'attribut ref (id) de la balise nd 

							//si la node n'est pas dans le cadre elle n'est pas prise
							//if ( Double.parseDouble(idNode.getAttribute("lon")) <= lonMax && Double.parseDouble(idNode.getAttribute("lon")) >= lonMin && Double.parseDouble(idNode.getAttribute("lat")) <= latMax && Double.parseDouble(idNode.getAttribute("lat")) >= latMin) {
							tabNode[j1][0] = Float.parseFloat(idNode.getAttribute("lon")); //recupere la lat de la node
							tabNode[j1][1] = Float.parseFloat(idNode.getAttribute("lat")); // recupere la lon de la node 
							j1++;
							//}

						}

						tabMember[i1].setLonLatNodes(tabNode);//insertion des lat/lon des node au way correspondant.
					}
					//System.out.println("Taille "+tabMember.length);
					if (tabMember[0].getLonLatNodes()!=null) {
						listRel.add(tabMember);
						k=k+1;
					}

				}
				else{				
					if(racineNoeuds.item(i).getNodeType()==Node.ELEMENT_NODE) {
						Relation [] w = new Relation[1];

						final Element way = (Element) racineNoeuds.item(i);
						if(way.getNodeName()=="way") {				
							String idWay  = way.getAttribute("id");
							w[0]=new Relation(idWay);
							//r cuperer tous les tags
							final NodeList tag = way.getElementsByTagName("tag");
							tag.getLength();

							//r cuperer tous les nd :
							final NodeList nd = way.getElementsByTagName("nd");
							final int nbNd = nd.getLength();
							float [][] tabNode = new float[nbNd][2];

							//System.out.println("way : ");
							//Affichage de toute les nodes :

							for(int x =0;x<nbNd;x++) {

								final Element node = (Element) nd.item(x);

								//System.out.println("lat : "+node.getAttribute("lat"));
								//System.out.println("lon : "+node.getAttribute("lon"));
								if (!node.getAttribute("lon").isEmpty() && !node.getAttribute("lat").isEmpty()) {
									tabNode[x][0] = Float.parseFloat(node.getAttribute("lon")); //recupere la lat de la node
									tabNode[x][1] = Float.parseFloat(node.getAttribute("lat")); // recupere la lon de la node 
								}


								w[0].setLonLatNodes(tabNode);//insertion des lat/lon des node au way correspondant.
							}
							//System.out.println("Taille "+tabMember.length);
							if (w[0].getLonLatNodes()!=null) {
								listRel.add(w);
								k=k+1;
							}
						}
					}
				}
			}
		}

		return listRel;
	}

	public static List<Relation[]> requeteResidential(double latMax, double latMin , double lonMax, double lonMin) {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder loader = null;
		try {
			loader = factory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Document document = null;
		try {
			document = loader.parse(new URL("https://maps.mail.ru/osm/tools/overpass/api/interpreter?data=[out:xml][timeout:25];(relation[\"landuse\"=\"residential\"]("+latMin+","+lonMin+","+latMax+","+lonMax+");way[\"landuse\"=\"residential\"]("+latMin+","+lonMin+","+latMax+","+lonMax+"););out geom;>;").openStream());

		} catch (SAXException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		List<Relation[]> listRel = new ArrayList<Relation[]>();

		final Element racine = document.getDocumentElement();
		final NodeList racineNoeuds = racine.getChildNodes();
		racineNoeuds.getLength();

		int k=0;
		for (int i = 0; i<10000; i++) 
		{
			if(racineNoeuds.item(i)!=null && racineNoeuds.item(i).getNodeType() == Node.ELEMENT_NODE) 
			{
				final Element Riviere = (Element) racineNoeuds.item(i);


				//Pour chaque relation
				if(Riviere.getNodeName()=="relation") {

					final NodeList member =  Riviere.getElementsByTagName("member");  //je prends tout les member
					final int nbMember = member.getLength(); //nombres de member total dans cette relation

					Relation []  tabMember = new Relation[nbMember];

					for (int i1 = 0; i1<nbMember; i1++) 
					{
						final Element idMmeber = (Element) member.item(i1);
						String id = idMmeber.getAttribute("ref"); // recupere l'id
						String role = idMmeber.getAttribute("role"); // recupere le role

						tabMember[i1] = new Relation(id);
						tabMember[i1].setRole(role);

						final NodeList node =  idMmeber.getElementsByTagName("nd"); //recupere les nodes contenue dans le member 
						final int nbNode = node.getLength();

						// tabNode = tableau   2 dimensions ex : tabNode[0][0] = lat de la premiere node.  
						float [][]  tabNode = new float[nbNode][2];

						//boucle pour parcourir toute les balises nd de chaque way
						int j1 =0;
						for (int j = 0; j<nbNode; j++) 
						{
							final Element idNode = (Element) node.item(j);
							//  System.out.println(idNode.getNodeName()); //affiche le nom de la balise ici nd
							//System.out.println("ID : " + idNode.getAttribute("ref")); // affiche ce qui est contenu dans l'attribut ref (id) de la balise nd 

							//si la node n'est pas dans le cadre elle n'est pas prise
							//if ( Double.parseDouble(idNode.getAttribute("lon")) <= lonMax && Double.parseDouble(idNode.getAttribute("lon")) >= lonMin && Double.parseDouble(idNode.getAttribute("lat")) <= latMax && Double.parseDouble(idNode.getAttribute("lat")) >= latMin) {
							tabNode[j1][0] = Float.parseFloat(idNode.getAttribute("lon")); //recupere la lat de la node
							tabNode[j1][1] = Float.parseFloat(idNode.getAttribute("lat")); // recupere la lon de la node 
							j1++;
							//}

						}

						tabMember[i1].setLonLatNodes(tabNode);//insertion des lat/lon des node au way correspondant.
					}
					//System.out.println("Taille "+tabMember.length);
					if (tabMember[0].getLonLatNodes()!=null) {
						listRel.add(tabMember);
						k=k+1;
					}

				}
				else{				
					if(racineNoeuds.item(i).getNodeType()==Node.ELEMENT_NODE) {
						Relation [] w = new Relation[1];

						final Element way = (Element) racineNoeuds.item(i);
						if(way.getNodeName()=="way") {				
							String idWay  = way.getAttribute("id");
							w[0]=new Relation(idWay);
							//r cuperer tous les tags
							final NodeList tag = way.getElementsByTagName("tag");
							tag.getLength();

							//r cuperer tous les nd :
							final NodeList nd = way.getElementsByTagName("nd");
							final int nbNd = nd.getLength();
							float [][] tabNode = new float[nbNd][2];

							//System.out.println("way : ");
							//Affichage de toute les nodes :

							for(int x =0;x<nbNd;x++) {

								final Element node = (Element) nd.item(x);

								//System.out.println("lat : "+node.getAttribute("lat"));
								//System.out.println("lon : "+node.getAttribute("lon"));
								if (!node.getAttribute("lon").isEmpty() && !node.getAttribute("lat").isEmpty()) {
									tabNode[x][0] = Float.parseFloat(node.getAttribute("lon")); //recupere la lat de la node
									tabNode[x][1] = Float.parseFloat(node.getAttribute("lat")); // recupere la lon de la node 
								}


								w[0].setLonLatNodes(tabNode);//insertion des lat/lon des node au way correspondant.
							}
							//System.out.println("Taille "+tabMember.length);
							if (w[0].getLonLatNodes()!=null) {
								listRel.add(w);
								k=k+1;
							}
						}
					}
				}
			}
		}

		return listRel;
	}


	public static List<Relation[]> requeteIndustrial(double latMax, double latMin , double lonMax, double lonMin) {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder loader = null;
		try {
			loader = factory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Document document = null;
		try {
			document = loader.parse(new URL("https://maps.mail.ru/osm/tools/overpass/api/interpreter?data=[out:xml][timeout:25];(relation[\"landuse\"=\"industrial\"]("+latMin+","+lonMin+","+latMax+","+lonMax+");way[\"landuse\"=\"industrial\"]("+latMin+","+lonMin+","+latMax+","+lonMax+"););out geom;>;").openStream());

		} catch (SAXException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		List<Relation[]> listRel = new ArrayList<Relation[]>();

		final Element racine = document.getDocumentElement();
		final NodeList racineNoeuds = racine.getChildNodes();
		racineNoeuds.getLength();

		int k=0;
		for (int i = 0; i<10000; i++) 
		{
			if(racineNoeuds.item(i)!=null && racineNoeuds.item(i).getNodeType() == Node.ELEMENT_NODE) 
			{
				final Element Riviere = (Element) racineNoeuds.item(i);


				//Pour chaque relation
				if(Riviere.getNodeName()=="relation") {

					final NodeList member =  Riviere.getElementsByTagName("member");  //je prends tout les member
					final int nbMember = member.getLength(); //nombres de member total dans cette relation

					Relation []  tabMember = new Relation[nbMember];

					for (int i1 = 0; i1<nbMember; i1++) 
					{
						final Element idMmeber = (Element) member.item(i1);
						String id = idMmeber.getAttribute("ref"); // recupere l'id
						String role = idMmeber.getAttribute("role"); // recupere le role

						tabMember[i1] = new Relation(id);
						tabMember[i1].setRole(role);

						final NodeList node =  idMmeber.getElementsByTagName("nd"); //recupere les nodes contenue dans le member 
						final int nbNode = node.getLength();

						// tabNode = tableau   2 dimensions ex : tabNode[0][0] = lat de la premiere node.  
						float [][]  tabNode = new float[nbNode][2];

						//boucle pour parcourir toute les balises nd de chaque way
						int j1 =0;
						for (int j = 0; j<nbNode; j++) 
						{
							final Element idNode = (Element) node.item(j);
							//  System.out.println(idNode.getNodeName()); //affiche le nom de la balise ici nd
							//System.out.println("ID : " + idNode.getAttribute("ref")); // affiche ce qui est contenu dans l'attribut ref (id) de la balise nd 

							//si la node n'est pas dans le cadre elle n'est pas prise
							//if ( Double.parseDouble(idNode.getAttribute("lon")) <= lonMax && Double.parseDouble(idNode.getAttribute("lon")) >= lonMin && Double.parseDouble(idNode.getAttribute("lat")) <= latMax && Double.parseDouble(idNode.getAttribute("lat")) >= latMin) {
							tabNode[j1][0] = Float.parseFloat(idNode.getAttribute("lon")); //recupere la lat de la node
							tabNode[j1][1] = Float.parseFloat(idNode.getAttribute("lat")); // recupere la lon de la node 
							j1++;
							//}

						}

						tabMember[i1].setLonLatNodes(tabNode);//insertion des lat/lon des node au way correspondant.
					}
					//System.out.println("Taille "+tabMember.length);
					if (tabMember[0].getLonLatNodes()!=null) {
						listRel.add(tabMember);
						k=k+1;
					}

				}
				else{				
					if(racineNoeuds.item(i).getNodeType()==Node.ELEMENT_NODE) {
						Relation [] w = new Relation[1];

						final Element way = (Element) racineNoeuds.item(i);
						if(way.getNodeName()=="way") {				
							String idWay  = way.getAttribute("id");
							w[0]=new Relation(idWay);
							//r cuperer tous les tags
							final NodeList tag = way.getElementsByTagName("tag");
							tag.getLength();

							//r cuperer tous les nd :
							final NodeList nd = way.getElementsByTagName("nd");
							final int nbNd = nd.getLength();
							float [][] tabNode = new float[nbNd][2];

							//System.out.println("way : ");
							//Affichage de toute les nodes :

							for(int x =0;x<nbNd;x++) {

								final Element node = (Element) nd.item(x);

								//System.out.println("lat : "+node.getAttribute("lat"));
								//System.out.println("lon : "+node.getAttribute("lon"));
								if (!node.getAttribute("lon").isEmpty() && !node.getAttribute("lat").isEmpty()) {
									tabNode[x][0] = Float.parseFloat(node.getAttribute("lon")); //recupere la lat de la node
									tabNode[x][1] = Float.parseFloat(node.getAttribute("lat")); // recupere la lon de la node 
								}


								w[0].setLonLatNodes(tabNode);//insertion des lat/lon des node au way correspondant.
							}
							//System.out.println("Taille "+tabMember.length);
							if (w[0].getLonLatNodes()!=null) {
								listRel.add(w);
								k=k+1;
							}
						}
					}
				}
			}
		}

		return listRel;
	}


	public static float[] RequeteAPiItineraire(String depart, String destination) {
		float[] ville1 = requeteCarreAffichage(depart);
		float[] ville2 = requeteCarreAffichage(destination);
		float[] plusPetit = new float[4];
		if(ville1[0]>=ville2[0]) {
			//System.out.println("ville1[] :"+ville1[0]+"ville2[3] :"+ville2[0]);
			plusPetit[0]=ville2[0];
		}
		if (ville1[0]<ville2[0]){
			//System.out.println("ville1[3] :"+ville1[0]+"ville2[3] :"+ville2[0]);
			plusPetit[0]=ville1[0];
		}


		if (ville1[1]>=ville2[1]) {
			//System.out.println("ville1[3] :"+ville1[1]+"ville2[3] :"+ville2[1]);
			plusPetit[1]=ville1[1];
		}
		if (ville1[1]<ville2[0]){
			//System.out.println("ville1[3] :"+ville1[1]+"ville2[3] :"+ville2[1]);
			plusPetit[1]=ville2[1];
		}


		if (ville1[2]<ville2[2]){
			//System.out.println("ville1[3] :"+ville1[2]+"ville2[3] :"+ville2[2]);
			plusPetit[2]=ville1[2];
		}
		if (ville1[2]>=ville2[2]){
			//System.out.println("ville1[3] :"+ville1[2]+"ville2[3] :"+ville2[2]);
			plusPetit[2]=ville2[2];
		}


		if (ville1[3]<ville2[3]){
			//System.out.println("ville1[3] :"+ville1[3]+"ville2[3] :"+ville2[3]);
			plusPetit[3]=ville2[3];
		}
		if (ville1[3]>=ville2[3]){
			//System.out.println("ville1[3] :"+ville1[3]+"ville2[3] :"+ville2[3]);
			plusPetit[3]=ville1[3];
		}


		for(int i =0;i<4;i++) {
			//System.out.println(plusPetit[i]);
		}


		//requeteRoute(plusPetit[0],plusPetit[1],plusPetit[2],plusPetit[3]);



		return plusPetit;
	}


	public  Way[] requeteRouteReDraw(float latMax, float latMin , float lonMax, float lonMin) {



		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		Way []tabWay = null;
		try {
			factory.newDocumentBuilder();

			//document = loader.parse(new URL("https://maps.mail.ru/osm/tools/overpass/api/interpreter?data=way(poly:\""+ latMin+" "+lonMin +" "+latMin +" "+lonMax +" "+latMax +" "+lonMax +" "+latMax +" "+lonMin +"\")[highway];out geom body;").openStream());
			final org.w3c.dom.Element racine  = document.getDocumentElement();

			// 
			final NodeList racineNoeuds = racine.getChildNodes();

			final int nbRacineNoeuds = racineNoeuds.getLength();
			tabWay = new Way[nbRacineNoeuds];

			//k = nombre de way dans le tableau;
			int k=0;

			// passage dans tous les way.
			for(int i=0; i<nbRacineNoeuds;i++) {

				if(racineNoeuds.item(i).getNodeType()==Node.ELEMENT_NODE) {

					final Element way = (Element) racineNoeuds.item(i);
					if(way.getNodeName()=="way") {				
						String idWay  = way.getAttribute("id");
						tabWay[k] = new Way(idWay);

						//r�cuperer tous les tags
						final NodeList tag = way.getElementsByTagName("tag");
						final int nbTag = tag.getLength();

						//r�cuperer tous les nd :
						final NodeList nd = way.getElementsByTagName("nd");
						final int nbNd = nd.getLength();
						float [][] tabNode = new float[nbNd][2];

						//System.out.println("way : ");
						//Affichage de toute les nodes :

						for(int x =0;x<nbNd;x++) {

							final Element node = (Element) nd.item(x);
							//System.out.println("lat : "+node.getAttribute("lat"));
							//System.out.println("lon : "+node.getAttribute("lon"));
							tabNode[x][0] = Float.parseFloat(node.getAttribute("lon")); //recupere la lat de la node
							tabNode[x][1] = Float.parseFloat(node.getAttribute("lat")); // recupere la lon de la node 
						}




						// Affichage de tous les tags :
						String max = null;
						String type = null;
						String nom = null;
						String oneWay = null;
						for(int v= 0;v<nbTag;v++) {
							final Element attrTag = (Element) tag.item(v);
							String k11 = attrTag.getAttribute("k");
							if (k11.equals("highway")) {
								type = attrTag.getAttribute("v");
								//System.out.println("type de voie : "+attrTag.getAttribute("v"));
							}
							else if (k11.equals("name")) {
								nom = attrTag.getAttribute("v");
								//System.out.println("nom de la voie : "+attrTag.getAttribute("v"));
							}
							else if (k11.equals("maxspeed")) {
								max = attrTag.getAttribute("v");
								//System.out.println("vitesse max  : "+attrTag.getAttribute("v"));
							}
							else if (k11.equals("oneway")) {
								oneWay = "yes";
								//System.out.println("vitesse max  : "+attrTag.getAttribute("v"));
							}
						}
						//ajout des infos du way
						tabWay[k].setNodes(tabNode);
						tabWay[k].setMaxSpeed(max);
						tabWay[k].setName(nom);
						tabWay[k].setType(type);	
						tabWay[k].setOneWay(oneWay);	
						k=k+1;
					}		
				}
			}

		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return tabWay;


	}




}
