package test;

public class Relation {
	
	//son id
	private String id;
	
	//"inner" = Frontiere interieur  |  "outer" = delimitation exterieur
	private String role;
	
	//coordonnees pour tracer
	private float[][] lonLatNodes;

	public Relation(String id) {
		this.id = id;
	}
	
	public float[][] getLonLatNodes() {
		return lonLatNodes;
	}

	public void setLonLatNodes(float[][] lonLatNodes) {
		this.lonLatNodes = lonLatNodes;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public void afficher() {
		System.out.println("Membre : "+id+" role = "+role+" Taille : "+ lonLatNodes.length);
		for (int i=0; i<lonLatNodes.length; i++) {
			System.out.println(lonLatNodes[i][1]+", "+lonLatNodes[i][0]);
		}
	}
	
}
