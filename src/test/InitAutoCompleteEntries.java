package test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;

public class InitAutoCompleteEntries {
	
	
	public static Collection<String> getStreetEntries()
	{
		Collection<String> a =  new ArrayList<String>() ;
		try
	    {
	      // Le fichier d'entrée
	      File file = new File("./rueAvignon.txt");    
	      // Créer l'objet File Reader
	      FileReader fr = new FileReader(file);  
	      // Créer l'objet BufferedReader        
	      BufferedReader br = new BufferedReader(fr);      
	      String line;
	      while((line = br.readLine()) != null)
	      {
	        
	        a.add(line);         
	      }
	      fr.close();    
	       
	    }
	    catch(IOException e)
	    {
	      e.printStackTrace();
	    }
	  
		return a;
		
	}
	
	public static Collection<String> getCitiesEntries()
	{
		Collection<String> a =  new ArrayList<String>() ;
		Charset utf8 = StandardCharsets.UTF_8;
		try
	    {
	      // Le fichier d'entrée
	      Path pathFile = Paths.get("./Cities.txt");    
	      // Créer l'objet File Reader
	     
	      // Créer l'objet BufferedReader        
	      BufferedReader br = Files.newBufferedReader( pathFile,utf8);      
	      String line;
	      while((line = br.readLine()) != null)
	      {
	        
	        a.add(line);         
	      }
	     // fr.close();    
	       
	    }
	    catch(IOException e)
	    {
	      e.printStackTrace();
	    }
	  
		return a;
		
	}

}
