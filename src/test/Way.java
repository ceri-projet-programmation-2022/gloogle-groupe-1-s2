package test;

public class Way {
	//tab 2d de lat/lon de chaque node pour ce way
	private float[][] lonLatNodes;
	// id du Way
	private String idWay;
	//nb total de Way pour la requete 
	private static int nbWay;
	
	private  String maxSpeed;
	
	private  String name;
	
	private  String type;
	
	private  String oneWay;
	
	private  String junction;
	
	public Way(String id) {
		this.setIdWay(id);
		setNbWay(getNbWay() + 1);
		//this.typeRoute = type;
	}
	
	
	//fonction permettant de stocker le nb de node et leur id dans un tableau 
	public void setNodes(float[][] tabNode) {
		this.lonLatNodes = tabNode.clone();	
	}
	
	public float[][] getLonLatNodes(){
		return this.lonLatNodes;
	}


	public String getIdWay() {
		return idWay;
	}


	public void setIdWay(String idWay) {
		this.idWay = idWay;
	}


	public static int getNbWay() {
		return nbWay;
	}


	public static void setNbWay(int nbWay) {
		Way.nbWay = nbWay;
	}


	public  String getMaxSpeed() {
		return maxSpeed;
	}


	public void setMaxSpeed(String maxSpeed) {
		this.maxSpeed = maxSpeed;
	}


	public String getName() {
		return name;
	}


	public  void setName(String name) {
		this.name = name;
	}


	public String getType() {
		return type;
	}


	public void setType(String type) {
		this.type = type;
	}


	public String getOneWay() {
		return oneWay;
	}


	public void setOneWay(String oneWay) {
		this.oneWay = oneWay;
	}


	public String getJunction() {
		return junction;
	}


	public void setJunction(String junction) {
		this.junction = junction;
	}
	
	
}