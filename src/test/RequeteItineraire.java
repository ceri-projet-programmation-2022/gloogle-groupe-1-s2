package test;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class RequeteItineraire {
	private static
		boolean trouve;
		static float [] coordoonneesDelimitaionZoneRecherche;
	private static RequeteAPi requeteApi = new RequeteAPi();
	private static int[] algorithmeDijkstra(float [][] tabItineraire,int total_nodes, int debut, int arrivee, float [][] tabNoeuds, int modeTransport) 
	{
		//initialisation des listes
		float [] d = new float[total_nodes];
		int [] pred = new int[total_nodes];
		int [] Q = new int[total_nodes];
		for (int i=0; i<total_nodes; i++) {
			d[i] = 9999;//liste des distances les plus courtes entre chaque points (au debut vu que pas encore calcule mis sur infini=9999)
			Q[i] = 1 ;// si le trajet jusqu'au point reste a trouve (1 = oui)
			pred[i] = -1;//le predecesseur du point
			
		}
		d[debut] = 0;//la distancce minimale sur un meme noeud est 0
		Q[debut] = 0;//il est visite donc passe de 1 a 0
		for (int i=0; i<total_nodes; i++) {
			d[i] = recherche(tabItineraire, debut, i, tabNoeuds, modeTransport);
			pred[i] = debut;
		}
		pred[debut]=debut;//c'est lui meme
		int s1 = debut;
		while (Qvide(Q,arrivee) != true) { // tant que tout les noeuds jusqu'a notre noeud voulu n'ont pas ete traite, on continu
			s1 = min(Q, d);// on choisit le prochain noeud a traite
			if(trouve==true) {// si false cela veut dire que le noud n'est pas atteignable
				Q[s1] = 0;//vu que traite on passe a 0
				for (int s2=0; s2<total_nodes; s2++) {
					if (Q[s2] == 1) {
						maj_distances(s1,s2,tabItineraire,pred,d, tabNoeuds, modeTransport);
					}
				}
			}
			else {
				Q[s1]=0;// on sort le noeud non atteigneble de la boucle
				pred[s1]=-1;// il n'a pas de preedecesseur vu qu'il est inateignable
			}
		}
		int [] res = resultat( debut, arrivee, pred, total_nodes, d);//on revoie les points predecesseur de notre noeud de telle sorte a ce que ces predecesseur represente le plus court chemin
		return res;
	}
	
	//cherche la distance minimale avec le noeud pas encore traite
	private static int min(int [] Q, float [] d) {
		trouve = false;
		float mini = 9999;// infini
		int sommet = -1;
		int sommetProbleme = -1;
		for (int s=0; s<Q.length; s++) {
			if (Q[s]==1) {
				if (d[s]<mini) {
					mini = d[s];
					sommet = s;
					trouve = true;
				}
				sommetProbleme = s;
			}
		}
		if (sommet == -1) {return sommetProbleme;}
		return sommet;
	}
	
	//met a jour les distances
	private static void maj_distances(int s1,int s2, float [][] tabItineraire, int [] pred, float [] d, float [][] tabNoeuds, int modeTransport)
	{
		if (d[s2] > (d[s1] + recherche(tabItineraire, s1, s2, tabNoeuds, modeTransport))) {
			d[s2] = (d[s1] + recherche(tabItineraire, s1, s2, tabNoeuds, modeTransport));
			pred[s2] = s1;
		}
	}
	
	//permet de verifier si tout les noeuds ont bien ete faits
	private static boolean Qvide(int [] Q, int arrivee)
	{
		if (Q[arrivee]==0) {return true;}
		for (int i=0; i<Q.length; i++) {
			if (Q[i]==1) {return false;}
		}
		return true;
	}
	
	//met en forme les donnees pour pouvoir les retourner
	private static int [] resultat( int debut, int trajet, int [] pred, int total_nodes, float [] d)
	{
		int [] res = new int[total_nodes];
		for (int i=0; i<total_nodes; i++) {
			res[i]=-1;	 
		}
		int j = 0;
		reResultat(trajet,debut, pred, j, res);
		return res;
	}
	private static int reResultat(int N, int debut, int [] pred, int j, int [] res) {
		if (N == debut) {
			res[j]=N;
		}
		else {
			if (N != -1) {
				j = reResultat(pred[N],debut, pred, j, res);
				res[j]=N;
			}
			else {
				res[j]=-1;
			}
		}
		j = j+1;
		return j;
	}

	
	
	private static Way[] retourRueCoordoonnees(float VilleLatMaxDepart, float VilleLatMinDepart, float VilleLonMaxDepart, float VilleLonMinDepart, String nomRue)
	{
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder loader = null;
		Way []tabWay = null;
		try {
			
			loader = factory.newDocumentBuilder();
			Document document = null;
			document = loader.parse(new URL("https://maps.mail.ru/osm/tools/overpass/api/interpreter?data=[out:xml][timeout:25];(way[\"name\"=\""+nomRue+"\"]("+VilleLatMinDepart+","+VilleLonMinDepart+","+VilleLatMaxDepart+","+VilleLonMaxDepart+"););out%20geom%20skel;%3E;").openStream());
			final org.w3c.dom.Element racine  = document.getDocumentElement();
			
			// 
			final NodeList racineNoeuds = racine.getChildNodes();
			
			final int nbRacineNoeuds = racineNoeuds.getLength();
			tabWay = new Way[nbRacineNoeuds];
			
			//k = nombre de way dans le tableau;
			int k=0;
			
			// passage dans tous les way.
			for(int i=0; i<nbRacineNoeuds;i++) {
				
				if(racineNoeuds.item(i).getNodeType()==Node.ELEMENT_NODE) {
					
					final Element way = (Element) racineNoeuds.item(i);
					if(way.getNodeName()=="way") {				
						String idWay  = way.getAttribute("id");
						tabWay[k] = new Way(idWay);
						
						//r�cuperer tous les tags
						final NodeList tag = way.getElementsByTagName("tag");
						final int nbTag = tag.getLength();
						
						//r�cuperer tous les nd :
						final NodeList nd = way.getElementsByTagName("nd");
						final int nbNd = nd.getLength();
						float [][] tabNode = new float[nbNd][2];
						
						//System.out.println("way : ");
						//Affichage de toute les nodes :
						
						for(int x =0;x<nbNd;x++) {
							
							final Element node = (Element) nd.item(x);
							//System.out.println("lat : "+node.getAttribute("lat"));
							//System.out.println("lon : "+node.getAttribute("lon"));
					    	 tabNode[x][0] = Float.parseFloat(node.getAttribute("lon")); //recupere la lat de la node
					    	 tabNode[x][1] = Float.parseFloat(node.getAttribute("lat")); // recupere la lon de la node 
						}
						
						
						
						
						// Affichage de tous les tags :
						String max = null;
						String type = null;
						String nom = null;
						String oneWay = null;
						for(int v= 0;v<nbTag;v++) {
							final Element attrTag = (Element) tag.item(v);
							String k11 = attrTag.getAttribute("k");
							if (k11.equals("highway")) {
								type = attrTag.getAttribute("v");
								//System.out.println("type de voie : "+attrTag.getAttribute("v"));
							}
							else if (k11.equals("name")) {
								nom = attrTag.getAttribute("v");
								//System.out.println("nom de la voie : "+attrTag.getAttribute("v"));
							}
							else if (k11.equals("maxspeed")) {
								max = attrTag.getAttribute("v");
								//System.out.println("vitesse max  : "+attrTag.getAttribute("v"));
							}
							else if (k11.equals("oneway")) {
								oneWay = "yes";
								//System.out.println("vitesse max  : "+attrTag.getAttribute("v"));
							}
						}
						//ajout des infos du way
						tabWay[k].setNodes(tabNode);
						tabWay[k].setMaxSpeed(max);
						tabWay[k].setName(nom);
						tabWay[k].setType(type);	
						tabWay[k].setOneWay(oneWay);	
						k=k+1;
					}		
				}
			}
			
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return tabWay;
		 
	}
	
	private static NodeNumStreet retourNumRueCoordoonnees(float VilleLatMaxDepart, float VilleLatMinDepart, float VilleLonMaxDepart, float VilleLonMinDepart, String nomRue, String num)
	{
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder loader = null;
		try {
			
			loader = factory.newDocumentBuilder();
			Document document = null;
			document = loader.parse(new URL("https://maps.mail.ru/osm/tools/overpass/api/interpreter?data=[out:xml][timeout:25];(node[\"addr:street\"=\""+nomRue+"\"]("+VilleLatMinDepart+","+VilleLonMinDepart+","+VilleLatMaxDepart+","+VilleLonMaxDepart+"););out body;out geom;>;out skel qt;").openStream());
			final org.w3c.dom.Element racine  = document.getDocumentElement();
			
			// 
			final NodeList racineNoeuds = racine.getChildNodes();
			
			final int nbRacineNoeuds = racineNoeuds.getLength();
			NodeNumStreet [] tabNoeuds = new  NodeNumStreet[nbRacineNoeuds];
			
			//k = nombre de way dans le tableau;
			int k=0;
			
			// passage dans tous les way.
			for(int i=0; i<nbRacineNoeuds;i++) {
				
				if(racineNoeuds.item(i).getNodeType()==Node.ELEMENT_NODE) {
					
					final Element node = (Element) racineNoeuds.item(i);
					if(node.getNodeName()=="node") {
						
						tabNoeuds[k]  = new NodeNumStreet();
						
						//r�cuperer tous les tags
						final NodeList tag = node.getElementsByTagName("tag");
						final int nbTag = tag.getLength();
						
						float [] coor = new float[2];
						coor[0] = Float.parseFloat(node.getAttribute("lon")); //recupere la lat de la node
						coor[1] = Float.parseFloat(node.getAttribute("lat")); // recupere la lon de la node 
						tabNoeuds[k].setLonLatNodes(coor);
						
						// Affichage de tous les tags :
						String num1 = null;
						String rue1 = null;
						for(int v= 0;v<nbTag;v++) {
							final Element attrTag = (Element) tag.item(v);
							String k11 = attrTag.getAttribute("k");
							if (k11.equals("addr:housenumber")) {
								num1 = attrTag.getAttribute("v");
								//System.out.println("type de voie : "+attrTag.getAttribute("v"));
							}
							else if (k11.equals("addr:street")) {
								rue1 = attrTag.getAttribute("v");
								//System.out.println("nom de la voie : "+attrTag.getAttribute("v"));
							}
						}
						//ajout des infos du way
						tabNoeuds[k].setNum(num1);
						tabNoeuds[k].setRue(rue1);
						k=k+1;
					}		
				}
			}
			for (int i=0; i<tabNoeuds.length; i++ ) {
				if ( tabNoeuds[i].getNum().equals(num) ) {
					return tabNoeuds[i];
				}
			}				 
			
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
		 
	}
	
	//permet transformer les informations en coordonnees
	private static float [][] recuperatioonCoordonneesArriveeDepart(String numDepart, String numArrivee, String nomRueDepart,  String nomRueArrivee, String VilleDepart, String VilleArrivee) throws Exception
	{
		float [][] LonLatRueDepartArrivee = new float[2][2];
		
		//coordonnees de la ville de depart
		float [] Coordoonnees = RequeteAPi.requeteCarreAffichage(VilleDepart);
		float VilleLatMaxDepart = Coordoonnees[1];
		float VilleLatMinDepart = Coordoonnees[0];
		float VilleLonMaxDepart = Coordoonnees[3];
		float VilleLonMinDepart = Coordoonnees[2];
		
		//si pas de numero pour le depart
		if ( numDepart.equals("") ) {
			Way[] way = retourRueCoordoonnees(VilleLatMaxDepart,VilleLatMinDepart,VilleLonMaxDepart,VilleLonMinDepart,nomRueDepart);
			float [][] LonLatNodes = way[0].getLonLatNodes();
			LonLatRueDepartArrivee [0][0] = LonLatNodes[0][0];
			LonLatRueDepartArrivee [0][1] = LonLatNodes[0][1];
		}
		//si numero pour le depart
		else {
			NodeNumStreet NodeNumStreet = retourNumRueCoordoonnees(VilleLatMaxDepart,VilleLatMinDepart,VilleLonMaxDepart,VilleLonMinDepart,nomRueDepart,numDepart);
			if (NodeNumStreet==null) {
				MyException.erreurPersonnalise(null,"Erreur, Probeleme avec le numero.","");
				throw new Exception("Probeleme avec le numero.");
			}
			float [] LonLatNodes = NodeNumStreet.getLonLatNodes();
			LonLatRueDepartArrivee [0][0] = LonLatNodes[0];
			LonLatRueDepartArrivee [0][1] = LonLatNodes[1];
		}
		
		//coordonnees de la ville d'arrivee
		Coordoonnees = RequeteAPi.requeteCarreAffichage(VilleArrivee);
		float VilleLatMaxArrivee = Coordoonnees[1];
		float VilleLatMinArrivee = Coordoonnees[0];
		float VilleLonMaxArrivee = Coordoonnees[3];
		float VilleLonMinArrivee = Coordoonnees[2];
		
		//si pas de numero pour le depart
		if ( numArrivee.equals("") ) {
			Way[] way = retourRueCoordoonnees(VilleLatMaxArrivee,VilleLatMinArrivee,VilleLonMaxArrivee,VilleLonMinArrivee,nomRueArrivee);
			float [][] LonLatNodes = way[0].getLonLatNodes();
			LonLatRueDepartArrivee [1][0] = LonLatNodes[0][0];
			LonLatRueDepartArrivee [1][1] = LonLatNodes[0][1];
		}
		//si numero pour le depart
		else {
			NodeNumStreet NodeNumStreet = retourNumRueCoordoonnees(VilleLatMaxDepart,VilleLatMinDepart,VilleLonMaxDepart,VilleLonMinDepart,nomRueArrivee,numArrivee);
			if (NodeNumStreet==null) {
				MyException.erreurPersonnalise(null,"Erreur, Probeleme avec le numero.","");
				throw new Exception("Probeleme avec le numero.");
			}
			float [] LonLatNodes = NodeNumStreet.getLonLatNodes();
			LonLatRueDepartArrivee [1][0] = LonLatNodes[0];
			LonLatRueDepartArrivee [1][1] = LonLatNodes[1];
		}
		//retourne les coordonnees du point d'arrivee (index 1) et de depart (index 0)
		return LonLatRueDepartArrivee;
	}
	
	public static Itineraire itineraire( String numRueDepart, String nomRueDepart, String VilleDepart, String paysDepart, String numRueArrivee, String nomRueArrivee, String VilleArrivee, String paysArrivee, int modeTransport) throws Exception 
	{
		//on gere que pour la France
		if ( !paysDepart.equals("France") || !paysArrivee.equals("France")) {
			MyException.erreurPersonnalise(null,"Erreur, uniquement des recherches en France fonctionnent.","Vous avez mis "+paysDepart+" et "+paysArrivee);
			throw new Exception("Erreur, uniquement des recherches en France fonctionnent.");
		}
		float[][] LonLatRueDepartArrivee;
		try {
			//on recupere les coordonnees de depart et d'arrrivee
			LonLatRueDepartArrivee = recuperatioonCoordonneesArriveeDepart(numRueDepart,numRueArrivee,nomRueDepart,nomRueArrivee,VilleDepart,VilleArrivee);
		} catch (Exception e) {
			MyException.erreurPersonnalise(null,"Erreur, la zone de départ ou d'arrivée n'existe probablement pas.","Vous avez mis "+numRueDepart+" "+nomRueDepart+" et "+numRueArrivee+" "+nomRueArrivee);
			throw new Exception("Erreur, la zone de départ ou d'arrivée n'existe probablement pas.");
		}
		float lonDep = LonLatRueDepartArrivee[0][0];
		float latDep = LonLatRueDepartArrivee[0][1];
		float lonArr = LonLatRueDepartArrivee[1][0];
		float latArr = LonLatRueDepartArrivee[1][1];
		RequeteItineraire.coordoonneesDelimitaionZoneRecherche = RequeteAPi.RequeteAPiItineraire(VilleDepart,VilleArrivee);
		
		return RequeteItineraire.RequeteAPiItineraire(lonDep, latDep, lonArr, latArr, modeTransport);
	}
	
	//renvoie les coordonnees de l'itineraire et prend en arg les coordonnees de departs et d'arrivee
		public static Itineraire RequeteAPiItineraire( float lonDep, float latDep, float lonArr, float latArr, int modeTransport) throws Exception { // requete itineraire entre deux coordonnees
			
			if (RequeteItineraire.coordoonneesDelimitaionZoneRecherche==null) {
				throw new Exception("Erreur dans l'execution de la methode.");
			}
			
			Way [] tabWayInit = requeteApi.requeteRoute(coordoonneesDelimitaionZoneRecherche[1], coordoonneesDelimitaionZoneRecherche[0], coordoonneesDelimitaionZoneRecherche[3], coordoonneesDelimitaionZoneRecherche[2]);
			
			
			//on va calculer l'itinereaire 
			int total_ways = 0;
			int total_nodes = 0;
			//modeTransport=0 : a pied
			//modeTransport=1 : a voiture
			//modeTransport=2 : a vélo
			
			
			//on enleve les routes reservées aux  piétons et cyclistes
			System.out.println("Total nodes : "+tabWayInit.length);
			for (int i = 0; tabWayInit[i]!=null; i++) {
				if ( (modeTransport==1)  &&  ( tabWayInit[i].getType().equals("footway") || tabWayInit[i].getType().equals("cycleway")) ) {tabWayInit[i]=new Way(null); }
				else if ( (modeTransport==0)  &&  ( tabWayInit[i].getType().equals("motorway") ) ) {tabWayInit[i]=new Way(null); }
				else if ( (modeTransport==2)  &&  ( tabWayInit[i].getType().equals("motorway") || tabWayInit[i].getType().equals("footway")) ) {tabWayInit[i]=new Way(null); }
				else {total_ways++;}
				//on compte le nombre de nodes
				if (tabWayInit[i].getLonLatNodes()!=null) {total_nodes += tabWayInit[i].getLonLatNodes().length;} 
			}
			Way [] tabWay = new Way[total_ways+1];
			int j = 0;
			for (int i = 0; tabWayInit[i]!=null; i++) {
				if (tabWayInit[i].getLonLatNodes()!=null) {tabWay[j] = tabWayInit[i]; j++;} 
				
			}
			//nb total de nodes
			System.out.println("Total nodes : "+total_nodes);
			//matrice avec les liaisons entre les noeuds (9999 = pas de liaison possible)
			 float [][] tabItineraire = new float[total_nodes][20];
			 /*tableau a deux dimension -1ere numero du noeud 
			 *-deuxieme information sur les noeuds atteignables
			 *la deuxieme dimansions sera divisé en deux parties de 10
			 *la premiere dizaine sera les distance avec les autres noeuds atteignable et l'autre dizaine leur numero a ces noeuds atteignable
			 *case non remplit = 9999 et -1 pour les numeros.
			 *
			 *on fait tout ça pour aconomiser de la place
			 */
			 
			//matrice avec les coordonnees de chaque noeuds
			 // index 1 et 2 coordonnées
			 // index 3 vitesse limite
			 // index 4 correspondance avec tabWay
			 float [][] tabNoeuds = new float[total_nodes][4];
			 
			//on initialise les matrices
			 initialiserMatrice(tabNoeuds, tabItineraire, total_nodes, tabWay);
			 
			//on remplit les matrices
			 initialiserDonneesMatrice(tabNoeuds, tabItineraire, total_nodes, tabWay);

			 //afficherMatrice(tabNoeuds, tabItineraire, total_nodes);
			 
			 String [] da = new String[2];
			 
			 float [] debut = plusProcheWay(lonDep, latDep, tabNoeuds, tabWay, da,0);// on definit le debut du trajet sur la node way la plus proche du point de depart demande 
			 float [] arrivee = plusProcheWay(lonArr, latArr, tabNoeuds, tabWay, da,1);// on definit l'arrivee du trajet sur la node way la plus proche du point d'arrivee demande 
			 if(debut[0]==-1 || arrivee[0]==-1) {
				 //pas de noeuds routiers ont ete trouve a proximite du depart ou de l'arrrivee
				 throw new Exception("Erreur pour le depart ou/et arrivee.");
			 }
				
			 String nameNewFile = "itineraire-"+debut[1]+"-"+debut[0]+"-"+arrivee[1]+"-"+arrivee[0]+"-"+modeTransport+".xml";
			 if (ParseItineraireXml.verifSiFichierXmlExiste(nameNewFile)==true) {
				 return ParseItineraireXml.requeteXmlItineraire(nameNewFile);
			 }
			 //on recupere tout les itineraires faisables et leurs plus courts chemin
			 int [] itineraire = algorithmeDijkstra(tabItineraire, total_nodes, (int) debut[2], (int) arrivee[2], tabNoeuds, modeTransport);
			 //On va récuperer les étapes
			 int taille=0;for (int i=0; itineraire[i]!=-1; i++) {taille++;}
			 String [] etapes = new String[taille];
			 //on met l'itineraire sous une meilleur forme
			 float [][] itineraireFinal = transformeItineraire(itineraire, tabNoeuds, arrivee, modeTransport, etapes, tabWay);
			 	
			//on recupere sa taille en metres
			 int Tailleitineraire = Tailleitineraire(itineraireFinal);
			 
			//on recupere sa duree en metres
			 float Tempsitineraire = Tempsitineraire(itineraireFinal);
			
			//on cree un objet qui contiendra toutes les infos a retourner
			Itineraire res = new Itineraire(debut,etapes, arrivee, itineraireFinal, Tailleitineraire, Tempsitineraire, modeTransport);
			res.setSArrivee(da[1]);
			res.setSdepart(da[0]);
			System.out.println("Depart : "+res.getSdepart());
			System.out.println("arrivee : "+res.getSArrivee());

			//On verifie si itineraire trouvé
			if (res.getTaille()!=0) {
				//on crée le fichier xml
				ParseItineraireXml.creerFichierXmlPourItineraire(res);
				return res;
			}
			else {
				return null;
			}
			

		}
		
		//formule trouvée sur internet
		private static float convertisseur( float lon1, float lon2, float lat1, float lat2)
		{
			float rayonTerre = (float) 6378.137; // rayon de la terre
			float res_lat = (float) (lat2 * Math.PI / 180 - lat1 * Math.PI / 180);
			float res_lon = (float) (lon2 * Math.PI / 180 - lon1 * Math.PI / 180);
			float a = (float) (Math.sin(res_lat/2) * Math.sin(res_lat/2) + Math.cos(lat1 * Math.PI / 180) * Math.cos(lat2 * Math.PI / 180) * Math.sin(res_lon/2) * Math.sin(res_lon/2));
			float c = (float) (2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)));
			float nbkilometre = rayonTerre * c; //on convertie en kilometre
		    return nbkilometre * 1000; // on convertie en mettre
		}

		private static void initialiserMatrice(float [][] tabNoeuds, float [][] tabItineraire, int total_nodes, Way [] tabWay)
		{
			for (int i=0; i<total_nodes; i++) {
				 for (int j=0; j<10; j++) {
					 tabItineraire[i][j] = 99999;
					 tabItineraire[i][j+10] = -1;

				 } 
			 }
			 int j = 0;
			 for (int i=0; tabWay[i]!=null; i++) {
				 float[][] lonLatNodes = tabWay[i].getLonLatNodes();
				 float taille = lonLatNodes.length;
				 for (int l=0; l<taille; l++) {
					 //index correspondant avec tabWay
					 tabNoeuds[j][3] = i;
					 //On remplit avec les coordonnees de chaque noeuds
					 tabNoeuds[j][0] = lonLatNodes[l][0];
					 tabNoeuds[j][1] = lonLatNodes[l][1];
					 //On remplit la vitesse limite
					 if (tabWay[i].getMaxSpeed()==null) {
						 tabNoeuds[j][2] = 30;
					 }
					 else{
						 tabNoeuds[j][2] = Float.parseFloat(tabWay[i].getMaxSpeed());
					 }
					 j++;
				 }
			 }
		}
		
		private static void initialiserDonneesMatrice(float [][] tabNoeuds, float [][] tabItineraire, int total_nodes, Way [] tabWay) {
			int j = 0;
			 for (int i=0; tabWay[i]!=null; i++) {
				 
				 float[][] lonLatNodes = tabWay[i].getLonLatNodes();
				 float taille = lonLatNodes.length;
				 
				 for (int l=0; l<taille; l++) {
					 
					//on remplit la matrice de liason de maniere a ce que dans un way chaque noeud suive bien le chemin
					 tabItineraire[j][0] = 0;
					 tabItineraire[j][10] = j;
					 if (l != 0 && (tabWay[i].getOneWay()!="yes") ) {
						 tabItineraire[j][1] = convertisseur(tabNoeuds[j][0], tabNoeuds[j-1][0], tabNoeuds[j][1], tabNoeuds[j-1][1]);
						 tabItineraire[j][11] = j-1;
						 if (tabItineraire[j][1] < 0) {tabItineraire[j][1]=-tabItineraire[j][1];}
					 }
					 if (l < taille-1 ) {
						 tabItineraire[j][2] = convertisseur(tabNoeuds[j][0], tabNoeuds[j+1][0], tabNoeuds[j][1], tabNoeuds[j+1][1]);
						 tabItineraire[j][12] = j+1;
						 if (tabItineraire[j][2] < 0) {tabItineraire[j][2]=-tabItineraire[j][2];}
					 }
					 
					 j++;
					 
				 }
			 }
			//on remplit la matrice de liason avec les intersections
			 for (int i=0; i<total_nodes; i++) {
				 int k=3;
				 for ( j=0; j<total_nodes; j++) {
					 if( (tabNoeuds[i][0] == tabNoeuds[j][0])) {
						 if(tabNoeuds[i][1] == tabNoeuds[j][1]) {
							 tabItineraire[i][k] = 0;
							 tabItineraire[i][10+k] = j;
							 k++;
						 }
					 }
				 } 
			 }
		}
		
		//calcule la taille de l'itineraire
		private static int Tailleitineraire(float [][] itineraire)
		{
			int taille = 0;
			for (int i=0; i<itineraire.length; i++) {
				if (i+1<itineraire.length) {
					taille = (int) (taille + convertisseur(itineraire[i][0], itineraire[i+1][0], itineraire[i][1], itineraire[i+1][1]));
				}
			}
			return taille;
		}
		
		//calcule le temps de l'itineraire
		private static float Tempsitineraire(float [][] itineraire)
		{
			float temps = 0;
			for (int i=0; i<itineraire.length; i++) {
				if (i+1<itineraire.length) {
					temps = (temps + ( convertisseur(itineraire[i][0], itineraire[i+1][0], itineraire[i][1], itineraire[i+1][1]) / ( itineraire[i][2] / (float) 3.6 ) ) );
				}
			}
			return temps;
		}
		
		//pour mettre l'itineraire sous un tableau en deux dimension.
		private static float [][] transformeItineraire(int [] itineraire, float [][] tabNoeuds, float [] arrivee, int modeDeTransport, String [] etapes, Way [] tabWay)
		{
			
			int taille = 0;
			for (int i=0; itineraire[i]!=-1; i++) {taille++;}
			float [][] res = new float[taille][3];
			String [][] m = new String[taille][2];
			for (int i=0; i < taille; i++) {
				
				//Trajet coordonnées
				res[i][0] = tabNoeuds[itineraire[i]][0];
				res[i][1] = tabNoeuds[itineraire[i]][1];
				
				//Trajet étapes
				if (tabWay[(int) tabNoeuds[itineraire[i]][3]].getName() == null) {m[i][0]="";}
				else{m[i][0] = new String (tabWay[(int) tabNoeuds[itineraire[i]][3]].getName());}
				if (tabWay[(int) tabNoeuds[itineraire[i]][3]].getJunction() == null) {m[i][1]="";}
				else{m[i][1] = new String (tabWay[(int) tabNoeuds[itineraire[i]][3]].getJunction());}
				if ( modeDeTransport==1 ) {
					res[i][2] = tabNoeuds[itineraire[i]][2] - 5 ;
				}
				if ( modeDeTransport==0 ) {
					res[i][2] = 5;
				}
				if ( modeDeTransport==2 ) {
					res[i][2] = 20;
				}
				
			}
			
			String mes = "Prenez ";
			String tmp = "";
			int j = 0;
			for (int i=0; i<taille; i++) {
				if (!m[i][0].equals("") && !tmp.equals(m[i][0])) {

					//Nom route à prendre
					mes += m[i][0];
					tmp = m[i][0];
					
					//On calcule la distance 
					int distance = (int) convertisseur(res[i][0],res[j][0],res[i][1],res[j][1]);
					mes += " dans "+distance+" mètres";
					
					//on ajoute la nouvelle etape
					etapes [i] = mes; 
					
					//On initialise pour le futur message
					mes = "Prenez ";
					j = i;
				}
				if (!m[i][1].equals("") && !tmp.equals(m[i][1])) {
					
					if (m[i][1].equals("roundabout")) {
						
						//Nom route à prendre
						mes += "Le rond point";
						tmp = m[i][1];
						
						//On calcule la distance 
						int distance = (int) convertisseur(res[i][0],res[j][0],res[i][1],res[j][1]);
						mes += " dans "+distance+" mètres";
						
						//on ajoute la nouvelle etape
						etapes [i] = mes; 
						
						//On initialise pour le futur message
						mes = "Sortez sur ";
						j = i;
					}
					else {
						
						//Nom route à prendre
						mes += "La bretelle";
						tmp = m[i][1];
						
						//On calcule la distance 
						int distance = (int) convertisseur(res[i][0],res[j][0],res[i][1],res[j][1]);
						mes += " dans "+distance+" mètres";
						
						//on ajoute la nouvelle etape
						etapes [i] = mes;
						
						//On initialise pour le futur message
						mes = "Continuez sur ";
						j = i;
					}

				}
				etapes [i] += "";
				
				
			}
			return res;
		}
		
		//pour trouver le noeud routier le plus proche des coordonnees entrees en param
		private static float [] plusProcheWay(float lon, float lat, float [][] tabNoeuds,Way [] tabWay, String [] da, int num)
		{
			float min = 9999;
			float actuel;
			float res[] = {-1,-1,-1};
			for (int i=0; i<tabNoeuds.length; i++) {
				actuel = convertisseur(tabNoeuds[i][0], lon, tabNoeuds[i][1], lat);
				if (actuel<=min) {
					res[0] = tabNoeuds[i][0];
					res[1] = tabNoeuds[i][1];
					res[2] = i;
					da[num] = tabWay[(int) tabNoeuds[i][3]].getName();
					min = actuel;
				}
			}
			return res;
		}
		
		//utiliser pour rechercher un element dans la matrice tabItineraire
		private static float recherche(float [][] tabItineraire, int i, int j, float [][] tabNoeuds, int modeTransport)
		{
			for (int k=0; k<10; k++) {
				if (tabItineraire[i][k+10] == j && modeTransport==1 ) {
					return (tabItineraire[i][k] / ( tabNoeuds[(int) tabItineraire[i][k+10]][2] / (float) 3.6 ) ) ;
				}
				if (tabItineraire[i][k+10] == j && modeTransport==2) {
					return tabItineraire[i][k] / (20 / (float) 3.6);
				}
				if (tabItineraire[i][k+10] == j && modeTransport==0) {
					return tabItineraire[i][k] / (20 / (float) 3.6);
				}
			}
			return 99999;
		}
		/*
		//Pour afficher les matrices tabItineraire et tabNoeuds
		private static void afficherMatrice(float [][] tabNoeuds, float [][] tabItineraire, int total_nodes) {
			for (int i=0; i<total_nodes; i++) {
				 //System.out.println(" "+ tabNoeuds[i][0] + "," + tabNoeuds[i][1]);	 
			 }
			 
			 for (int i=0; i<total_nodes; i++) {
				 //System.out.println("");
				 for (int j=0; j<total_nodes; j++) {
					 //System.out.print(" " + tabItineraire[i][j]);
					 float tailleTrajet = recherche(tabItineraire, i, j);
					 //System.out.print(" " + tailleTrajet);
				 } 
				 //System.out.println("");
			 }
		}*/
}
