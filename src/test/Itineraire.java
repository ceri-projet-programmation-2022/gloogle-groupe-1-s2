package test;


public class Itineraire {
	//tab 2d de lat/lon de chaque node pour cette itineraire
	private float[][] lonLatNodes;
	// coordonnees depart
	private String[] etapes;
	// coordonnees depart
	private float[] depart;
	// String depart
	private String sdepart;
	// coordonnees arrivee
	private float[] arrivee;
	// String arrivee
	private String sarrivee;
	// taille en metres du trajet
	private int taille;
	// taille en metres du trajet
	private float temps;
	//Moyen de transport 
	//modeTransport=0 : a pied
	//modeTransport=1 : a voiture
	//modeTransport=2 : a vélo
	private int mtp;
	
	public Itineraire(float [] source,String[] etapes, float [] arrivee, float [][] itineraire, int taille, float temps, int mtp) {
		this.depart=source;
		this.setEtapes(etapes);
		this.arrivee=arrivee;
		this.lonLatNodes=itineraire;
		this.taille=taille;
		this.temps=temps;
		this.setMtp(mtp);
	}
	
	//fonction permettant de stocker le nb de node dans un tableau 
	public void setNodes(float[][] lonLatNodes2) {
		this.lonLatNodes = lonLatNodes2;
	}
	public float[][] getLonLatNodes(){
		return this.lonLatNodes;
	}
	public float[] getDepart(){
		return this.depart;
	}
	public float[] getArrivee(){
		return this.arrivee;
	}
	public int getTaille(){
		return this.taille;
	}
	public float getTemps(){
		return this.temps;
	}
	
	public void affichage()
	{
		System.out.println("Depart : " + depart[1] + ", " +depart[0]);
		System.out.println("arrivee : " + arrivee[1] + ", " +arrivee[0]);
		System.out.println("Taille : " + taille);
		System.out.println("durée : " + temps);
		System.out.println("moyen de transport : " + mtp);
		for (int i=0; i<lonLatNodes.length; i++) {
			System.out.println(lonLatNodes[i][1] + ", " + lonLatNodes[i][0]);
		}
		for (int i =0; i<etapes.length; i++) {
			if (!etapes [i].equals("null")) {System.out.println(etapes [i]);}
		}
	}

	public String[] getEtapes() {
		return etapes;
	}

	public void setEtapes(String[] etapes) {
		this.etapes = etapes;
	}

	public int getMtp() {
		return mtp;
	}

	public void setMtp(int mtp) {
		this.mtp = mtp;
	}

	public String getSArrivee() {
		return sarrivee;
	}

	public void setSArrivee(String SArrivee) {
		sarrivee = SArrivee;
	}

	public String getSdepart() {
		return sdepart;
	}

	public void setSdepart(String sdepart) {
		this.sdepart = sdepart;
	}
}
