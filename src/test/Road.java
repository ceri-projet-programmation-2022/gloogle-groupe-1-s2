package test;
import java.util.ArrayList;
import java.util.List;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;
import javafx.scene.text.FontSmoothingType;




public class Road {

	//une liste pour stocker les threads en cours d'exécution
	List<Thread> threads = new ArrayList<>();

	
	private static RequeteAPi requeteRoute = new RequeteAPi();
	private static float[][]latLon;
	private static int nbNodes;
	private static int sizeXCanvas;
	private static int sizeYCanvas;     
	private static float maxLon;
	private static float minLon;
	private static float maxLat;
	private static float minLat;
	private static Way [] tabWay;
	private static Building [] tabWayB;
	private static float[] test;
	private static int nbWay;
	private static float zoomMax = 1;
	private static float [] dimAff;

	private static float[] popPos;

	//Permet le zoom
	Road(GraphicsContext gc,boolean z,Canvas canvas){
		sizeYCanvas = (int) canvas.getHeight();
		sizeXCanvas = (int) canvas.getWidth();
		zoomer(z);
	}


	// Cree un Objet route pour l'affichage d'une ville
	Road(GraphicsContext gc,GraphicsContext gcRoute,GraphicsContext gcParc ,GraphicsContext gcBuil,GraphicsContext gcZone,GraphicsContext gcEau,TextField fieldVilleRech, Canvas canvas) {
		/* primaryStage.setTitle("Drawing Operations Test");
         Group root = new Group();
         Canvas canvas = new Canvas(sizeXCanvas, sizeYCanvas);
         GraphicsContext gc = canvas.getGraphicsContext2D();*/
		//System.out.print((int) canvas.getHeight() +" | "+ (int) canvas.getWidth());


		sizeYCanvas = (int) canvas.getHeight();
		sizeXCanvas = (int) canvas.getWidth();


		// TextField fieldVilleRech = null;
		//float[] test = RequeteAPi.requeteCarreAffichage(fieldVilleRech.getText() );
		System.out.println("nom de la ville : "+fieldVilleRech.getText()); 
		System.out.println("----------------------------------------d�but requeter Carr�-------------------------------------------------------------");
		test = RequeteAPi.requeteCarreAffichage(fieldVilleRech.getText());
		System.out.println(test[0]);
		System.out.println(test[1]);
		System.out.println(test[2]);
		System.out.println(test[3]);
		System.out.println("----------------------------------------FIN requeter Carr�-------------------------------------------------------------");
		//Way[] v = RequeteAPi.requeteTourVille("Avignon");
		// System.out.println(v[0].getLonLatNodes()[0][1]);
		//float[] test = RequeteAPi.RequeteAPiItineraire("Avignon","Mazan");

		getMaxMin(test);


		//Couleur de fond
		Thread fond = new Thread(new Runnable() {
			public void run() {
				double [] lx = new double[4];
				double [] ly = new double[4];
				lx[0] = (float) sizeXCanvas;
				ly[0] = 0.0F;
				lx[1] = (float) sizeXCanvas;
				ly[1] = (float) sizeYCanvas;
				lx[2] = 0.0F;
				ly[2] = (float) sizeYCanvas;
				lx[3] = 0.0F;
				ly[3] = 0.0F;
				gc.setFill(Color.LIGHTGREEN);
				gc.setStroke(Color.LIGHTGREEN);
				gc.fillPolygon(lx,ly,4);
			}
		});


		//Thread Rail
		Thread rail = new Thread(new Runnable() {
			public void run() {
				Way [] tabWayR = RequeteAPi.requeteRail(test[0], test[1], test[2], test[3]);
				int nbWayR = Way.getNbWay();
				for(int i=0;i<nbWayR && tabWayR[i] != null ;i++) {
					// Vérifiez si une demande d'interruption a été reçue
		            if (Thread.currentThread().isInterrupted()) {
		                System.out.println("Interruption reçue. Sortie de la méthode run()...");
		                return;
		            }
		            
		            // Suite du traitement du thread
					List<Float> aero = new ArrayList<Float>();
					for (int j=0; j<tabWayR[i].getLonLatNodes().length; j++) {
						aero.add(tabWayR[i].getLonLatNodes()[j][0]);
						aero.add(tabWayR[i].getLonLatNodes()[j][1]);
					}
					Float [] l = new Float[aero.size()];
					aero.toArray(l);
					traitsNodesRemplit(gcRoute,l,"rail"); 

				}
				System.out.println("---------Fin-Rail---------");
			}
		});

		//Thread Aero
		Thread aero = new Thread(new Runnable() {
			public void run() {
				Way [] tabWayA = RequeteAPi.requeteAero(test[0], test[1], test[2], test[3]);
				int nbWayR = Way.getNbWay();
				for(int i=0;i<nbWayR && tabWayA[i] != null ;i++) {
					// Vérifiez si une demande d'interruption a été reçue
		            if (Thread.currentThread().isInterrupted()) {
		                System.out.println("Interruption reçue. Sortie de la méthode run()...");
		                return;
		            }
		            
		            // Suite du traitement du thread
					List<Float> aero = new ArrayList<Float>();
					for (int j=0; j<tabWayA[i].getLonLatNodes().length; j++) {
						aero.add(tabWayA[i].getLonLatNodes()[j][0]);
						aero.add(tabWayA[i].getLonLatNodes()[j][1]);
					}
					Float [] l = new Float[aero.size()];
					aero.toArray(l);
					traitsNodesRemplit(gcRoute,l,"aero"); 

				}
				System.out.println("---------Fin-Aero---------");
			}
		});


		//Thread Route
		Thread route = new Thread(new Runnable() {
			public void run() {

				//Route
				//System.out.println("---------Route---------");
				tabWay = requeteRoute.requeteRoute(test[0], test[1], test[2], test[3]);
				nbWay = Way.getNbWay();
				for(int i=0;i<nbWay && tabWay[i] != null ;i++) {
					// Vérifiez si une demande d'interruption a été reçue
		            if (Thread.currentThread().isInterrupted()) {
		                System.out.println("Interruption reçue. Sortie de la méthode run()...");
		                return;
		            }
		            
		            // Suite du traitement du thread
					Road.getListe(tabWay[i].getLonLatNodes());
					if (tabWay[i].getOneWay()!=null) {
						traitsNodes(gcRoute,nbNodes,latLon,"OneWay");
					}
					if (tabWay[i].getType()!=null) {
						if (tabWay[i].getType().equals("motorway")){
							traitsNodes(gcRoute,nbNodes,latLon,"motorway");
						}
						if (tabWay[i].getType().equals("secondary")){
							traitsNodes(gcRoute,nbNodes,latLon,"secondary");
						}
					}
					traitsNodes(gcRoute,nbNodes,latLon,"base");

				}
				System.out.println("---------Fin-Route---------");

				rail.start();
				aero.start();
			}
		});

		//Thread Building
		Thread building = new Thread(new Runnable() {
			public void run() {
				//Building
				//System.out.println("---------Building---------");
				tabWayB = RequeteAPi.requeteBuilding(test[1], test[0], test[3], test[2]);
				RequeteAPi.affichageConsoleBuilding(tabWayB);
				int nbBat=tabWayB.length;
				for(int i=0;i<nbBat && tabWayB[i] != null ;i++) {
					// Vérifiez si une demande d'interruption a été reçue
		            if (Thread.currentThread().isInterrupted()) {
		                System.out.println("Interruption reçue. Sortie de la méthode run()...");
		                return;
		            }
		            
		            // Suite du traitement du thread
					List<Float> batiment = new ArrayList<Float>();
					for (int j=0; j<tabWayB[i].getLonLatNodes().length; j++) {
						batiment.add(tabWayB[i].getLonLatNodes()[j][0]);
						batiment.add(tabWayB[i].getLonLatNodes()[j][1]);
					}
					Float [] l = new Float[batiment.size()];
					batiment.toArray(l);
					traitsNodesRemplit(gcBuil,l,"batiment"); 

				}
				System.out.println("---------Fin-Building---------");

			}
		});
		
		
		//Thread BuildingOpti
		Thread buildingOpti = new Thread(new Runnable() {
			public void run() {
				//Building
				//System.out.println("---------Building---------");
				for (int a=0; a<4; a++) {
					// Vérifiez si une demande d'interruption a été reçue
		            if (Thread.currentThread().isInterrupted()) {
		                System.out.println("Interruption reçue. Sortie de la méthode run()...");
		                return;
		            }
		            
		            // Suite du traitement du thread
					float latInt = (test[1]-test[0])/2+test[0];
					float lonInt = (test[3]-test[2])/2+test[2];
					if (a==0) {
						System.out.println(test[1]);
						System.out.println(test[0]);
						System.out.println(test[3]);
						System.out.println(test[2]);
						System.out.println(lonInt);
						System.out.println(latInt);
						tabWayB = RequeteAPi.requeteBuilding(latInt, test[0], lonInt, test[2]);
					}
					if (a==1) {
						tabWayB = RequeteAPi.requeteBuilding(test[1], latInt, lonInt, test[2]);
					}
					if (a==2) {
						tabWayB = RequeteAPi.requeteBuilding(latInt, test[0], test[3], lonInt);
					}
					if (a==3) {
						tabWayB = RequeteAPi.requeteBuilding(test[1], latInt, test[3], lonInt);
					}


					//RequeteAPi.affichageConsoleBuilding(tabWayB);
					int nbBat=tabWayB.length;
					for(int i=0;i<nbBat && tabWayB[i] != null ;i++) {
						// Vérifiez si une demande d'interruption a été reçue
			            if (Thread.currentThread().isInterrupted()) {
			                System.out.println("Interruption reçue. Sortie de la méthode run()...");
			                return;
			            }
			            
			            // Suite du traitement du thread
						List<Float> batiment = new ArrayList<Float>();
						for (int j=0; j<tabWayB[i].getLonLatNodes().length; j++) {
							batiment.add(tabWayB[i].getLonLatNodes()[j][0]);
							batiment.add(tabWayB[i].getLonLatNodes()[j][1]);
						}
						Float [] l = new Float[batiment.size()];
						batiment.toArray(l);
						traitsNodesRemplit(gcBuil,l,"batiment"); 

					}
					System.out.println("---------Fin-Building---------");

				}
			}
		});
		
		
	
				
				

		//Thread Parc
		Thread parc = new Thread(new Runnable() {
			public void run() {

				Way [] tabWayP = RequeteAPi.requeteParc(test[0], test[1], test[2], test[3]);
				int nbWayP = Way.getNbWay();
				for(int i=0;i<nbWayP && tabWayP[i] != null ;i++) {
					List<Float> parc = new ArrayList<Float>();
					for (int j=0; j<tabWayP[i].getLonLatNodes().length; j++) {
						// Vérifiez si une demande d'interruption a été reçue
			            if (Thread.currentThread().isInterrupted()) {
			                System.out.println("Interruption reçue. Sortie de la méthode run()...");
			                return;
			            }
			            
			            // Suite du traitement du thread
						parc.add(tabWayP[i].getLonLatNodes()[j][0]);
						parc.add(tabWayP[i].getLonLatNodes()[j][1]);
					}
					Float [] l = new Float[parc.size()];
					parc.toArray(l);
					traitsNodesRemplit(gcParc,l,"parc"); 

				}
				System.out.println("---------Fin-Parc---------");
			}
		});


		//Thread Parking
		Thread parking = new Thread(new Runnable() {
			public void run() {
				Way [] tabWayP = RequeteAPi.requeteParking(test[0], test[1], test[2], test[3]);
				int nbWayP = Way.getNbWay();
				for(int i=0;i<nbWayP && tabWayP[i] != null ;i++) {
					List<Float> parking = new ArrayList<Float>();
					for (int j=0; j<tabWayP[i].getLonLatNodes().length; j++) {
						// Vérifiez si une demande d'interruption a été reçue
			            if (Thread.currentThread().isInterrupted()) {
			                System.out.println("Interruption reçue. Sortie de la méthode run()...");
			                return;
			            }
			            
			            // Suite du traitement du thread
						parking.add(tabWayP[i].getLonLatNodes()[j][0]);
						parking.add(tabWayP[i].getLonLatNodes()[j][1]);
					}
					Float [] l = new Float[parking.size()];
					parking.toArray(l);
					traitsNodesRemplit(gcZone,l,"parking"); 

				}
				System.out.println("---------Fin-Parking---------");
			}
		});


		//Thread Farmland
		Thread farmland = new Thread(new Runnable() {
			public void run() {
				Way [] tabWayF = RequeteAPi.requeteFarmland(test[0], test[1], test[2], test[3]);
				int nbWayF = Way.getNbWay();
				for(int i=0;i<nbWayF && tabWayF[i] != null ;i++) {
					List<Float> parking = new ArrayList<Float>();
					for (int j=0; j<tabWayF[i].getLonLatNodes().length; j++) {
						// Vérifiez si une demande d'interruption a été reçue
			            if (Thread.currentThread().isInterrupted()) {
			                System.out.println("Interruption reçue. Sortie de la méthode run()...");
			                return;
			            }
			            
			            // Suite du traitement du thread
						parking.add(tabWayF[i].getLonLatNodes()[j][0]);
						parking.add(tabWayF[i].getLonLatNodes()[j][1]);
					}
					Float [] l = new Float[parking.size()];
					parking.toArray(l);
					traitsNodesRemplit(gc,l,"farmland"); 

				}
				System.out.println("---------Fin-Farmland---------");
			}
		});

		//Thread Foret
		Thread forest = new Thread(new Runnable() {
			public void run() {
				Way [] tabWayF = RequeteAPi.requeteForest(test[0], test[1], test[2], test[3]);
				int nbWayF = Way.getNbWay();
				for(int i=0;i<nbWayF && tabWayF[i] != null ;i++) {
					List<Float> parking = new ArrayList<Float>();
					for (int j=0; j<tabWayF[i].getLonLatNodes().length; j++) {
						// Vérifiez si une demande d'interruption a été reçue
			            if (Thread.currentThread().isInterrupted()) {
			                System.out.println("Interruption reçue. Sortie de la méthode run()...");
			                return;
			            }
			            
			            // Suite du traitement du thread
						parking.add(tabWayF[i].getLonLatNodes()[j][0]);
						parking.add(tabWayF[i].getLonLatNodes()[j][1]);
					}
					Float [] l = new Float[parking.size()];
					parking.toArray(l);
					traitsNodesRemplit(gcParc,l,"forest"); 

				}
				System.out.println("---------Fin-Foret---------");
			}
		});

		//Thread Scrub
		Thread scrub = new Thread(new Runnable() {
			public void run() {
				Way [] tabWayS = RequeteAPi.requeteScrub(test[0], test[1], test[2], test[3]);
				int nbWayS = Way.getNbWay();
				for(int i=0;i<nbWayS && tabWayS[i] != null ;i++) {
					List<Float> scrub = new ArrayList<Float>();
					for (int j=0; j<tabWayS[i].getLonLatNodes().length; j++) {
						// Vérifiez si une demande d'interruption a été reçue
			            if (Thread.currentThread().isInterrupted()) {
			                System.out.println("Interruption reçue. Sortie de la méthode run()...");
			                return;
			            }
			            
			            // Suite du traitement du thread
						scrub.add(tabWayS[i].getLonLatNodes()[j][0]);
						scrub.add(tabWayS[i].getLonLatNodes()[j][1]);
					}
					Float [] l = new Float[scrub.size()];
					scrub.toArray(l);
					traitsNodesRemplit(gc,l,"scrub"); 

				}
				System.out.println("---------Fin-Scrub---------");
			}
		});


		//Thread Zone Commercial
		Thread commercial = new Thread(new Runnable() {
			public void run() {
				Way [] tabWayC = RequeteAPi.requeteZoneCommercial(test[0], test[1], test[2], test[3]);
				int nbWayC = Way.getNbWay();
				//System.out.println(tabWayP[0].getIdWay());

				for(int i=0;i<nbWayC && tabWayC[i] != null ;i++) {
					// Vérifiez si une demande d'interruption a été reçue
		            if (Thread.currentThread().isInterrupted()) {
		                System.out.println("Interruption reçue. Sortie de la méthode run()...");
		                return;
		            }
		            
		            // Suite du traitement du thread
					getListe(tabWayC[i].getLonLatNodes());
					traitsZoneCommercial(gcZone,nbNodes,latLon);
				}
				System.out.println("---------Fin-ZoneCommercial---------");
			}
		});


		//Thread Residentiel
		Thread residential = new Thread(new Runnable() {
			public void run() {


				//Residentiel
				List<?> list = RequeteAPi.requeteResidential(test[1], test[0], test[3], test[2]);
				//nbWay = Water.getLonLatNodes().length;
				Relation [][] tabWayR = new Relation[list.size()][100000];
				list.toArray(tabWayR);

				for (int j=0; j<tabWayR.length ;j++) {

					//initialisation
					List<Float> latLon = new ArrayList<Float>();
					boolean [] visite = new boolean [tabWayR[j].length];
					for (int i1=0; i1<tabWayR[j].length; i1++) {
						visite[i1]=false;
						if ( tabWayR[j][i1] == null) {
							visite[i1]=true;
						}
					}
					boolean fin = true;
					int deb = 0;
					for (int i1=0; i1<tabWayR[j].length && fin==true; i1++) {
						if (tabWayR[j][i1]!=null) {
							if (tabWayR[j][i1].getRole()!=null) {
								if (!tabWayR[j][i1].getRole().equals("inner")) {
									deb = i1;
									fin = false;
								}
							}
							else if (tabWayR[j][i1].getRole()==null) {
								deb = i1;
								fin = false;
							}
						}
					}
					visite[deb]=true;
					int pro = deb;
					int sens = 1;

					for (int k=0; k<tabWayR[j][deb].getLonLatNodes().length; k++) {
						latLon.add( tabWayR[j][deb].getLonLatNodes()[k][0]);
						latLon.add( tabWayR[j][deb].getLonLatNodes()[k][1]);
					}
					//fin init
					float min = 9999F;
					while (fin!=true) {
						// Vérifiez si une demande d'interruption a été reçue
			            if (Thread.currentThread().isInterrupted()) {
			                System.out.println("Interruption reçue. Sortie de la méthode run()...");
			                return;
			            }
			            
			            // Suite du traitement du thread
						min = 9999F;
						fin=true;
						boolean quitter = false;

						//Verification de quelle membre prendre
						for (int i=0; i<tabWayR[j].length && tabWayR[j][i]!=null && tabWayR[j][i].getLonLatNodes().length>0 && quitter==false; i++) {
							if (visite[i]==false) {
								//System.out.println("t ; "+tabWayW[j].length);
								if (tabWayR[j][i].getRole()!=null) {
									if (tabWayR[j][i].getRole().equals("inner")) {
										visite[i]=true;
									}
								}

								if (sens == 1) {

									if (tabWayR[j][deb].getLonLatNodes()[tabWayR[j][deb].getLonLatNodes().length-1][0] == tabWayR[j][i].getLonLatNodes()[0][0]){
										pro = i;
										sens = 1;
										quitter = true;
									}
									else if ( tabWayR[j][deb].getLonLatNodes()[tabWayR[j][deb].getLonLatNodes().length-1][0] == tabWayR[j][i].getLonLatNodes()[tabWayR[j][i].getLonLatNodes().length-1][0]) {
										sens = 0;
										pro = i;
										quitter = true;
									}
									float diffx = tabWayR[j][deb].getLonLatNodes()[tabWayR[j][deb].getLonLatNodes().length-1][0] - tabWayR[j][i].getLonLatNodes()[0][0];
									if (diffx<0) {diffx=-diffx;}
									if (diffx<min) {
										min = diffx;
										pro = i;

									}
									diffx = tabWayR[j][deb].getLonLatNodes()[tabWayR[j][deb].getLonLatNodes().length-1][0] - tabWayR[j][i].getLonLatNodes()[tabWayR[j][i].getLonLatNodes().length-1][0];
									if (diffx<0) {diffx=-diffx;}
									if (diffx<min) {
										min = diffx;
										pro = i;

									}
								}
								else {
									float diffx = tabWayR[j][deb].getLonLatNodes()[0][0] - tabWayR[j][i].getLonLatNodes()[0][0];
									if (diffx<0) {diffx=-diffx;}
									if (diffx<min) {
										min = diffx;
										pro = i;

									}
									diffx = tabWayR[j][deb].getLonLatNodes()[0][0] - tabWayR[j][i].getLonLatNodes()[tabWayR[j][i].getLonLatNodes().length-1][0];
									if (diffx<0) {diffx=-diffx;}
									if (diffx<min) {
										min = diffx;
										pro = i;

									}
									if (tabWayR[j][deb].getLonLatNodes()[0][0] == tabWayR[j][i].getLonLatNodes()[0][0]){
										pro = i;
										sens = 1;
										quitter = true;
									}
									else if ( tabWayR[j][deb].getLonLatNodes()[0][0] == tabWayR[j][i].getLonLatNodes()[tabWayR[j][i].getLonLatNodes().length-1][0]) {
										sens = 0;
										pro = i;
										quitter = true;
									}
								}
								fin=false;
							}
						}
						if (deb==pro) {
							fin=true;
						}
						deb=pro;

						visite[deb]=true;
						//on rempli la liste
						if (sens == 0) {
							for (int k=tabWayR[j][deb].getLonLatNodes().length-1; k>=0 && fin==false; k=k-1) {
								latLon.add( tabWayR[j][deb].getLonLatNodes()[k][0]);
								latLon.add( tabWayR[j][deb].getLonLatNodes()[k][1]);
							}
						}
						else if (sens == 1) {
							for (int k=0; k<tabWayR[j][deb].getLonLatNodes().length && fin==false; k=k+1) {
								latLon.add( tabWayR[j][deb].getLonLatNodes()[k][0]);
								latLon.add( tabWayR[j][deb].getLonLatNodes()[k][1]);
							}
						}


					}

					Float [] l = new Float[latLon.size()];
					latLon.toArray(l);
					traitsNodesRemplit(gc,l,"residential"); 			


				}
			}
		});

		//Thread Industriel
		Thread industrial = new Thread(new Runnable() {
			public void run() {


				//Residentiel
				List<?> list = RequeteAPi.requeteIndustrial(test[1], test[0], test[3], test[2]);
				//nbWay = Water.getLonLatNodes().length;
				Relation [][] tabWayR = new Relation[list.size()][100000];
				list.toArray(tabWayR);

				for (int j=0; j<tabWayR.length ;j++) {

					//initialisation
					List<Float> latLon = new ArrayList<Float>();
					boolean [] visite = new boolean [tabWayR[j].length];
					for (int i1=0; i1<tabWayR[j].length; i1++) {
						visite[i1]=false;
						if ( tabWayR[j][i1] == null) {
							visite[i1]=true;
						}
					}
					boolean fin = true;
					int deb = 0;
					for (int i1=0; i1<tabWayR[j].length && fin==true; i1++) {
						if (tabWayR[j][i1]!=null) {
							if (tabWayR[j][i1].getRole()!=null) {
								if (!tabWayR[j][i1].getRole().equals("inner")) {
									deb = i1;
									fin = false;
								}
							}
							else if (tabWayR[j][i1].getRole()==null) {
								deb = i1;
								fin = false;
							}
						}
					}
					visite[deb]=true;
					int pro = deb;
					int sens = 1;

					for (int k=0; k<tabWayR[j][deb].getLonLatNodes().length; k++) {
						latLon.add( tabWayR[j][deb].getLonLatNodes()[k][0]);
						latLon.add( tabWayR[j][deb].getLonLatNodes()[k][1]);
					}
					//fin init
					float min = 9999F;
					while (fin!=true) {
						// Vérifiez si une demande d'interruption a été reçue
			            if (Thread.currentThread().isInterrupted()) {
			                System.out.println("Interruption reçue. Sortie de la méthode run()...");
			                return;
			            }
			            
			            // Suite du traitement du thread
						min = 9999F;
						fin=true;
						boolean quitter = false;

						//Verification de quelle membre prendre
						for (int i=0; i<tabWayR[j].length && tabWayR[j][i]!=null && tabWayR[j][i].getLonLatNodes().length>0 && quitter==false; i++) {
							if (visite[i]==false) {
								//System.out.println("t ; "+tabWayW[j].length);
								if (tabWayR[j][i].getRole()!=null) {
									if (tabWayR[j][i].getRole().equals("inner")) {
										visite[i]=true;
									}
								}

								if (sens == 1) {

									if (tabWayR[j][deb].getLonLatNodes()[tabWayR[j][deb].getLonLatNodes().length-1][0] == tabWayR[j][i].getLonLatNodes()[0][0]){
										pro = i;
										sens = 1;
										quitter = true;
									}
									else if ( tabWayR[j][deb].getLonLatNodes()[tabWayR[j][deb].getLonLatNodes().length-1][0] == tabWayR[j][i].getLonLatNodes()[tabWayR[j][i].getLonLatNodes().length-1][0]) {
										sens = 0;
										pro = i;
										quitter = true;
									}
									float diffx = tabWayR[j][deb].getLonLatNodes()[tabWayR[j][deb].getLonLatNodes().length-1][0] - tabWayR[j][i].getLonLatNodes()[0][0];
									if (diffx<0) {diffx=-diffx;}
									if (diffx<min) {
										min = diffx;
										pro = i;

									}
									diffx = tabWayR[j][deb].getLonLatNodes()[tabWayR[j][deb].getLonLatNodes().length-1][0] - tabWayR[j][i].getLonLatNodes()[tabWayR[j][i].getLonLatNodes().length-1][0];
									if (diffx<0) {diffx=-diffx;}
									if (diffx<min) {
										min = diffx;
										pro = i;

									}
								}
								else {
									float diffx = tabWayR[j][deb].getLonLatNodes()[0][0] - tabWayR[j][i].getLonLatNodes()[0][0];
									if (diffx<0) {diffx=-diffx;}
									if (diffx<min) {
										min = diffx;
										pro = i;

									}
									diffx = tabWayR[j][deb].getLonLatNodes()[0][0] - tabWayR[j][i].getLonLatNodes()[tabWayR[j][i].getLonLatNodes().length-1][0];
									if (diffx<0) {diffx=-diffx;}
									if (diffx<min) {
										min = diffx;
										pro = i;

									}
									if (tabWayR[j][deb].getLonLatNodes()[0][0] == tabWayR[j][i].getLonLatNodes()[0][0]){
										pro = i;
										sens = 1;
										quitter = true;
									}
									else if ( tabWayR[j][deb].getLonLatNodes()[0][0] == tabWayR[j][i].getLonLatNodes()[tabWayR[j][i].getLonLatNodes().length-1][0]) {
										sens = 0;
										pro = i;
										quitter = true;
									}
								}
								fin=false;
							}
						}
						if (deb==pro) {
							fin=true;
						}
						deb=pro;

						visite[deb]=true;
						//on rempli la liste
						if (sens == 0) {
							for (int k=tabWayR[j][deb].getLonLatNodes().length-1; k>=0 && fin==false; k=k-1) {
								latLon.add( tabWayR[j][deb].getLonLatNodes()[k][0]);
								latLon.add( tabWayR[j][deb].getLonLatNodes()[k][1]);
							}
						}
						else if (sens == 1) {
							for (int k=0; k<tabWayR[j][deb].getLonLatNodes().length && fin==false; k=k+1) {
								latLon.add( tabWayR[j][deb].getLonLatNodes()[k][0]);
								latLon.add( tabWayR[j][deb].getLonLatNodes()[k][1]);
							}
						}


					}

					Float [] l = new Float[latLon.size()];
					latLon.toArray(l);
					traitsNodesRemplit(gc,l,"industrial"); 			


				}

				System.out.println("---------Fin-Industrie---------");

				forest.start();
			}
		});

		//Water
		Thread water = new Thread(new Runnable() {
			public void run() {
				//Point d'eau
				List<?> list2 = RequeteAPi.requeteWater2(test[1], test[0], test[3], test[2]);
				//nbWay = Water.getLonLatNodes().length;
				Relation [][] tabWayW = new Relation[list2.size()][100000];
				list2.toArray(tabWayW);

				for (int j=0; j<tabWayW.length ;j++) {

					//initialisation
					List<Float> latLon = new ArrayList<Float>();
					boolean [] visite = new boolean [tabWayW[j].length];
					for (int i1=0; i1<tabWayW[j].length; i1++) {
						visite[i1]=false;
						if ( tabWayW[j][i1] == null) {
							visite[i1]=true;
						}
					}
					boolean fin = true;
					int deb = 0;
					for (int i1=0; i1<tabWayW[j].length && fin==true; i1++) {
						if (tabWayW[j][i1]!=null) {
							if (tabWayW[j][i1].getRole()!=null) {
								if (!tabWayW[j][i1].getRole().equals("inner")) {
									deb = i1;
									fin = false;
								}
							}
							else if (tabWayW[j][i1].getRole()==null) {
								deb = i1;
								fin = false;
							}
						}
					}
					visite[deb]=true;
					int pro = deb;
					int sens = 1;

					for (int k=0; k<tabWayW[j][deb].getLonLatNodes().length; k++) {
						latLon.add( tabWayW[j][deb].getLonLatNodes()[k][0]);
						latLon.add( tabWayW[j][deb].getLonLatNodes()[k][1]);
					}
					//fin init
					float min = 9999F;
					while (fin!=true) {
						// Vérifiez si une demande d'interruption a été reçue
			            if (Thread.currentThread().isInterrupted()) {
			                System.out.println("Interruption reçue. Sortie de la méthode run()...");
			                return;
			            }
			            
			            // Suite du traitement du thread
						min = 9999F;
						fin=true;
						boolean quitter = false;

						//Verification de quelle membre prendre
						for (int i=0; i<tabWayW[j].length && tabWayW[j][i]!=null && tabWayW[j][i].getLonLatNodes().length>0 && quitter==false; i++) {
							if (visite[i]==false) {
								if (tabWayW[j][i].getRole()!=null) {
									if (tabWayW[j][i].getRole().equals("inner")) {
										visite[i]=true;
									}
								}

								if (sens == 1) {

									if (tabWayW[j][deb].getLonLatNodes()[tabWayW[j][deb].getLonLatNodes().length-1][0] == tabWayW[j][i].getLonLatNodes()[0][0]){
										pro = i;
										sens = 1;
										quitter = true;
									}
									else if ( tabWayW[j][deb].getLonLatNodes()[tabWayW[j][deb].getLonLatNodes().length-1][0] == tabWayW[j][i].getLonLatNodes()[tabWayW[j][i].getLonLatNodes().length-1][0]) {
										sens = 0;
										pro = i;
										quitter = true;
									}
									float diffx = tabWayW[j][deb].getLonLatNodes()[tabWayW[j][deb].getLonLatNodes().length-1][0] - tabWayW[j][i].getLonLatNodes()[0][0];
									if (diffx<0) {diffx=-diffx;}
									if (diffx<min) {
										min = diffx;
										pro = i;

									}
									diffx = tabWayW[j][deb].getLonLatNodes()[tabWayW[j][deb].getLonLatNodes().length-1][0] - tabWayW[j][i].getLonLatNodes()[tabWayW[j][i].getLonLatNodes().length-1][0];
									if (diffx<0) {diffx=-diffx;}
									if (diffx<min) {
										min = diffx;
										pro = i;

									}
								}
								else {
									float diffx = tabWayW[j][deb].getLonLatNodes()[0][0] - tabWayW[j][i].getLonLatNodes()[0][0];
									if (diffx<0) {diffx=-diffx;}
									if (diffx<min) {
										min = diffx;
										pro = i;

									}
									diffx = tabWayW[j][deb].getLonLatNodes()[0][0] - tabWayW[j][i].getLonLatNodes()[tabWayW[j][i].getLonLatNodes().length-1][0];
									if (diffx<0) {diffx=-diffx;}
									if (diffx<min) {
										min = diffx;
										pro = i;

									}
									if (tabWayW[j][deb].getLonLatNodes()[0][0] == tabWayW[j][i].getLonLatNodes()[0][0]){
										pro = i;
										sens = 1;
										quitter = true;
									}
									else if ( tabWayW[j][deb].getLonLatNodes()[0][0] == tabWayW[j][i].getLonLatNodes()[tabWayW[j][i].getLonLatNodes().length-1][0]) {
										sens = 0;
										pro = i;
										quitter = true;
									}
								}
								fin=false;
							}
						}
						if (deb==pro) {
							fin=true;
						}
						deb=pro;

						visite[deb]=true;
						//on rempli la liste
						if (sens == 0) {
							for (int k=tabWayW[j][deb].getLonLatNodes().length-1; k>=0 && fin==false; k=k-1) {
								latLon.add( tabWayW[j][deb].getLonLatNodes()[k][0]);
								latLon.add( tabWayW[j][deb].getLonLatNodes()[k][1]);
							}
						}
						else if (sens == 1) {
							for (int k=0; k<tabWayW[j][deb].getLonLatNodes().length && fin==false; k=k+1) {
								latLon.add( tabWayW[j][deb].getLonLatNodes()[k][0]);
								latLon.add( tabWayW[j][deb].getLonLatNodes()[k][1]);
							}
						}


					}

					Float [] l = new Float[latLon.size()];
					latLon.toArray(l);
					traitsNodesRemplit(gcEau,l,"outer"); 			


				}
				System.out.println("---------Fin-PointD'eau-Outer---------");

				//Points d'eau inner
				List<?> list3 = RequeteAPi.requeteWater2(test[1], test[0], test[3], test[2]);
				//nbWay = Water.getLonLatNodes().length;
				tabWayW = new Relation[list3.size()][10000];
				list3.toArray(tabWayW);

				List<Float[]> inner = new ArrayList<Float[]>();
				for (int j=0; j<tabWayW.length && tabWayW[j][0] != null ;j++) {
					for(int i=0; i<tabWayW[j].length && tabWayW[j][i] != null ;i++) {
						if (tabWayW[j][i].getRole()!=null) {
							if (tabWayW[j][i].getRole().equals("inner")) {
								Float [] l = new Float[tabWayW[j][i].getLonLatNodes().length*2];
								int m=0;
								for (int k=0; k<tabWayW[j][i].getLonLatNodes().length; k++) {
									// Vérifiez si une demande d'interruption a été reçue
						            if (Thread.currentThread().isInterrupted()) {
						                System.out.println("Interruption reçue. Sortie de la méthode run()...");
						                return;
						            }
						            
						            // Suite du traitement du thread
									l[m] = tabWayW[j][i].getLonLatNodes()[k][0];
									l[m+1] = tabWayW[j][i].getLonLatNodes()[k][1];
									m=m+2;

								}
								inner.add(l);
							}
						}

					}


					for (int i=0; i<inner.size(); i++) {
						traitsNodesRemplit(gcEau,inner.get(i),"inner"); 
					}
				}
				System.out.println("---------Fin-PointD'eau-Inner---------");
			}
		});

		//On arrete tout les anciens threads
		for (Thread thread : threads) {
		    thread.interrupt();
		}

		
		//On commence à charger l'affichage si la zone à bien été trouvée
		if ( test[0]!=0 && test[1]!=0 && test[2]!=0 && test[3]!=0) {
			threads.add(fond);fond.start();
			threads.add(route);route.start();
			dimAff = convertionCanvas();
			if(dimAff[0]<=0.15 && dimAff[1]<=0.15) {threads.add(building);building.start();}
			else {threads.add(buildingOpti);buildingOpti.start();}
			threads.add(parc);parc.start();
			threads.add(parking);parking.start();
			threads.add(residential);residential.start();
			threads.add(industrial);industrial.start();
			threads.add(water);water.start();
			threads.add(scrub);scrub.start();
			threads.add(commercial);commercial.start();
			threads.add(farmland);farmland.start();
			
			
		}
		else {
			MyException.erreurPersonnalise(null,"Erreur, La ville est introuvable.","Vous avez mis "+fieldVilleRech.getText());
		}

	}


	//Cree un obj Route pour l'affichage de l'itineraire
	Road(GraphicsContext gc,GraphicsContext gcRoute,GraphicsContext gcParc ,GraphicsContext gcBuil,GraphicsContext gcZone,GraphicsContext gcEau,TextField fieldVilleRechDep,TextField fieldVilleRechArr,Itineraire itieraireTrouve, Canvas canvas ,Button  dep,Button arr) {

		/* primaryStage.setTitle("Drawing Operations Test");
 	         Group root = new Group();
 	         Canvas canvas = new Canvas(sizeXCanvas, sizeYCanvas);
 	         GraphicsContext gc = canvas.getGraphicsContext2D();*/
		//System.out.print((int) canvas.getHeight() +" | "+ (int) canvas.getWidth());
		sizeYCanvas = (int) canvas.getHeight();
		sizeXCanvas = (int) canvas.getWidth();


		// TextField fieldVilleRech = null;
		//float[] test = RequeteAPi.requeteCarreAffichage(fieldVilleRech.getText() );
		test = RequeteAPi.RequeteAPiItineraire(fieldVilleRechDep.getText(),fieldVilleRechArr.getText());
		//float[] test = RequeteAPi.RequeteAPiItineraire("Avignon","Mazan");

		getMaxMin(test);
		//Couleur de fond
		Thread fond = new Thread(new Runnable() {
			public void run() {
				double [] lx = new double[4];
				double [] ly = new double[4];
				lx[0] = (float) sizeXCanvas;
				ly[0] = 0.0F;
				lx[1] = (float) sizeXCanvas;
				ly[1] = (float) sizeYCanvas;
				lx[2] = 0.0F;
				ly[2] = (float) sizeYCanvas;
				lx[3] = 0.0F;
				ly[3] = 0.0F;
				gc.setFill(Color.LIGHTGREEN);
				gc.setStroke(Color.LIGHTGREEN);
				gc.fillPolygon(lx,ly,4);
			}
		});


		//Thread Rail
		Thread rail = new Thread(new Runnable() {
			public void run() {
				Way [] tabWayR = RequeteAPi.requeteRail(test[0], test[1], test[2], test[3]);
				int nbWayR = Way.getNbWay();
				for(int i=0;i<nbWayR && tabWayR[i] != null ;i++) {
					// Vérifiez si une demande d'interruption a été reçue
		            if (Thread.currentThread().isInterrupted()) {
		                System.out.println("Interruption reçue. Sortie de la méthode run()...");
		                return;
		            }
		            
		            // Suite du traitement du thread
					List<Float> aero = new ArrayList<Float>();
					for (int j=0; j<tabWayR[i].getLonLatNodes().length; j++) {
						aero.add(tabWayR[i].getLonLatNodes()[j][0]);
						aero.add(tabWayR[i].getLonLatNodes()[j][1]);
					}
					Float [] l = new Float[aero.size()];
					aero.toArray(l);
					traitsNodesRemplit(gcRoute,l,"rail"); 

				}
				System.out.println("---------Fin-Rail---------");
			}
		});

		//Thread Aero
		Thread aero = new Thread(new Runnable() {
			public void run() {
				Way [] tabWayA = RequeteAPi.requeteAero(test[0], test[1], test[2], test[3]);
				int nbWayR = Way.getNbWay();
				for(int i=0;i<nbWayR && tabWayA[i] != null ;i++) {
					// Vérifiez si une demande d'interruption a été reçue
		            if (Thread.currentThread().isInterrupted()) {
		                System.out.println("Interruption reçue. Sortie de la méthode run()...");
		                return;
		            }
		            
		            // Suite du traitement du thread
					List<Float> aero = new ArrayList<Float>();
					for (int j=0; j<tabWayA[i].getLonLatNodes().length; j++) {
						aero.add(tabWayA[i].getLonLatNodes()[j][0]);
						aero.add(tabWayA[i].getLonLatNodes()[j][1]);
					}
					Float [] l = new Float[aero.size()];
					aero.toArray(l);
					traitsNodesRemplit(gcRoute,l,"aero"); 

				}
				System.out.println("---------Fin-Aero---------");
			}
		});


		//Thread Route
		Thread route = new Thread(new Runnable() {
			public void run() {

				//Route
				//System.out.println("---------Route---------");
				tabWay = requeteRoute.requeteRoute(test[0], test[1], test[2], test[3]);
				nbWay = Way.getNbWay();
				for(int i=0;i<nbWay && tabWay[i] != null ;i++) {
					// Vérifiez si une demande d'interruption a été reçue
		            if (Thread.currentThread().isInterrupted()) {
		                System.out.println("Interruption reçue. Sortie de la méthode run()...");
		                return;
		            }
		            
		            // Suite du traitement du thread
					Road.getListe(tabWay[i].getLonLatNodes());
					if (tabWay[i].getType()!=null) {
						if (tabWay[i].getType().equals("motorway")){
							traitsNodes(gcRoute,nbNodes,latLon,"motorway");
						}
						if (tabWay[i].getType().equals("secondary")){
							traitsNodes(gcRoute,nbNodes,latLon,"secondary");
						}
					}
					traitsNodes(gcRoute,nbNodes,latLon,"base");

				}
				//cette ligne affiche l'itineraire
				if (itieraireTrouve!=null) {traitsNodesPourItineraire(gcRoute, itieraireTrouve.getLonLatNodes().length, itieraireTrouve.getLonLatNodes(),  dep, arr);}

				System.out.println("---------Fin-Route---------");

				rail.start();
				aero.start();
			}
		});

		//Thread Building
		Thread building = new Thread(new Runnable() {
			public void run() {
				//Building
				//System.out.println("---------Building---------");
				tabWayB = RequeteAPi.requeteBuilding(test[1], test[0], test[3], test[2]);
				RequeteAPi.affichageConsoleBuilding(tabWayB);
				int nbBat=tabWayB.length;
				for(int i=0;i<nbBat && tabWayB[i] != null ;i++) {
					// Vérifiez si une demande d'interruption a été reçue
		            if (Thread.currentThread().isInterrupted()) {
		                System.out.println("Interruption reçue. Sortie de la méthode run()...");
		                return;
		            }
		            
		            // Suite du traitement du thread
					List<Float> batiment = new ArrayList<Float>();
					for (int j=0; j<tabWayB[i].getLonLatNodes().length; j++) {
						batiment.add(tabWayB[i].getLonLatNodes()[j][0]);
						batiment.add(tabWayB[i].getLonLatNodes()[j][1]);
					}
					Float [] l = new Float[batiment.size()];
					batiment.toArray(l);
					traitsNodesRemplit(gcBuil,l,"batiment"); 

				}
				System.out.println("---------Fin-Building---------");

			}
		});
		//Thread BuildingOpti
		Thread buildingOpti = new Thread(new Runnable() {
			public void run() {
				//Building
				//System.out.println("---------Building---------");
				for (int a=0; a<4; a++) {
					// Vérifiez si une demande d'interruption a été reçue
		            if (Thread.currentThread().isInterrupted()) {
		                System.out.println("Interruption reçue. Sortie de la méthode run()...");
		                return;
		            }
		            
		            // Suite du traitement du thread
					float latInt = (test[1]-test[0])/2+test[0];
					float lonInt = (test[3]-test[2])/2+test[2];
					if (a==0) {
						System.out.println(test[1]);
						System.out.println(test[0]);
						System.out.println(test[3]);
						System.out.println(test[2]);
						System.out.println(lonInt);
						System.out.println(latInt);
						tabWayB = RequeteAPi.requeteBuilding(latInt, test[0], lonInt, test[2]);
					}
					if (a==1) {
						tabWayB = RequeteAPi.requeteBuilding(test[1], latInt, lonInt, test[2]);
					}
					if (a==2) {
						tabWayB = RequeteAPi.requeteBuilding(latInt, test[0], test[3], lonInt);
					}
					if (a==3) {
						tabWayB = RequeteAPi.requeteBuilding(test[1], latInt, test[3], lonInt);
					}


					//RequeteAPi.affichageConsoleBuilding(tabWayB);
					int nbBat=tabWayB.length;
					for(int i=0;i<nbBat && tabWayB[i] != null ;i++) {
						// Vérifiez si une demande d'interruption a été reçue
			            if (Thread.currentThread().isInterrupted()) {
			                System.out.println("Interruption reçue. Sortie de la méthode run()...");
			                return;
			            }
			            
			            // Suite du traitement du thread
						List<Float> batiment = new ArrayList<Float>();
						for (int j=0; j<tabWayB[i].getLonLatNodes().length; j++) {
							batiment.add(tabWayB[i].getLonLatNodes()[j][0]);
							batiment.add(tabWayB[i].getLonLatNodes()[j][1]);
						}
						Float [] l = new Float[batiment.size()];
						batiment.toArray(l);
						traitsNodesRemplit(gcBuil,l,"batiment"); 

					}
					System.out.println("---------Fin-Building---------");

				}
			}
		});

		//Thread Parc
		Thread parc = new Thread(new Runnable() {
			public void run() {

				Way [] tabWayP = RequeteAPi.requeteParc(test[0], test[1], test[2], test[3]);
				int nbWayP = Way.getNbWay();
				for(int i=0;i<nbWayP && tabWayP[i] != null ;i++) {
					List<Float> parc = new ArrayList<Float>();
					for (int j=0; j<tabWayP[i].getLonLatNodes().length; j++) {
						// Vérifiez si une demande d'interruption a été reçue
			            if (Thread.currentThread().isInterrupted()) {
			                System.out.println("Interruption reçue. Sortie de la méthode run()...");
			                return;
			            }
			            
			            // Suite du traitement du thread
						parc.add(tabWayP[i].getLonLatNodes()[j][0]);
						parc.add(tabWayP[i].getLonLatNodes()[j][1]);
					}
					Float [] l = new Float[parc.size()];
					parc.toArray(l);
					traitsNodesRemplit(gcParc,l,"parc"); 

				}
				System.out.println("---------Fin-Parc---------");
			}
		});


		//Thread Parking
		Thread parking = new Thread(new Runnable() {
			public void run() {
				Way [] tabWayP = RequeteAPi.requeteParking(test[0], test[1], test[2], test[3]);
				int nbWayP = Way.getNbWay();
				for(int i=0;i<nbWayP && tabWayP[i] != null ;i++) {
					List<Float> parking = new ArrayList<Float>();
					for (int j=0; j<tabWayP[i].getLonLatNodes().length; j++) {
						// Vérifiez si une demande d'interruption a été reçue
			            if (Thread.currentThread().isInterrupted()) {
			                System.out.println("Interruption reçue. Sortie de la méthode run()...");
			                return;
			            }
			            
			            // Suite du traitement du thread
						parking.add(tabWayP[i].getLonLatNodes()[j][0]);
						parking.add(tabWayP[i].getLonLatNodes()[j][1]);
					}
					Float [] l = new Float[parking.size()];
					parking.toArray(l);
					traitsNodesRemplit(gcZone,l,"parking"); 

				}
				System.out.println("---------Fin-Parking---------");
			}
		});


		//Thread Farmland
		Thread farmland = new Thread(new Runnable() {
			public void run() {
				Way [] tabWayF = RequeteAPi.requeteFarmland(test[0], test[1], test[2], test[3]);
				int nbWayF = Way.getNbWay();
				for(int i=0;i<nbWayF && tabWayF[i] != null ;i++) {
					List<Float> parking = new ArrayList<Float>();
					for (int j=0; j<tabWayF[i].getLonLatNodes().length; j++) {
						// Vérifiez si une demande d'interruption a été reçue
			            if (Thread.currentThread().isInterrupted()) {
			                System.out.println("Interruption reçue. Sortie de la méthode run()...");
			                return;
			            }
			            
			            // Suite du traitement du thread
						parking.add(tabWayF[i].getLonLatNodes()[j][0]);
						parking.add(tabWayF[i].getLonLatNodes()[j][1]);
					}
					Float [] l = new Float[parking.size()];
					parking.toArray(l);
					traitsNodesRemplit(gc,l,"farmland"); 

				}
				System.out.println("---------Fin-Farmland---------");
			}
		});

		//Thread Foret
		Thread forest = new Thread(new Runnable() {
			public void run() {
				Way [] tabWayF = RequeteAPi.requeteForest(test[0], test[1], test[2], test[3]);
				int nbWayF = Way.getNbWay();
				for(int i=0;i<nbWayF && tabWayF[i] != null ;i++) {
					List<Float> parking = new ArrayList<Float>();
					for (int j=0; j<tabWayF[i].getLonLatNodes().length; j++) {
						// Vérifiez si une demande d'interruption a été reçue
			            if (Thread.currentThread().isInterrupted()) {
			                System.out.println("Interruption reçue. Sortie de la méthode run()...");
			                return;
			            }
			            
			            // Suite du traitement du thread
						parking.add(tabWayF[i].getLonLatNodes()[j][0]);
						parking.add(tabWayF[i].getLonLatNodes()[j][1]);
					}
					Float [] l = new Float[parking.size()];
					parking.toArray(l);
					traitsNodesRemplit(gcParc,l,"forest"); 

				}
				System.out.println("---------Fin-Foret---------");
			}
		});

		//Thread Scrub
		Thread scrub = new Thread(new Runnable() {
			public void run() {
				Way [] tabWayS = RequeteAPi.requeteScrub(test[0], test[1], test[2], test[3]);
				int nbWayS = Way.getNbWay();
				for(int i=0;i<nbWayS && tabWayS[i] != null ;i++) {
					List<Float> scrub = new ArrayList<Float>();
					for (int j=0; j<tabWayS[i].getLonLatNodes().length; j++) {
						// Vérifiez si une demande d'interruption a été reçue
			            if (Thread.currentThread().isInterrupted()) {
			                System.out.println("Interruption reçue. Sortie de la méthode run()...");
			                return;
			            }
			            
			            // Suite du traitement du thread
						scrub.add(tabWayS[i].getLonLatNodes()[j][0]);
						scrub.add(tabWayS[i].getLonLatNodes()[j][1]);
					}
					Float [] l = new Float[scrub.size()];
					scrub.toArray(l);
					traitsNodesRemplit(gc,l,"scrub"); 

				}
				System.out.println("---------Fin-Foret---------");
			}
		});


		//Thread Zone Commercial
		Thread commercial = new Thread(new Runnable() {
			public void run() {
				Way [] tabWayC = RequeteAPi.requeteZoneCommercial(test[0], test[1], test[2], test[3]);
				int nbWayC = Way.getNbWay();
				//System.out.println(tabWayP[0].getIdWay());

				for(int i=0;i<nbWayC && tabWayC[i] != null ;i++) {
					// Vérifiez si une demande d'interruption a été reçue
		            if (Thread.currentThread().isInterrupted()) {
		                System.out.println("Interruption reçue. Sortie de la méthode run()...");
		                return;
		            }
		            
		            // Suite du traitement du thread
					getListe(tabWayC[i].getLonLatNodes());
					traitsZoneCommercial(gcZone,nbNodes,latLon);
				}
				System.out.println("---------Fin-ZoneCommercial---------");
			}
		});


		//Thread Residentiel
		Thread residential = new Thread(new Runnable() {
			public void run() {


				//Residentiel
				List<?> list = RequeteAPi.requeteResidential(test[1], test[0], test[3], test[2]);
				//nbWay = Water.getLonLatNodes().length;
				Relation [][] tabWayR = new Relation[list.size()][100000];
				list.toArray(tabWayR);

				for (int j=0; j<tabWayR.length ;j++) {

					//initialisation
					List<Float> latLon = new ArrayList<Float>();
					boolean [] visite = new boolean [tabWayR[j].length];
					for (int i1=0; i1<tabWayR[j].length; i1++) {
						visite[i1]=false;
						if ( tabWayR[j][i1] == null) {
							visite[i1]=true;
						}
					}
					boolean fin = true;
					int deb = 0;
					for (int i1=0; i1<tabWayR[j].length && fin==true; i1++) {
						if (tabWayR[j][i1]!=null) {
							if (tabWayR[j][i1].getRole()!=null) {
								if (!tabWayR[j][i1].getRole().equals("inner")) {
									deb = i1;
									fin = false;
								}
							}
							else if (tabWayR[j][i1].getRole()==null) {
								deb = i1;
								fin = false;
							}
						}
					}
					visite[deb]=true;
					int pro = deb;
					int sens = 1;

					for (int k=0; k<tabWayR[j][deb].getLonLatNodes().length; k++) {
						latLon.add( tabWayR[j][deb].getLonLatNodes()[k][0]);
						latLon.add( tabWayR[j][deb].getLonLatNodes()[k][1]);
					}
					//fin init
					float min = 9999F;
					while (fin!=true) {
						// Vérifiez si une demande d'interruption a été reçue
			            if (Thread.currentThread().isInterrupted()) {
			                System.out.println("Interruption reçue. Sortie de la méthode run()...");
			                return;
			            }
			            
			            // Suite du traitement du thread
						min = 9999F;
						fin=true;
						boolean quitter = false;

						//Verification de quelle membre prendre
						for (int i=0; i<tabWayR[j].length && tabWayR[j][i]!=null && tabWayR[j][i].getLonLatNodes().length>0 && quitter==false; i++) {
							if (visite[i]==false) {
								//System.out.println("t ; "+tabWayW[j].length);
								if (tabWayR[j][i].getRole()!=null) {
									if (tabWayR[j][i].getRole().equals("inner")) {
										visite[i]=true;
									}
								}

								if (sens == 1) {

									if (tabWayR[j][deb].getLonLatNodes()[tabWayR[j][deb].getLonLatNodes().length-1][0] == tabWayR[j][i].getLonLatNodes()[0][0]){
										pro = i;
										sens = 1;
										quitter = true;
									}
									else if ( tabWayR[j][deb].getLonLatNodes()[tabWayR[j][deb].getLonLatNodes().length-1][0] == tabWayR[j][i].getLonLatNodes()[tabWayR[j][i].getLonLatNodes().length-1][0]) {
										sens = 0;
										pro = i;
										quitter = true;
									}
									float diffx = tabWayR[j][deb].getLonLatNodes()[tabWayR[j][deb].getLonLatNodes().length-1][0] - tabWayR[j][i].getLonLatNodes()[0][0];
									if (diffx<0) {diffx=-diffx;}
									if (diffx<min) {
										min = diffx;
										pro = i;

									}
									diffx = tabWayR[j][deb].getLonLatNodes()[tabWayR[j][deb].getLonLatNodes().length-1][0] - tabWayR[j][i].getLonLatNodes()[tabWayR[j][i].getLonLatNodes().length-1][0];
									if (diffx<0) {diffx=-diffx;}
									if (diffx<min) {
										min = diffx;
										pro = i;

									}
								}
								else {
									float diffx = tabWayR[j][deb].getLonLatNodes()[0][0] - tabWayR[j][i].getLonLatNodes()[0][0];
									if (diffx<0) {diffx=-diffx;}
									if (diffx<min) {
										min = diffx;
										pro = i;

									}
									diffx = tabWayR[j][deb].getLonLatNodes()[0][0] - tabWayR[j][i].getLonLatNodes()[tabWayR[j][i].getLonLatNodes().length-1][0];
									if (diffx<0) {diffx=-diffx;}
									if (diffx<min) {
										min = diffx;
										pro = i;

									}
									if (tabWayR[j][deb].getLonLatNodes()[0][0] == tabWayR[j][i].getLonLatNodes()[0][0]){
										pro = i;
										sens = 1;
										quitter = true;
									}
									else if ( tabWayR[j][deb].getLonLatNodes()[0][0] == tabWayR[j][i].getLonLatNodes()[tabWayR[j][i].getLonLatNodes().length-1][0]) {
										sens = 0;
										pro = i;
										quitter = true;
									}
								}
								fin=false;
							}
						}
						if (deb==pro) {
							fin=true;
						}
						deb=pro;

						visite[deb]=true;
						//on rempli la liste
						if (sens == 0) {
							for (int k=tabWayR[j][deb].getLonLatNodes().length-1; k>=0 && fin==false; k=k-1) {
								latLon.add( tabWayR[j][deb].getLonLatNodes()[k][0]);
								latLon.add( tabWayR[j][deb].getLonLatNodes()[k][1]);
							}
						}
						else if (sens == 1) {
							for (int k=0; k<tabWayR[j][deb].getLonLatNodes().length && fin==false; k=k+1) {
								latLon.add( tabWayR[j][deb].getLonLatNodes()[k][0]);
								latLon.add( tabWayR[j][deb].getLonLatNodes()[k][1]);
							}
						}


					}

					Float [] l = new Float[latLon.size()];
					latLon.toArray(l);
					traitsNodesRemplit(gc,l,"residential"); 			


				}
			}
		});

		//Thread Industriel
		Thread industrial = new Thread(new Runnable() {
			public void run() {


				//Residentiel
				List<?> list = RequeteAPi.requeteIndustrial(test[1], test[0], test[3], test[2]);
				//nbWay = Water.getLonLatNodes().length;
				Relation [][] tabWayR = new Relation[list.size()][100000];
				list.toArray(tabWayR);

				for (int j=0; j<tabWayR.length ;j++) {

					//initialisation
					List<Float> latLon = new ArrayList<Float>();
					boolean [] visite = new boolean [tabWayR[j].length];
					for (int i1=0; i1<tabWayR[j].length; i1++) {
						visite[i1]=false;
						if ( tabWayR[j][i1] == null) {
							visite[i1]=true;
						}
					}
					boolean fin = true;
					int deb = 0;
					for (int i1=0; i1<tabWayR[j].length && fin==true; i1++) {
						if (tabWayR[j][i1]!=null) {
							if (tabWayR[j][i1].getRole()!=null) {
								if (!tabWayR[j][i1].getRole().equals("inner")) {
									deb = i1;
									fin = false;
								}
							}
							else if (tabWayR[j][i1].getRole()==null) {
								deb = i1;
								fin = false;
							}
						}
					}
					visite[deb]=true;
					int pro = deb;
					int sens = 1;

					for (int k=0; k<tabWayR[j][deb].getLonLatNodes().length; k++) {
						latLon.add( tabWayR[j][deb].getLonLatNodes()[k][0]);
						latLon.add( tabWayR[j][deb].getLonLatNodes()[k][1]);
					}
					//fin init
					float min = 9999F;
					while (fin!=true) {
						// Vérifiez si une demande d'interruption a été reçue
			            if (Thread.currentThread().isInterrupted()) {
			                System.out.println("Interruption reçue. Sortie de la méthode run()...");
			                return;
			            }
			            
			            // Suite du traitement du thread
						min = 9999F;
						fin=true;
						boolean quitter = false;

						//Verification de quelle membre prendre
						for (int i=0; i<tabWayR[j].length && tabWayR[j][i]!=null && tabWayR[j][i].getLonLatNodes().length>0 && quitter==false; i++) {
							if (visite[i]==false) {
								//System.out.println("t ; "+tabWayW[j].length);
								if (tabWayR[j][i].getRole()!=null) {
									if (tabWayR[j][i].getRole().equals("inner")) {
										visite[i]=true;
									}
								}

								if (sens == 1) {

									if (tabWayR[j][deb].getLonLatNodes()[tabWayR[j][deb].getLonLatNodes().length-1][0] == tabWayR[j][i].getLonLatNodes()[0][0]){
										pro = i;
										sens = 1;
										quitter = true;
									}
									else if ( tabWayR[j][deb].getLonLatNodes()[tabWayR[j][deb].getLonLatNodes().length-1][0] == tabWayR[j][i].getLonLatNodes()[tabWayR[j][i].getLonLatNodes().length-1][0]) {
										sens = 0;
										pro = i;
										quitter = true;
									}
									float diffx = tabWayR[j][deb].getLonLatNodes()[tabWayR[j][deb].getLonLatNodes().length-1][0] - tabWayR[j][i].getLonLatNodes()[0][0];
									if (diffx<0) {diffx=-diffx;}
									if (diffx<min) {
										min = diffx;
										pro = i;

									}
									diffx = tabWayR[j][deb].getLonLatNodes()[tabWayR[j][deb].getLonLatNodes().length-1][0] - tabWayR[j][i].getLonLatNodes()[tabWayR[j][i].getLonLatNodes().length-1][0];
									if (diffx<0) {diffx=-diffx;}
									if (diffx<min) {
										min = diffx;
										pro = i;

									}
								}
								else {
									float diffx = tabWayR[j][deb].getLonLatNodes()[0][0] - tabWayR[j][i].getLonLatNodes()[0][0];
									if (diffx<0) {diffx=-diffx;}
									if (diffx<min) {
										min = diffx;
										pro = i;

									}
									diffx = tabWayR[j][deb].getLonLatNodes()[0][0] - tabWayR[j][i].getLonLatNodes()[tabWayR[j][i].getLonLatNodes().length-1][0];
									if (diffx<0) {diffx=-diffx;}
									if (diffx<min) {
										min = diffx;
										pro = i;

									}
									if (tabWayR[j][deb].getLonLatNodes()[0][0] == tabWayR[j][i].getLonLatNodes()[0][0]){
										pro = i;
										sens = 1;
										quitter = true;
									}
									else if ( tabWayR[j][deb].getLonLatNodes()[0][0] == tabWayR[j][i].getLonLatNodes()[tabWayR[j][i].getLonLatNodes().length-1][0]) {
										sens = 0;
										pro = i;
										quitter = true;
									}
								}
								fin=false;
							}
						}
						if (deb==pro) {
							fin=true;
						}
						deb=pro;

						visite[deb]=true;
						//on rempli la liste
						if (sens == 0) {
							for (int k=tabWayR[j][deb].getLonLatNodes().length-1; k>=0 && fin==false; k=k-1) {
								latLon.add( tabWayR[j][deb].getLonLatNodes()[k][0]);
								latLon.add( tabWayR[j][deb].getLonLatNodes()[k][1]);
							}
						}
						else if (sens == 1) {
							for (int k=0; k<tabWayR[j][deb].getLonLatNodes().length && fin==false; k=k+1) {
								latLon.add( tabWayR[j][deb].getLonLatNodes()[k][0]);
								latLon.add( tabWayR[j][deb].getLonLatNodes()[k][1]);
							}
						}


					}

					Float [] l = new Float[latLon.size()];
					latLon.toArray(l);
					traitsNodesRemplit(gc,l,"industrial"); 			


				}

				System.out.println("---------Fin-Industrie---------");

				forest.start();
			}
		});

		//Water
		Thread water = new Thread(new Runnable() {
			public void run() {
				//Point d'eau
				List<?> list2 = RequeteAPi.requeteWater2(test[1], test[0], test[3], test[2]);
				//nbWay = Water.getLonLatNodes().length;
				Relation [][] tabWayW = new Relation[list2.size()][100000];
				list2.toArray(tabWayW);

				for (int j=0; j<tabWayW.length ;j++) {

					//initialisation
					List<Float> latLon = new ArrayList<Float>();
					boolean [] visite = new boolean [tabWayW[j].length];
					for (int i1=0; i1<tabWayW[j].length; i1++) {
						visite[i1]=false;
						if ( tabWayW[j][i1] == null) {
							visite[i1]=true;
						}
					}
					boolean fin = true;
					int deb = 0;
					for (int i1=0; i1<tabWayW[j].length && fin==true; i1++) {
						if (tabWayW[j][i1]!=null) {
							if (tabWayW[j][i1].getRole()!=null) {
								if (!tabWayW[j][i1].getRole().equals("inner")) {
									deb = i1;
									fin = false;
								}
							}
							else if (tabWayW[j][i1].getRole()==null) {
								deb = i1;
								fin = false;
							}
						}
					}
					visite[deb]=true;
					int pro = deb;
					int sens = 1;

					for (int k=0; k<tabWayW[j][deb].getLonLatNodes().length; k++) {
						latLon.add( tabWayW[j][deb].getLonLatNodes()[k][0]);
						latLon.add( tabWayW[j][deb].getLonLatNodes()[k][1]);
					}
					//fin init
					float min = 9999F;
					while (fin!=true) {
						// Vérifiez si une demande d'interruption a été reçue
			            if (Thread.currentThread().isInterrupted()) {
			                System.out.println("Interruption reçue. Sortie de la méthode run()...");
			                return;
			            }
			            
			            // Suite du traitement du thread
						min = 9999F;
						fin=true;
						boolean quitter = false;

						//Verification de quelle membre prendre
						for (int i=0; i<tabWayW[j].length && tabWayW[j][i]!=null && tabWayW[j][i].getLonLatNodes().length>0 && quitter==false; i++) {
							if (visite[i]==false) {
								if (tabWayW[j][i].getRole()!=null) {
									if (tabWayW[j][i].getRole().equals("inner")) {
										visite[i]=true;
									}
								}

								if (sens == 1) {

									if (tabWayW[j][deb].getLonLatNodes()[tabWayW[j][deb].getLonLatNodes().length-1][0] == tabWayW[j][i].getLonLatNodes()[0][0]){
										pro = i;
										sens = 1;
										quitter = true;
									}
									else if ( tabWayW[j][deb].getLonLatNodes()[tabWayW[j][deb].getLonLatNodes().length-1][0] == tabWayW[j][i].getLonLatNodes()[tabWayW[j][i].getLonLatNodes().length-1][0]) {
										sens = 0;
										pro = i;
										quitter = true;
									}
									float diffx = tabWayW[j][deb].getLonLatNodes()[tabWayW[j][deb].getLonLatNodes().length-1][0] - tabWayW[j][i].getLonLatNodes()[0][0];
									if (diffx<0) {diffx=-diffx;}
									if (diffx<min) {
										min = diffx;
										pro = i;

									}
									diffx = tabWayW[j][deb].getLonLatNodes()[tabWayW[j][deb].getLonLatNodes().length-1][0] - tabWayW[j][i].getLonLatNodes()[tabWayW[j][i].getLonLatNodes().length-1][0];
									if (diffx<0) {diffx=-diffx;}
									if (diffx<min) {
										min = diffx;
										pro = i;

									}
								}
								else {
									float diffx = tabWayW[j][deb].getLonLatNodes()[0][0] - tabWayW[j][i].getLonLatNodes()[0][0];
									if (diffx<0) {diffx=-diffx;}
									if (diffx<min) {
										min = diffx;
										pro = i;

									}
									diffx = tabWayW[j][deb].getLonLatNodes()[0][0] - tabWayW[j][i].getLonLatNodes()[tabWayW[j][i].getLonLatNodes().length-1][0];
									if (diffx<0) {diffx=-diffx;}
									if (diffx<min) {
										min = diffx;
										pro = i;

									}
									if (tabWayW[j][deb].getLonLatNodes()[0][0] == tabWayW[j][i].getLonLatNodes()[0][0]){
										pro = i;
										sens = 1;
										quitter = true;
									}
									else if ( tabWayW[j][deb].getLonLatNodes()[0][0] == tabWayW[j][i].getLonLatNodes()[tabWayW[j][i].getLonLatNodes().length-1][0]) {
										sens = 0;
										pro = i;
										quitter = true;
									}
								}
								fin=false;
							}
						}
						if (deb==pro) {
							fin=true;
						}
						deb=pro;

						visite[deb]=true;
						//on rempli la liste
						if (sens == 0) {
							for (int k=tabWayW[j][deb].getLonLatNodes().length-1; k>=0 && fin==false; k=k-1) {
								latLon.add( tabWayW[j][deb].getLonLatNodes()[k][0]);
								latLon.add( tabWayW[j][deb].getLonLatNodes()[k][1]);
							}
						}
						else if (sens == 1) {
							for (int k=0; k<tabWayW[j][deb].getLonLatNodes().length && fin==false; k=k+1) {
								latLon.add( tabWayW[j][deb].getLonLatNodes()[k][0]);
								latLon.add( tabWayW[j][deb].getLonLatNodes()[k][1]);
							}
						}


					}

					Float [] l = new Float[latLon.size()];
					latLon.toArray(l);
					traitsNodesRemplit(gcEau,l,"outer"); 			


				}
				System.out.println("---------Fin-PointD'eau-Outer---------");

				//Points d'eau inner
				List<?> list3 = RequeteAPi.requeteWater2(test[1], test[0], test[3], test[2]);
				//nbWay = Water.getLonLatNodes().length;
				tabWayW = new Relation[list3.size()][10000];
				list3.toArray(tabWayW);

				List<Float[]> inner = new ArrayList<Float[]>();
				for (int j=0; j<tabWayW.length && tabWayW[j][0] != null ;j++) {
					for(int i=0; i<tabWayW[j].length && tabWayW[j][i] != null ;i++) {
						if (tabWayW[j][i].getRole()!=null) {
							if (tabWayW[j][i].getRole().equals("inner")) {
								Float [] l = new Float[tabWayW[j][i].getLonLatNodes().length*2];
								int m=0;
								for (int k=0; k<tabWayW[j][i].getLonLatNodes().length; k++) {
									// Vérifiez si une demande d'interruption a été reçue
						            if (Thread.currentThread().isInterrupted()) {
						                System.out.println("Interruption reçue. Sortie de la méthode run()...");
						                return;
						            }
						            
						            // Suite du traitement du thread
									l[m] = tabWayW[j][i].getLonLatNodes()[k][0];
									l[m+1] = tabWayW[j][i].getLonLatNodes()[k][1];
									m=m+2;

								}
								inner.add(l);
							}
						}

					}


					for (int i=0; i<inner.size(); i++) {
						traitsNodesRemplit(gcEau,inner.get(i),"inner"); 
					}
				}
				System.out.println("---------Fin-PointD'eau-Inner---------");
			}
		});

		//On arrete tout les anciens threads
		for (Thread thread : threads) {
		    thread.interrupt();
		}

		
		//On commence à charger l'affichage si la zone à bien été trouvée
		if ( test[0]!=0 && test[1]!=0 && test[2]!=0 && test[3]!=0) {
			threads.add(fond);fond.start();
			threads.add(route);route.start();
			dimAff = convertionCanvas();
			if(dimAff[0]<=0.15 && dimAff[1]<=0.15) {threads.add(building);building.start();}
			else {threads.add(buildingOpti);buildingOpti.start();}
			threads.add(parc);parc.start();
			threads.add(parking);parking.start();
			threads.add(residential);residential.start();
			threads.add(industrial);industrial.start();
			threads.add(water);water.start();
			threads.add(scrub);scrub.start();
			threads.add(commercial);commercial.start();
			threads.add(farmland);farmland.start();
			
			
		}


		System.out.println(minLat);
		System.out.println(maxLat);
		System.out.println(minLon);
		System.out.println(maxLon);

		/*root.getChildren().add(canvas);
        primaryStage.setScene(new Scene(root));
        primaryStage.show();*/

	}




	//Permet de recuperer les lat/lon maximum et minimum  
	public static void getMaxMin(float[] ListeMaxMin) {
		minLat=ListeMaxMin[0];
		maxLat=ListeMaxMin[1];
		minLon=ListeMaxMin[2];
		maxLon=ListeMaxMin[3];
	}



	public static void getListe(float[][] liste) {
		latLon=liste;
		nbNodes=latLon.length;

	}



	//Pour Remplire
	private static void traitsNodesRemplit(GraphicsContext gc,Float[] latLon,String role) {

		float[][] listePix = convertionRoadPourRemplit(latLon);
		gc.setLineWidth(1); //Plus tard largeur route
		gc.setImageSmoothing(false);
		double [] x = new double[listePix.length];
		double [] y = new double[listePix.length];
		for (int i=0; i<listePix.length; i++) {
			x[i] = listePix[i][0];
			y[i] = listePix[i][1];
			if (role.equals("urbain1")){
				System.out.println("1: "+x[i]);
				System.out.println("2: "+y[i]);}
		}
		if (role.equals("outer")){
			gc.setFill(Color.LIGHTBLUE);
			gc.setStroke(Color.LIGHTBLUE);
			gc.fillPolygon(x,y,y.length);
		}
		if (role.equals("batiment")){
			gc.setLineWidth(0.001);
			gc.setFill(Color.GREY);
			gc.setStroke(Color.GREY);
			gc.fillPolygon(x,y,y.length);
		}
		if (role.equals("parc")){
			gc.setLineWidth(0.1);
			gc.setFill(Color.LIGHTGREEN);
			gc.setStroke(Color.GREEN);
			gc.fillPolygon(x,y,y.length);
		}
		if (role.equals("parking")){
			gc.setLineWidth(0.1);
			gc.setFill(Color.LIGHTGREY);
			gc.setStroke(Color.LIGHTGREY);
			gc.fillPolygon(x,y,y.length);
		}
		if (role.equals("farmland")){
			gc.setLineWidth(0.1);
			gc.setFill(Color.LIGHTYELLOW);
			gc.setStroke(Color.LIGHTYELLOW);
			gc.fillPolygon(x,y,y.length);
		}
		if (role.equals("forest")){
			gc.setLineWidth(0.1);
			gc.setFill(Color.GREEN);
			gc.setStroke(Color.GREEN);
			gc.fillPolygon(x,y,y.length);
		}
		if (role.equals("scrub")){
			gc.setLineWidth(0.1);
			gc.setFill(Color.rgb(191, 236, 123));
			gc.setStroke(Color.rgb(191, 236, 123));
			gc.fillPolygon(x,y,y.length);
		}
		if (role.equals("aero")){
			gc.setLineWidth(2);
			gc.setStroke(Color.rgb(168, 116, 183));
			gc.strokePolyline(x,y,y.length);
		}
		if (role.equals("residential")){
			gc.setLineWidth(0.1);
			gc.setFill(Color.rgb(240, 230, 230));
			gc.setStroke(Color.rgb(240, 230, 230));
			gc.fillPolygon(x,y,y.length);
		}
		if (role.equals("industrial")){
			gc.setLineWidth(0.1);
			gc.setFill(Color.rgb(179, 164, 150));
			gc.setStroke(Color.rgb(179, 164, 150));
			gc.fillPolygon(x,y,y.length);
		}
		if (role.equals("rail")){
			gc.setLineWidth(1);
			gc.setStroke(Color.DARKGREY);
			gc.strokePolyline(x,y,y.length);
		}
		if (role.equals("urbain")){
			gc.setLineWidth(3);
			gc.setStroke(Color.YELLOW);
			gc.strokePolyline(x,y,y.length);
		}
		if (role.equals("inner")){
			gc.setLineWidth(1);
			gc.setFill(Color.LIGHTGREEN);
			gc.setStroke(Color.LIGHTGREEN);
			gc.fillPolygon(x,y,y.length);

		}
		int sameWay=0;
		while(sameWay<(x.length-1)) { 
			gc.strokeLine(x[sameWay], y[sameWay], x[sameWay+1], y[sameWay+1]);
			sameWay=sameWay+1;
		}
		gc.setImageSmoothing(false);

	}


	//Trace les traits des differentes nodes des routes
	private static void traitsNodes(GraphicsContext gc,int nbNodes,float[][] latLon, String role) {
		float[][] listePix = convertionRoad(latLon);
		gc.setStroke(Color.BEIGE); //Plus tard couleur route
		gc.setLineWidth(0.5); //Plus tard largeur route
		gc.setFontSmoothingType(FontSmoothingType.GRAY);
		if (role.equals("OneWay")) {
			gc.setStroke(Color.BLACK); //Plus tard couleur route
			gc.setLineWidth(1.0); //Plus tard largeur route
		}
		if (role.equals("secondary")) {
			gc.setStroke(Color.YELLOW); //Plus tard couleur route
			gc.setLineWidth(1.0); //Plus tard largeur route
		}
		if (role.equals("motorway")) {
			gc.setStroke(Color.RED); //Plus tard couleur route
			gc.setLineWidth(1.5); //Plus tard largeur route
		}

		int sameWay=0;
		while(sameWay<(nbNodes-1)) { 
			gc.strokeLine(listePix[sameWay][0], listePix[sameWay][1], listePix[sameWay+1][0], listePix[sameWay+1][1]);
			sameWay=sameWay+1;
		}
		//float[] quelAffichage = convertionCanvas();
		//System.out.println(quelAffichage[0]);
		//System.out.println("\n"+quelAffichage[1]);
	}

	private static void traitsZoneCommercial(GraphicsContext gc,int nbNodes,float[][] latLon) {
		float[][] listePix = convertionRoad(latLon);
		gc.setStroke(Color.PURPLE); //Plus tard couleur route
		gc.setLineWidth(1); //Plus tard largeur route

		int sameWay=0;
		while(sameWay<(nbNodes-1)) { 
			gc.strokeLine(listePix[sameWay][0], listePix[sameWay][1], listePix[sameWay+1][0], listePix[sameWay+1][1]);
			sameWay=sameWay+1;
		}
		//System.out.println(quelAffichage[0]);
		//System.out.println("\n"+quelAffichage[1]);
	}



	//trace l'itineraire
	public static void traitsNodesPourItineraire(GraphicsContext gc,int nbNodes,float[][] latLon,Button  dep,Button arr) {
		float[][] listePix = convertionRoad(latLon);
		gc.setStroke(Color.BLUE); //Plus tard couleur route
		gc.setLineWidth(1); //Plus tard largeur route

		int sameWay=0;
		while(sameWay<(nbNodes-1)) { 
			gc.strokeLine(listePix[sameWay][0], listePix[sameWay][1], listePix[sameWay+1][0], listePix[sameWay+1][1]);
			sameWay=sameWay+1;
		}
		convertionCanvas();

		popPos = new float[4];
		popPos[0] = listePix[0][0];
		popPos[1] = listePix[0][1];
		popPos[2] = listePix[sameWay][0];
		popPos[3] = listePix[sameWay][1];
		//convertionRoad(latLon);
		// System.out.print("listePix[0][0]-sizeXCanvas/2 : "+(listePix[0][0]-sizeXCanvas/2)+"listePix[0][1]-sizeYCanvas/2 : "+(listePix[0][0]-sizeYCanvas/2));
		Affichage.moveDepArr(zoomMax,  dep, arr,  listePix[0][0]-sizeXCanvas/2, listePix[0][1]-sizeYCanvas/2, listePix[sameWay][0]-sizeXCanvas/2,  listePix[sameWay][1]-sizeYCanvas/2);
	}



	//Recupere la distance entre la val max et la val vin de la longitude/latitude  
	private static float [] convertionCanvas() {
		float lonP, latP;
		lonP=maxLon-minLon;
		latP=maxLat-minLat;
		final float conv[] = {lonP, latP};
		return conv;
	}


	//Calcul pour le positionnement de chaque node
	private static float [][] convertionRoad(float[][] cooL) {
		float[] dim= convertionCanvas();
		int i=0;
		float lonI,latI;
		float [][] a= new float[cooL.length][2];

		while(i<cooL.length) {
			lonI=(cooL[i][0]-minLon)*sizeXCanvas/dim[0];
			latI=(maxLat-cooL[i][1])*sizeYCanvas/dim[1];

			a[i][0]=lonI;
			a[i][1]=latI;

			i=i+1;
		}

		return a;
	}

	//tentative foundLonLat 
    public static Double[] foundLonLat(Double x, Double y) {
        float[] dim= convertionCanvas();
        
        Double lonI,latI;
        Double [] a= new Double[2];
        /*System.out.print("x : "+x+" y : "+y);
        System.out.print("sizeXCanvas : "+sizeXCanvas+" sizeYCanvas : "+sizeYCanvas);
        System.out.print("dim[0] : "+dim[0]+" dim[1] : "+dim[1]);*/
        // dom[i] = lonP
        // dim[1] = latP
            lonI=(x*dim[0])/sizeXCanvas;
            latI=(y*dim[1])/sizeYCanvas;
        //System.out.print("lonI : "+lonI+minLon+" latI : "+latI+minLat);
            a[0]=lonI+minLon;
            a[1]=maxLat-latI;
            
        
        return a;
    }
	
	//Calcul pour le positionnement de chaque node
	private static float [][] convertionRoadPourRemplit(Float[] cooL) {
		float[] dim= convertionCanvas();
		int i=0;
		float lonI,latI;
		float [][] a= new float[cooL.length/2][2];
		int j=0;
		while(i<cooL.length) {



			lonI=(cooL[i]-minLon)*sizeXCanvas/dim[0];
			latI=(maxLat-cooL[i+1])*sizeYCanvas/dim[1];

			a[j][0]=lonI;
			a[j][1]=latI;

			if (j>0) {
				float diffx = a[j][0]-a[j-1][0];
				float diffy = a[j][1]-a[j-1][1];
				if (diffy<0) { diffy=-diffy;}
				if (diffx<0) { diffx=-diffx;}
				if ( diffx>1000 || diffy>1000 ) {
					a[j][0]=a[j-1][0];
					a[j][1]=a[j-1][1];
				}
			}
			j++;
			i=i+2;
		}

		return a;
	}



	public void resize(GraphicsContext gc,GraphicsContext gcRoute,GraphicsContext gcParc ,GraphicsContext gcBuil,GraphicsContext gcZone,GraphicsContext gcEau, Canvas canvas){
		//System.out.print((int) canvas.getHeight()+" | " + (int) canvas.getWidth());

		sizeYCanvas = (int) canvas.getHeight();
		sizeXCanvas = (int) canvas.getWidth();


		getMaxMin(test);


		//Couleur de fond
		Thread fond = new Thread(new Runnable() {
			public void run() {
				double [] lx = new double[4];
				double [] ly = new double[4];
				lx[0] = (float) sizeXCanvas;
				ly[0] = 0.0F;
				lx[1] = (float) sizeXCanvas;
				ly[1] = (float) sizeYCanvas;
				lx[2] = 0.0F;
				ly[2] = (float) sizeYCanvas;
				lx[3] = 0.0F;
				ly[3] = 0.0F;
				gc.setFill(Color.LIGHTGREEN);
				gc.setStroke(Color.LIGHTGREEN);
				gc.fillPolygon(lx,ly,4);
			}
		});


		//Thread Rail
		Thread rail = new Thread(new Runnable() {
			public void run() {
				Way [] tabWayR = RequeteAPi.requeteRail(test[0], test[1], test[2], test[3]);
				int nbWayR = Way.getNbWay();
				for(int i=0;i<nbWayR && tabWayR[i] != null ;i++) {
					// Vérifiez si une demande d'interruption a été reçue
		            if (Thread.currentThread().isInterrupted()) {
		                System.out.println("Interruption reçue. Sortie de la méthode run()...");
		                return;
		            }
		            
		            // Suite du traitement du thread
					List<Float> aero = new ArrayList<Float>();
					for (int j=0; j<tabWayR[i].getLonLatNodes().length; j++) {
						aero.add(tabWayR[i].getLonLatNodes()[j][0]);
						aero.add(tabWayR[i].getLonLatNodes()[j][1]);
					}
					Float [] l = new Float[aero.size()];
					aero.toArray(l);
					traitsNodesRemplit(gcRoute,l,"rail"); 

				}
				System.out.println("---------Fin-Rail---------");
			}
		});

		//Thread Aero
		Thread aero = new Thread(new Runnable() {
			public void run() {
				Way [] tabWayA = RequeteAPi.requeteAero(test[0], test[1], test[2], test[3]);
				int nbWayR = Way.getNbWay();
				for(int i=0;i<nbWayR && tabWayA[i] != null ;i++) {
					// Vérifiez si une demande d'interruption a été reçue
		            if (Thread.currentThread().isInterrupted()) {
		                System.out.println("Interruption reçue. Sortie de la méthode run()...");
		                return;
		            }
		            
		            // Suite du traitement du thread
					List<Float> aero = new ArrayList<Float>();
					for (int j=0; j<tabWayA[i].getLonLatNodes().length; j++) {
						aero.add(tabWayA[i].getLonLatNodes()[j][0]);
						aero.add(tabWayA[i].getLonLatNodes()[j][1]);
					}
					Float [] l = new Float[aero.size()];
					aero.toArray(l);
					traitsNodesRemplit(gcRoute,l,"aero"); 

				}
				System.out.println("---------Fin-Aero---------");
			}
		});


		//Thread Route
		Thread route = new Thread(new Runnable() {
			public void run() {

				//Route
				//System.out.println("---------Route---------");
				tabWay = requeteRoute.requeteRoute(test[0], test[1], test[2], test[3]);
				nbWay = Way.getNbWay();
				for(int i=0;i<nbWay && tabWay[i] != null ;i++) {
					// Vérifiez si une demande d'interruption a été reçue
		            if (Thread.currentThread().isInterrupted()) {
		                System.out.println("Interruption reçue. Sortie de la méthode run()...");
		                return;
		            }
		            
		            // Suite du traitement du thread
					Road.getListe(tabWay[i].getLonLatNodes());
					if (tabWay[i].getType()!=null) {
						if (tabWay[i].getType().equals("OneWay")) {
							gc.setStroke(Color.BLACK); //Plus tard couleur route
							gc.setLineWidth(1.0); //Plus tard largeur route
						}
						if (tabWay[i].getType().equals("motorway")){
							traitsNodes(gcRoute,nbNodes,latLon,"motorway");
						}
						if (tabWay[i].getType().equals("secondary")){
							traitsNodes(gcRoute,nbNodes,latLon,"secondary");
						}
					}
					traitsNodes(gcRoute,nbNodes,latLon,"base");

				}
				System.out.println("---------Fin-Route---------");

				rail.start();
				aero.start();
			}
		});

		//Thread Building
		Thread building = new Thread(new Runnable() {
			public void run() {
				//Building
				//System.out.println("---------Building---------");
				tabWayB = RequeteAPi.requeteBuilding(test[1], test[0], test[3], test[2]);
				RequeteAPi.affichageConsoleBuilding(tabWayB);
				int nbBat=tabWayB.length;
				for(int i=0;i<nbBat && tabWayB[i] != null ;i++) {
					// Vérifiez si une demande d'interruption a été reçue
		            if (Thread.currentThread().isInterrupted()) {
		                System.out.println("Interruption reçue. Sortie de la méthode run()...");
		                return;
		            }
		            
		            // Suite du traitement du thread
					List<Float> batiment = new ArrayList<Float>();
					for (int j=0; j<tabWayB[i].getLonLatNodes().length; j++) {
						batiment.add(tabWayB[i].getLonLatNodes()[j][0]);
						batiment.add(tabWayB[i].getLonLatNodes()[j][1]);
					}
					Float [] l = new Float[batiment.size()];
					batiment.toArray(l);
					traitsNodesRemplit(gcBuil,l,"batiment"); 

				}
				System.out.println("---------Fin-Building---------");

			}
		});
		//Thread BuildingOpti
		Thread buildingOpti = new Thread(new Runnable() {
			public void run() {
				//Building
				//System.out.println("---------Building---------");
				for (int a=0; a<4; a++) {
					// Vérifiez si une demande d'interruption a été reçue
		            if (Thread.currentThread().isInterrupted()) {
		                System.out.println("Interruption reçue. Sortie de la méthode run()...");
		                return;
		            }
		            
		            // Suite du traitement du thread
					float latInt = (test[1]-test[0])/2+test[0];
					float lonInt = (test[3]-test[2])/2+test[2];
					if (a==0) {
						System.out.println(test[1]);
						System.out.println(test[0]);
						System.out.println(test[3]);
						System.out.println(test[2]);
						System.out.println(lonInt);
						System.out.println(latInt);
						tabWayB = RequeteAPi.requeteBuilding(latInt, test[0], lonInt, test[2]);
					}
					if (a==1) {
						tabWayB = RequeteAPi.requeteBuilding(test[1], latInt, lonInt, test[2]);
					}
					if (a==2) {
						tabWayB = RequeteAPi.requeteBuilding(latInt, test[0], test[3], lonInt);
					}
					if (a==3) {
						tabWayB = RequeteAPi.requeteBuilding(test[1], latInt, test[3], lonInt);
					}


					//RequeteAPi.affichageConsoleBuilding(tabWayB);
					int nbBat=tabWayB.length;
					for(int i=0;i<nbBat && tabWayB[i] != null ;i++) {
						// Vérifiez si une demande d'interruption a été reçue
			            if (Thread.currentThread().isInterrupted()) {
			                System.out.println("Interruption reçue. Sortie de la méthode run()...");
			                return;
			            }
			            
			            // Suite du traitement du thread
						List<Float> batiment = new ArrayList<Float>();
						for (int j=0; j<tabWayB[i].getLonLatNodes().length; j++) {
							batiment.add(tabWayB[i].getLonLatNodes()[j][0]);
							batiment.add(tabWayB[i].getLonLatNodes()[j][1]);
						}
						Float [] l = new Float[batiment.size()];
						batiment.toArray(l);
						traitsNodesRemplit(gcBuil,l,"batiment"); 

					}
					System.out.println("---------Fin-Building---------");

				}
			}
		});

		//Thread Parc
		Thread parc = new Thread(new Runnable() {
			public void run() {

				Way [] tabWayP = RequeteAPi.requeteParc(test[0], test[1], test[2], test[3]);
				int nbWayP = Way.getNbWay();
				for(int i=0;i<nbWayP && tabWayP[i] != null ;i++) {
					List<Float> parc = new ArrayList<Float>();
					for (int j=0; j<tabWayP[i].getLonLatNodes().length; j++) {
						// Vérifiez si une demande d'interruption a été reçue
			            if (Thread.currentThread().isInterrupted()) {
			                System.out.println("Interruption reçue. Sortie de la méthode run()...");
			                return;
			            }
			            
			            // Suite du traitement du thread
						parc.add(tabWayP[i].getLonLatNodes()[j][0]);
						parc.add(tabWayP[i].getLonLatNodes()[j][1]);
					}
					Float [] l = new Float[parc.size()];
					parc.toArray(l);
					traitsNodesRemplit(gcParc,l,"parc"); 

				}
				System.out.println("---------Fin-Parc---------");
			}
		});


		//Thread Parking
		Thread parking = new Thread(new Runnable() {
			public void run() {
				Way [] tabWayP = RequeteAPi.requeteParking(test[0], test[1], test[2], test[3]);
				int nbWayP = Way.getNbWay();
				for(int i=0;i<nbWayP && tabWayP[i] != null ;i++) {
					List<Float> parking = new ArrayList<Float>();
					for (int j=0; j<tabWayP[i].getLonLatNodes().length; j++) {
						// Vérifiez si une demande d'interruption a été reçue
			            if (Thread.currentThread().isInterrupted()) {
			                System.out.println("Interruption reçue. Sortie de la méthode run()...");
			                return;
			            }
			            
			            // Suite du traitement du thread
						parking.add(tabWayP[i].getLonLatNodes()[j][0]);
						parking.add(tabWayP[i].getLonLatNodes()[j][1]);
					}
					Float [] l = new Float[parking.size()];
					parking.toArray(l);
					traitsNodesRemplit(gcZone,l,"parking"); 

				}
				System.out.println("---------Fin-Parking---------");
			}
		});


		//Thread Farmland
		Thread farmland = new Thread(new Runnable() {
			public void run() {
				Way [] tabWayF = RequeteAPi.requeteFarmland(test[0], test[1], test[2], test[3]);
				int nbWayF = Way.getNbWay();
				for(int i=0;i<nbWayF && tabWayF[i] != null ;i++) {
					List<Float> parking = new ArrayList<Float>();
					for (int j=0; j<tabWayF[i].getLonLatNodes().length; j++) {
						// Vérifiez si une demande d'interruption a été reçue
			            if (Thread.currentThread().isInterrupted()) {
			                System.out.println("Interruption reçue. Sortie de la méthode run()...");
			                return;
			            }
			            
			            // Suite du traitement du thread
						parking.add(tabWayF[i].getLonLatNodes()[j][0]);
						parking.add(tabWayF[i].getLonLatNodes()[j][1]);
					}
					Float [] l = new Float[parking.size()];
					parking.toArray(l);
					traitsNodesRemplit(gc,l,"farmland"); 

				}
				System.out.println("---------Fin-Farmland---------");
			}
		});

		//Thread Foret
		Thread forest = new Thread(new Runnable() {
			public void run() {
				Way [] tabWayF = RequeteAPi.requeteForest(test[0], test[1], test[2], test[3]);
				int nbWayF = Way.getNbWay();
				for(int i=0;i<nbWayF && tabWayF[i] != null ;i++) {
					List<Float> parking = new ArrayList<Float>();
					for (int j=0; j<tabWayF[i].getLonLatNodes().length; j++) {
						// Vérifiez si une demande d'interruption a été reçue
			            if (Thread.currentThread().isInterrupted()) {
			                System.out.println("Interruption reçue. Sortie de la méthode run()...");
			                return;
			            }
			            
			            // Suite du traitement du thread
						parking.add(tabWayF[i].getLonLatNodes()[j][0]);
						parking.add(tabWayF[i].getLonLatNodes()[j][1]);
					}
					Float [] l = new Float[parking.size()];
					parking.toArray(l);
					traitsNodesRemplit(gcParc,l,"forest"); 

				}
				System.out.println("---------Fin-Foret---------");
			}
		});

		//Thread Scrub
		Thread scrub = new Thread(new Runnable() {
			public void run() {
				Way [] tabWayS = RequeteAPi.requeteScrub(test[0], test[1], test[2], test[3]);
				int nbWayS = Way.getNbWay();
				for(int i=0;i<nbWayS && tabWayS[i] != null ;i++) {
					List<Float> scrub = new ArrayList<Float>();
					for (int j=0; j<tabWayS[i].getLonLatNodes().length; j++) {
						// Vérifiez si une demande d'interruption a été reçue
			            if (Thread.currentThread().isInterrupted()) {
			                System.out.println("Interruption reçue. Sortie de la méthode run()...");
			                return;
			            }
			            
			            // Suite du traitement du thread
						scrub.add(tabWayS[i].getLonLatNodes()[j][0]);
						scrub.add(tabWayS[i].getLonLatNodes()[j][1]);
					}
					Float [] l = new Float[scrub.size()];
					scrub.toArray(l);
					traitsNodesRemplit(gc,l,"scrub"); 

				}
				System.out.println("---------Fin-Foret---------");
			}
		});


		//Thread Zone Commercial
		Thread commercial = new Thread(new Runnable() {
			public void run() {
				Way [] tabWayC = RequeteAPi.requeteZoneCommercial(test[0], test[1], test[2], test[3]);
				int nbWayC = Way.getNbWay();
				//System.out.println(tabWayP[0].getIdWay());

				for(int i=0;i<nbWayC && tabWayC[i] != null ;i++) {
					// Vérifiez si une demande d'interruption a été reçue
		            if (Thread.currentThread().isInterrupted()) {
		                System.out.println("Interruption reçue. Sortie de la méthode run()...");
		                return;
		            }
		            
		            // Suite du traitement du thread
					getListe(tabWayC[i].getLonLatNodes());
					traitsZoneCommercial(gcZone,nbNodes,latLon);
				}
				System.out.println("---------Fin-ZoneCommercial---------");
			}
		});


		//Thread Residentiel
		Thread residential = new Thread(new Runnable() {
			public void run() {


				//Residentiel
				List<?> list = RequeteAPi.requeteResidential(test[1], test[0], test[3], test[2]);
				//nbWay = Water.getLonLatNodes().length;
				Relation [][] tabWayR = new Relation[list.size()][100000];
				list.toArray(tabWayR);

				for (int j=0; j<tabWayR.length ;j++) {

					//initialisation
					List<Float> latLon = new ArrayList<Float>();
					boolean [] visite = new boolean [tabWayR[j].length];
					for (int i1=0; i1<tabWayR[j].length; i1++) {
						visite[i1]=false;
						if ( tabWayR[j][i1] == null) {
							visite[i1]=true;
						}
					}
					boolean fin = true;
					int deb = 0;
					for (int i1=0; i1<tabWayR[j].length && fin==true; i1++) {
						if (tabWayR[j][i1]!=null) {
							if (tabWayR[j][i1].getRole()!=null) {
								if (!tabWayR[j][i1].getRole().equals("inner")) {
									deb = i1;
									fin = false;
								}
							}
							else if (tabWayR[j][i1].getRole()==null) {
								deb = i1;
								fin = false;
							}
						}
					}
					visite[deb]=true;
					int pro = deb;
					int sens = 1;

					for (int k=0; k<tabWayR[j][deb].getLonLatNodes().length; k++) {
						latLon.add( tabWayR[j][deb].getLonLatNodes()[k][0]);
						latLon.add( tabWayR[j][deb].getLonLatNodes()[k][1]);
					}
					//fin init
					float min = 9999F;
					while (fin!=true) {
						// Vérifiez si une demande d'interruption a été reçue
			            if (Thread.currentThread().isInterrupted()) {
			                System.out.println("Interruption reçue. Sortie de la méthode run()...");
			                return;
			            }
			            
			            // Suite du traitement du thread
						min = 9999F;
						fin=true;
						boolean quitter = false;

						//Verification de quelle membre prendre
						for (int i=0; i<tabWayR[j].length && tabWayR[j][i]!=null && tabWayR[j][i].getLonLatNodes().length>0 && quitter==false; i++) {
							if (visite[i]==false) {
								//System.out.println("t ; "+tabWayW[j].length);
								if (tabWayR[j][i].getRole()!=null) {
									if (tabWayR[j][i].getRole().equals("inner")) {
										visite[i]=true;
									}
								}

								if (sens == 1) {

									if (tabWayR[j][deb].getLonLatNodes()[tabWayR[j][deb].getLonLatNodes().length-1][0] == tabWayR[j][i].getLonLatNodes()[0][0]){
										pro = i;
										sens = 1;
										quitter = true;
									}
									else if ( tabWayR[j][deb].getLonLatNodes()[tabWayR[j][deb].getLonLatNodes().length-1][0] == tabWayR[j][i].getLonLatNodes()[tabWayR[j][i].getLonLatNodes().length-1][0]) {
										sens = 0;
										pro = i;
										quitter = true;
									}
									float diffx = tabWayR[j][deb].getLonLatNodes()[tabWayR[j][deb].getLonLatNodes().length-1][0] - tabWayR[j][i].getLonLatNodes()[0][0];
									if (diffx<0) {diffx=-diffx;}
									if (diffx<min) {
										min = diffx;
										pro = i;

									}
									diffx = tabWayR[j][deb].getLonLatNodes()[tabWayR[j][deb].getLonLatNodes().length-1][0] - tabWayR[j][i].getLonLatNodes()[tabWayR[j][i].getLonLatNodes().length-1][0];
									if (diffx<0) {diffx=-diffx;}
									if (diffx<min) {
										min = diffx;
										pro = i;

									}
								}
								else {
									float diffx = tabWayR[j][deb].getLonLatNodes()[0][0] - tabWayR[j][i].getLonLatNodes()[0][0];
									if (diffx<0) {diffx=-diffx;}
									if (diffx<min) {
										min = diffx;
										pro = i;

									}
									diffx = tabWayR[j][deb].getLonLatNodes()[0][0] - tabWayR[j][i].getLonLatNodes()[tabWayR[j][i].getLonLatNodes().length-1][0];
									if (diffx<0) {diffx=-diffx;}
									if (diffx<min) {
										min = diffx;
										pro = i;

									}
									if (tabWayR[j][deb].getLonLatNodes()[0][0] == tabWayR[j][i].getLonLatNodes()[0][0]){
										pro = i;
										sens = 1;
										quitter = true;
									}
									else if ( tabWayR[j][deb].getLonLatNodes()[0][0] == tabWayR[j][i].getLonLatNodes()[tabWayR[j][i].getLonLatNodes().length-1][0]) {
										sens = 0;
										pro = i;
										quitter = true;
									}
								}
								fin=false;
							}
						}
						if (deb==pro) {
							fin=true;
						}
						deb=pro;

						visite[deb]=true;
						//on rempli la liste
						if (sens == 0) {
							for (int k=tabWayR[j][deb].getLonLatNodes().length-1; k>=0 && fin==false; k=k-1) {
								latLon.add( tabWayR[j][deb].getLonLatNodes()[k][0]);
								latLon.add( tabWayR[j][deb].getLonLatNodes()[k][1]);
							}
						}
						else if (sens == 1) {
							for (int k=0; k<tabWayR[j][deb].getLonLatNodes().length && fin==false; k=k+1) {
								latLon.add( tabWayR[j][deb].getLonLatNodes()[k][0]);
								latLon.add( tabWayR[j][deb].getLonLatNodes()[k][1]);
							}
						}


					}

					Float [] l = new Float[latLon.size()];
					latLon.toArray(l);
					traitsNodesRemplit(gc,l,"residential"); 			


				}
			}
		});

		//Thread Industriel
		Thread industrial = new Thread(new Runnable() {
			public void run() {


				//Residentiel
				List<?> list = RequeteAPi.requeteIndustrial(test[1], test[0], test[3], test[2]);
				//nbWay = Water.getLonLatNodes().length;
				Relation [][] tabWayR = new Relation[list.size()][100000];
				list.toArray(tabWayR);

				for (int j=0; j<tabWayR.length ;j++) {

					//initialisation
					List<Float> latLon = new ArrayList<Float>();
					boolean [] visite = new boolean [tabWayR[j].length];
					for (int i1=0; i1<tabWayR[j].length; i1++) {
						visite[i1]=false;
						if ( tabWayR[j][i1] == null) {
							visite[i1]=true;
						}
					}
					boolean fin = true;
					int deb = 0;
					for (int i1=0; i1<tabWayR[j].length && fin==true; i1++) {
						if (tabWayR[j][i1]!=null) {
							if (tabWayR[j][i1].getRole()!=null) {
								if (!tabWayR[j][i1].getRole().equals("inner")) {
									deb = i1;
									fin = false;
								}
							}
							else if (tabWayR[j][i1].getRole()==null) {
								deb = i1;
								fin = false;
							}
						}
					}
					visite[deb]=true;
					int pro = deb;
					int sens = 1;

					for (int k=0; k<tabWayR[j][deb].getLonLatNodes().length; k++) {
						latLon.add( tabWayR[j][deb].getLonLatNodes()[k][0]);
						latLon.add( tabWayR[j][deb].getLonLatNodes()[k][1]);
					}
					//fin init
					float min = 9999F;
					while (fin!=true) {
						// Vérifiez si une demande d'interruption a été reçue
			            if (Thread.currentThread().isInterrupted()) {
			                System.out.println("Interruption reçue. Sortie de la méthode run()...");
			                return;
			            }
			            
			            // Suite du traitement du thread
						min = 9999F;
						fin=true;
						boolean quitter = false;

						//Verification de quelle membre prendre
						for (int i=0; i<tabWayR[j].length && tabWayR[j][i]!=null && tabWayR[j][i].getLonLatNodes().length>0 && quitter==false; i++) {
							if (visite[i]==false) {
								//System.out.println("t ; "+tabWayW[j].length);
								if (tabWayR[j][i].getRole()!=null) {
									if (tabWayR[j][i].getRole().equals("inner")) {
										visite[i]=true;
									}
								}

								if (sens == 1) {

									if (tabWayR[j][deb].getLonLatNodes()[tabWayR[j][deb].getLonLatNodes().length-1][0] == tabWayR[j][i].getLonLatNodes()[0][0]){
										pro = i;
										sens = 1;
										quitter = true;
									}
									else if ( tabWayR[j][deb].getLonLatNodes()[tabWayR[j][deb].getLonLatNodes().length-1][0] == tabWayR[j][i].getLonLatNodes()[tabWayR[j][i].getLonLatNodes().length-1][0]) {
										sens = 0;
										pro = i;
										quitter = true;
									}
									float diffx = tabWayR[j][deb].getLonLatNodes()[tabWayR[j][deb].getLonLatNodes().length-1][0] - tabWayR[j][i].getLonLatNodes()[0][0];
									if (diffx<0) {diffx=-diffx;}
									if (diffx<min) {
										min = diffx;
										pro = i;

									}
									diffx = tabWayR[j][deb].getLonLatNodes()[tabWayR[j][deb].getLonLatNodes().length-1][0] - tabWayR[j][i].getLonLatNodes()[tabWayR[j][i].getLonLatNodes().length-1][0];
									if (diffx<0) {diffx=-diffx;}
									if (diffx<min) {
										min = diffx;
										pro = i;

									}
								}
								else {
									float diffx = tabWayR[j][deb].getLonLatNodes()[0][0] - tabWayR[j][i].getLonLatNodes()[0][0];
									if (diffx<0) {diffx=-diffx;}
									if (diffx<min) {
										min = diffx;
										pro = i;

									}
									diffx = tabWayR[j][deb].getLonLatNodes()[0][0] - tabWayR[j][i].getLonLatNodes()[tabWayR[j][i].getLonLatNodes().length-1][0];
									if (diffx<0) {diffx=-diffx;}
									if (diffx<min) {
										min = diffx;
										pro = i;

									}
									if (tabWayR[j][deb].getLonLatNodes()[0][0] == tabWayR[j][i].getLonLatNodes()[0][0]){
										pro = i;
										sens = 1;
										quitter = true;
									}
									else if ( tabWayR[j][deb].getLonLatNodes()[0][0] == tabWayR[j][i].getLonLatNodes()[tabWayR[j][i].getLonLatNodes().length-1][0]) {
										sens = 0;
										pro = i;
										quitter = true;
									}
								}
								fin=false;
							}
						}
						if (deb==pro) {
							fin=true;
						}
						deb=pro;

						visite[deb]=true;
						//on rempli la liste
						if (sens == 0) {
							for (int k=tabWayR[j][deb].getLonLatNodes().length-1; k>=0 && fin==false; k=k-1) {
								latLon.add( tabWayR[j][deb].getLonLatNodes()[k][0]);
								latLon.add( tabWayR[j][deb].getLonLatNodes()[k][1]);
							}
						}
						else if (sens == 1) {
							for (int k=0; k<tabWayR[j][deb].getLonLatNodes().length && fin==false; k=k+1) {
								latLon.add( tabWayR[j][deb].getLonLatNodes()[k][0]);
								latLon.add( tabWayR[j][deb].getLonLatNodes()[k][1]);
							}
						}


					}

					Float [] l = new Float[latLon.size()];
					latLon.toArray(l);
					traitsNodesRemplit(gc,l,"industrial"); 			


				}

				System.out.println("---------Fin-Industrie---------");

				forest.start();
			}
		});

		//Water
		Thread water = new Thread(new Runnable() {
			public void run() {
				//Point d'eau
				List<?> list2 = RequeteAPi.requeteWater2(test[1], test[0], test[3], test[2]);
				//nbWay = Water.getLonLatNodes().length;
				Relation [][] tabWayW = new Relation[list2.size()][100000];
				list2.toArray(tabWayW);

				for (int j=0; j<tabWayW.length ;j++) {

					//initialisation
					List<Float> latLon = new ArrayList<Float>();
					boolean [] visite = new boolean [tabWayW[j].length];
					for (int i1=0; i1<tabWayW[j].length; i1++) {
						visite[i1]=false;
						if ( tabWayW[j][i1] == null) {
							visite[i1]=true;
						}
					}
					boolean fin = true;
					int deb = 0;
					for (int i1=0; i1<tabWayW[j].length && fin==true; i1++) {
						if (tabWayW[j][i1]!=null) {
							if (tabWayW[j][i1].getRole()!=null) {
								if (!tabWayW[j][i1].getRole().equals("inner")) {
									deb = i1;
									fin = false;
								}
							}
							else if (tabWayW[j][i1].getRole()==null) {
								deb = i1;
								fin = false;
							}
						}
					}
					visite[deb]=true;
					int pro = deb;
					int sens = 1;

					for (int k=0; k<tabWayW[j][deb].getLonLatNodes().length; k++) {
						latLon.add( tabWayW[j][deb].getLonLatNodes()[k][0]);
						latLon.add( tabWayW[j][deb].getLonLatNodes()[k][1]);
					}
					//fin init
					float min = 9999F;
					while (fin!=true) {
						// Vérifiez si une demande d'interruption a été reçue
			            if (Thread.currentThread().isInterrupted()) {
			                System.out.println("Interruption reçue. Sortie de la méthode run()...");
			                return;
			            }
			            
			            // Suite du traitement du thread
						min = 9999F;
						fin=true;
						boolean quitter = false;

						//Verification de quelle membre prendre
						for (int i=0; i<tabWayW[j].length && tabWayW[j][i]!=null && tabWayW[j][i].getLonLatNodes().length>0 && quitter==false; i++) {
							if (visite[i]==false) {
								if (tabWayW[j][i].getRole()!=null) {
									if (tabWayW[j][i].getRole().equals("inner")) {
										visite[i]=true;
									}
								}

								if (sens == 1) {

									if (tabWayW[j][deb].getLonLatNodes()[tabWayW[j][deb].getLonLatNodes().length-1][0] == tabWayW[j][i].getLonLatNodes()[0][0]){
										pro = i;
										sens = 1;
										quitter = true;
									}
									else if ( tabWayW[j][deb].getLonLatNodes()[tabWayW[j][deb].getLonLatNodes().length-1][0] == tabWayW[j][i].getLonLatNodes()[tabWayW[j][i].getLonLatNodes().length-1][0]) {
										sens = 0;
										pro = i;
										quitter = true;
									}
									float diffx = tabWayW[j][deb].getLonLatNodes()[tabWayW[j][deb].getLonLatNodes().length-1][0] - tabWayW[j][i].getLonLatNodes()[0][0];
									if (diffx<0) {diffx=-diffx;}
									if (diffx<min) {
										min = diffx;
										pro = i;

									}
									diffx = tabWayW[j][deb].getLonLatNodes()[tabWayW[j][deb].getLonLatNodes().length-1][0] - tabWayW[j][i].getLonLatNodes()[tabWayW[j][i].getLonLatNodes().length-1][0];
									if (diffx<0) {diffx=-diffx;}
									if (diffx<min) {
										min = diffx;
										pro = i;

									}
								}
								else {
									float diffx = tabWayW[j][deb].getLonLatNodes()[0][0] - tabWayW[j][i].getLonLatNodes()[0][0];
									if (diffx<0) {diffx=-diffx;}
									if (diffx<min) {
										min = diffx;
										pro = i;

									}
									diffx = tabWayW[j][deb].getLonLatNodes()[0][0] - tabWayW[j][i].getLonLatNodes()[tabWayW[j][i].getLonLatNodes().length-1][0];
									if (diffx<0) {diffx=-diffx;}
									if (diffx<min) {
										min = diffx;
										pro = i;

									}
									if (tabWayW[j][deb].getLonLatNodes()[0][0] == tabWayW[j][i].getLonLatNodes()[0][0]){
										pro = i;
										sens = 1;
										quitter = true;
									}
									else if ( tabWayW[j][deb].getLonLatNodes()[0][0] == tabWayW[j][i].getLonLatNodes()[tabWayW[j][i].getLonLatNodes().length-1][0]) {
										sens = 0;
										pro = i;
										quitter = true;
									}
								}
								fin=false;
							}
						}
						if (deb==pro) {
							fin=true;
						}
						deb=pro;

						visite[deb]=true;
						//on rempli la liste
						if (sens == 0) {
							for (int k=tabWayW[j][deb].getLonLatNodes().length-1; k>=0 && fin==false; k=k-1) {
								latLon.add( tabWayW[j][deb].getLonLatNodes()[k][0]);
								latLon.add( tabWayW[j][deb].getLonLatNodes()[k][1]);
							}
						}
						else if (sens == 1) {
							for (int k=0; k<tabWayW[j][deb].getLonLatNodes().length && fin==false; k=k+1) {
								latLon.add( tabWayW[j][deb].getLonLatNodes()[k][0]);
								latLon.add( tabWayW[j][deb].getLonLatNodes()[k][1]);
							}
						}


					}

					Float [] l = new Float[latLon.size()];
					latLon.toArray(l);
					traitsNodesRemplit(gcEau,l,"outer"); 			


				}
				System.out.println("---------Fin-PointD'eau-Outer---------");

				//Points d'eau inner
				List<?> list3 = RequeteAPi.requeteWater2(test[1], test[0], test[3], test[2]);
				//nbWay = Water.getLonLatNodes().length;
				tabWayW = new Relation[list3.size()][10000];
				list3.toArray(tabWayW);

				List<Float[]> inner = new ArrayList<Float[]>();
				for (int j=0; j<tabWayW.length && tabWayW[j][0] != null ;j++) {
					for(int i=0; i<tabWayW[j].length && tabWayW[j][i] != null ;i++) {
						if (tabWayW[j][i].getRole()!=null) {
							if (tabWayW[j][i].getRole().equals("inner")) {
								Float [] l = new Float[tabWayW[j][i].getLonLatNodes().length*2];
								int m=0;
								for (int k=0; k<tabWayW[j][i].getLonLatNodes().length; k++) {
									// Vérifiez si une demande d'interruption a été reçue
						            if (Thread.currentThread().isInterrupted()) {
						                System.out.println("Interruption reçue. Sortie de la méthode run()...");
						                return;
						            }
						            
						            // Suite du traitement du thread
									l[m] = tabWayW[j][i].getLonLatNodes()[k][0];
									l[m+1] = tabWayW[j][i].getLonLatNodes()[k][1];
									m=m+2;

								}
								inner.add(l);
							}
						}

					}


					for (int i=0; i<inner.size(); i++) {
						traitsNodesRemplit(gcEau,inner.get(i),"inner"); 
					}
				}
				System.out.println("---------Fin-PointD'eau-Inner---------");
			}
		});

		//On arrete tout les anciens threads
		for (Thread thread : threads) {
		    thread.interrupt();
		}

		
		//On commence à charger l'affichage si la zone à bien été trouvée
		if ( test[0]!=0 && test[1]!=0 && test[2]!=0 && test[3]!=0) {
			threads.add(fond);fond.start();
			threads.add(route);route.start();
			dimAff = convertionCanvas();
			if(dimAff[0]<=0.15 && dimAff[1]<=0.15) {threads.add(building);building.start();}
			else {threads.add(buildingOpti);buildingOpti.start();}
			threads.add(parc);parc.start();
			threads.add(parking);parking.start();
			threads.add(residential);residential.start();
			threads.add(industrial);industrial.start();
			threads.add(water);water.start();
			threads.add(scrub);scrub.start();
			threads.add(commercial);commercial.start();
			threads.add(farmland);farmland.start();
			
			
		}

	}




	/*
	 * Fonction de resize 
	 * Permet de recharger la carte entierement 
	 * 
	 */






	public void resize(GraphicsContext gc,GraphicsContext gcRoute,GraphicsContext gcParc ,GraphicsContext gcBuil,GraphicsContext gcZone,GraphicsContext gcEau, Canvas canvas,Itineraire itieraireTrouve ,Button  dep,Button arr)
	{
		//System.out.print((int) canvas.getHeight() +" | "+ (int) canvas.getWidth());
		sizeYCanvas = (int) canvas.getHeight();
		sizeXCanvas = (int) canvas.getWidth();


		// TextField fieldVilleRech = null;
		//float[] test = RequeteAPi.requeteCarreAffichage(fieldVilleRech.getText() );
		// test = RequeteAPi.RequeteAPiItineraire(fieldVilleRechDep.getText(),fieldVilleRechArr.getText());
		//float[] test = RequeteAPi.RequeteAPiItineraire("Avignon","Mazan");

		getMaxMin(test);

		//Couleur de fond
		Thread fond = new Thread(new Runnable() {
			public void run() {
				double [] lx = new double[4];
				double [] ly = new double[4];
				lx[0] = (float) sizeXCanvas;
				ly[0] = 0.0F;
				lx[1] = (float) sizeXCanvas;
				ly[1] = (float) sizeYCanvas;
				lx[2] = 0.0F;
				ly[2] = (float) sizeYCanvas;
				lx[3] = 0.0F;
				ly[3] = 0.0F;
				gc.setFill(Color.LIGHTGREEN);
				gc.setStroke(Color.LIGHTGREEN);
				gc.fillPolygon(lx,ly,4);
			}
		});


		//Thread Rail
		Thread rail = new Thread(new Runnable() {
			public void run() {
				Way [] tabWayR = RequeteAPi.requeteRail(test[0], test[1], test[2], test[3]);
				int nbWayR = Way.getNbWay();
				for(int i=0;i<nbWayR && tabWayR[i] != null ;i++) {
					// Vérifiez si une demande d'interruption a été reçue
		            if (Thread.currentThread().isInterrupted()) {
		                System.out.println("Interruption reçue. Sortie de la méthode run()...");
		                return;
		            }
		            
		            // Suite du traitement du thread
					List<Float> aero = new ArrayList<Float>();
					for (int j=0; j<tabWayR[i].getLonLatNodes().length; j++) {
						aero.add(tabWayR[i].getLonLatNodes()[j][0]);
						aero.add(tabWayR[i].getLonLatNodes()[j][1]);
					}
					Float [] l = new Float[aero.size()];
					aero.toArray(l);
					traitsNodesRemplit(gcRoute,l,"rail"); 

				}
				System.out.println("---------Fin-Rail---------");
			}
		});

		//Thread Aero
		Thread aero = new Thread(new Runnable() {
			public void run() {
				Way [] tabWayA = RequeteAPi.requeteAero(test[0], test[1], test[2], test[3]);
				int nbWayR = Way.getNbWay();
				for(int i=0;i<nbWayR && tabWayA[i] != null ;i++) {
					// Vérifiez si une demande d'interruption a été reçue
		            if (Thread.currentThread().isInterrupted()) {
		                System.out.println("Interruption reçue. Sortie de la méthode run()...");
		                return;
		            }
		            
		            // Suite du traitement du thread
					List<Float> aero = new ArrayList<Float>();
					for (int j=0; j<tabWayA[i].getLonLatNodes().length; j++) {
						aero.add(tabWayA[i].getLonLatNodes()[j][0]);
						aero.add(tabWayA[i].getLonLatNodes()[j][1]);
					}
					Float [] l = new Float[aero.size()];
					aero.toArray(l);
					traitsNodesRemplit(gcRoute,l,"aero"); 

				}
				System.out.println("---------Fin-Aero---------");
			}
		});


		//Thread Route
		Thread route = new Thread(new Runnable() {
			public void run() {

				//Route
				//System.out.println("---------Route---------");
				tabWay = requeteRoute.requeteRoute(test[0], test[1], test[2], test[3]);
				nbWay = Way.getNbWay();
				for(int i=0;i<nbWay && tabWay[i] != null ;i++) {
					// Vérifiez si une demande d'interruption a été reçue
		            if (Thread.currentThread().isInterrupted()) {
		                System.out.println("Interruption reçue. Sortie de la méthode run()...");
		                return;
		            }
		            
		            // Suite du traitement du thread
					Road.getListe(tabWay[i].getLonLatNodes());
					if (tabWay[i].getType()!=null) {
						if (tabWay[i].getType().equals("motorway")){
							traitsNodes(gcRoute,nbNodes,latLon,"motorway");
						}
						if (tabWay[i].getType().equals("secondary")){
							traitsNodes(gcRoute,nbNodes,latLon,"secondary");
						}
					}
					traitsNodes(gcRoute,nbNodes,latLon,"base");

				}
				//cette ligne affiche l'itineraire
				if (itieraireTrouve!=null) {traitsNodesPourItineraire(gcRoute, itieraireTrouve.getLonLatNodes().length, itieraireTrouve.getLonLatNodes(),  dep, arr);}

				System.out.println("---------Fin-Route---------");

				rail.start();
				aero.start();
			}
		});

		//Thread Building
		Thread building = new Thread(new Runnable() {
			public void run() {
				//Building
				//System.out.println("---------Building---------");
				tabWayB = RequeteAPi.requeteBuilding(test[1], test[0], test[3], test[2]);
				RequeteAPi.affichageConsoleBuilding(tabWayB);
				int nbBat=tabWayB.length;
				for(int i=0;i<nbBat && tabWayB[i] != null ;i++) {
					// Vérifiez si une demande d'interruption a été reçue
		            if (Thread.currentThread().isInterrupted()) {
		                System.out.println("Interruption reçue. Sortie de la méthode run()...");
		                return;
		            }
		            
		            // Suite du traitement du thread
					List<Float> batiment = new ArrayList<Float>();
					for (int j=0; j<tabWayB[i].getLonLatNodes().length; j++) {
						batiment.add(tabWayB[i].getLonLatNodes()[j][0]);
						batiment.add(tabWayB[i].getLonLatNodes()[j][1]);
					}
					Float [] l = new Float[batiment.size()];
					batiment.toArray(l);
					traitsNodesRemplit(gcBuil,l,"batiment"); 

				}
				System.out.println("---------Fin-Building---------");

			}
		});
		//Thread BuildingOpti
		Thread buildingOpti = new Thread(new Runnable() {
			public void run() {
				//Building
				//System.out.println("---------Building---------");
				for (int a=0; a<4; a++) {
					// Vérifiez si une demande d'interruption a été reçue
		            if (Thread.currentThread().isInterrupted()) {
		                System.out.println("Interruption reçue. Sortie de la méthode run()...");
		                return;
		            }
		            
		            // Suite du traitement du thread
					float latInt = (test[1]-test[0])/2+test[0];
					float lonInt = (test[3]-test[2])/2+test[2];
					if (a==0) {
						System.out.println(test[1]);
						System.out.println(test[0]);
						System.out.println(test[3]);
						System.out.println(test[2]);
						System.out.println(lonInt);
						System.out.println(latInt);
						tabWayB = RequeteAPi.requeteBuilding(latInt, test[0], lonInt, test[2]);
					}
					if (a==1) {
						tabWayB = RequeteAPi.requeteBuilding(test[1], latInt, lonInt, test[2]);
					}
					if (a==2) {
						tabWayB = RequeteAPi.requeteBuilding(latInt, test[0], test[3], lonInt);
					}
					if (a==3) {
						tabWayB = RequeteAPi.requeteBuilding(test[1], latInt, test[3], lonInt);
					}


					//RequeteAPi.affichageConsoleBuilding(tabWayB);
					int nbBat=tabWayB.length;
					for(int i=0;i<nbBat && tabWayB[i] != null ;i++) {
						// Vérifiez si une demande d'interruption a été reçue
			            if (Thread.currentThread().isInterrupted()) {
			                System.out.println("Interruption reçue. Sortie de la méthode run()...");
			                return;
			            }
			            
			            // Suite du traitement du thread
						List<Float> batiment = new ArrayList<Float>();
						for (int j=0; j<tabWayB[i].getLonLatNodes().length; j++) {
							batiment.add(tabWayB[i].getLonLatNodes()[j][0]);
							batiment.add(tabWayB[i].getLonLatNodes()[j][1]);
						}
						Float [] l = new Float[batiment.size()];
						batiment.toArray(l);
						traitsNodesRemplit(gcBuil,l,"batiment"); 

					}
					System.out.println("---------Fin-Building---------");

				}
			}
		});

		//Thread Parc
		Thread parc = new Thread(new Runnable() {
			public void run() {

				Way [] tabWayP = RequeteAPi.requeteParc(test[0], test[1], test[2], test[3]);
				int nbWayP = Way.getNbWay();
				for(int i=0;i<nbWayP && tabWayP[i] != null ;i++) {
					List<Float> parc = new ArrayList<Float>();
					for (int j=0; j<tabWayP[i].getLonLatNodes().length; j++) {
						// Vérifiez si une demande d'interruption a été reçue
			            if (Thread.currentThread().isInterrupted()) {
			                System.out.println("Interruption reçue. Sortie de la méthode run()...");
			                return;
			            }
			            
			            // Suite du traitement du thread
						parc.add(tabWayP[i].getLonLatNodes()[j][0]);
						parc.add(tabWayP[i].getLonLatNodes()[j][1]);
					}
					Float [] l = new Float[parc.size()];
					parc.toArray(l);
					traitsNodesRemplit(gcParc,l,"parc"); 

				}
				System.out.println("---------Fin-Parc---------");
			}
		});


		//Thread Parking
		Thread parking = new Thread(new Runnable() {
			public void run() {
				Way [] tabWayP = RequeteAPi.requeteParking(test[0], test[1], test[2], test[3]);
				int nbWayP = Way.getNbWay();
				for(int i=0;i<nbWayP && tabWayP[i] != null ;i++) {
					List<Float> parking = new ArrayList<Float>();
					for (int j=0; j<tabWayP[i].getLonLatNodes().length; j++) {
						// Vérifiez si une demande d'interruption a été reçue
			            if (Thread.currentThread().isInterrupted()) {
			                System.out.println("Interruption reçue. Sortie de la méthode run()...");
			                return;
			            }
			            
			            // Suite du traitement du thread
						parking.add(tabWayP[i].getLonLatNodes()[j][0]);
						parking.add(tabWayP[i].getLonLatNodes()[j][1]);
					}
					Float [] l = new Float[parking.size()];
					parking.toArray(l);
					traitsNodesRemplit(gcZone,l,"parking"); 

				}
				System.out.println("---------Fin-Parking---------");
			}
		});


		//Thread Farmland
		Thread farmland = new Thread(new Runnable() {
			public void run() {
				Way [] tabWayF = RequeteAPi.requeteFarmland(test[0], test[1], test[2], test[3]);
				int nbWayF = Way.getNbWay();
				for(int i=0;i<nbWayF && tabWayF[i] != null ;i++) {
					List<Float> parking = new ArrayList<Float>();
					for (int j=0; j<tabWayF[i].getLonLatNodes().length; j++) {
						// Vérifiez si une demande d'interruption a été reçue
			            if (Thread.currentThread().isInterrupted()) {
			                System.out.println("Interruption reçue. Sortie de la méthode run()...");
			                return;
			            }
			            
			            // Suite du traitement du thread
						parking.add(tabWayF[i].getLonLatNodes()[j][0]);
						parking.add(tabWayF[i].getLonLatNodes()[j][1]);
					}
					Float [] l = new Float[parking.size()];
					parking.toArray(l);
					traitsNodesRemplit(gc,l,"farmland"); 

				}
				System.out.println("---------Fin-Farmland---------");
			}
		});

		//Thread Foret
		Thread forest = new Thread(new Runnable() {
			public void run() {
				Way [] tabWayF = RequeteAPi.requeteForest(test[0], test[1], test[2], test[3]);
				int nbWayF = Way.getNbWay();
				for(int i=0;i<nbWayF && tabWayF[i] != null ;i++) {
					List<Float> parking = new ArrayList<Float>();
					for (int j=0; j<tabWayF[i].getLonLatNodes().length; j++) {
						// Vérifiez si une demande d'interruption a été reçue
			            if (Thread.currentThread().isInterrupted()) {
			                System.out.println("Interruption reçue. Sortie de la méthode run()...");
			                return;
			            }
			            
			            // Suite du traitement du thread
						parking.add(tabWayF[i].getLonLatNodes()[j][0]);
						parking.add(tabWayF[i].getLonLatNodes()[j][1]);
					}
					Float [] l = new Float[parking.size()];
					parking.toArray(l);
					traitsNodesRemplit(gcParc,l,"forest"); 

				}
				System.out.println("---------Fin-Foret---------");
			}
		});

		//Thread Scrub
		Thread scrub = new Thread(new Runnable() {
			public void run() {
				Way [] tabWayS = RequeteAPi.requeteScrub(test[0], test[1], test[2], test[3]);
				int nbWayS = Way.getNbWay();
				for(int i=0;i<nbWayS && tabWayS[i] != null ;i++) {
					List<Float> scrub = new ArrayList<Float>();
					for (int j=0; j<tabWayS[i].getLonLatNodes().length; j++) {
						// Vérifiez si une demande d'interruption a été reçue
			            if (Thread.currentThread().isInterrupted()) {
			                System.out.println("Interruption reçue. Sortie de la méthode run()...");
			                return;
			            }
			            
			            // Suite du traitement du thread
						scrub.add(tabWayS[i].getLonLatNodes()[j][0]);
						scrub.add(tabWayS[i].getLonLatNodes()[j][1]);
					}
					Float [] l = new Float[scrub.size()];
					scrub.toArray(l);
					traitsNodesRemplit(gc,l,"scrub"); 

				}
				System.out.println("---------Fin-Foret---------");
			}
		});


		//Thread Zone Commercial
		Thread commercial = new Thread(new Runnable() {
			public void run() {
				Way [] tabWayC = RequeteAPi.requeteZoneCommercial(test[0], test[1], test[2], test[3]);
				int nbWayC = Way.getNbWay();
				//System.out.println(tabWayP[0].getIdWay());

				for(int i=0;i<nbWayC && tabWayC[i] != null ;i++) {
					// Vérifiez si une demande d'interruption a été reçue
		            if (Thread.currentThread().isInterrupted()) {
		                System.out.println("Interruption reçue. Sortie de la méthode run()...");
		                return;
		            }
		            
		            // Suite du traitement du thread
					getListe(tabWayC[i].getLonLatNodes());
					traitsZoneCommercial(gcZone,nbNodes,latLon);
				}
				System.out.println("---------Fin-ZoneCommercial---------");
			}
		});


		//Thread Residentiel
		Thread residential = new Thread(new Runnable() {
			public void run() {


				//Residentiel
				List<?> list = RequeteAPi.requeteResidential(test[1], test[0], test[3], test[2]);
				//nbWay = Water.getLonLatNodes().length;
				Relation [][] tabWayR = new Relation[list.size()][100000];
				list.toArray(tabWayR);

				for (int j=0; j<tabWayR.length ;j++) {

					//initialisation
					List<Float> latLon = new ArrayList<Float>();
					boolean [] visite = new boolean [tabWayR[j].length];
					for (int i1=0; i1<tabWayR[j].length; i1++) {
						visite[i1]=false;
						if ( tabWayR[j][i1] == null) {
							visite[i1]=true;
						}
					}
					boolean fin = true;
					int deb = 0;
					for (int i1=0; i1<tabWayR[j].length && fin==true; i1++) {
						if (tabWayR[j][i1]!=null) {
							if (tabWayR[j][i1].getRole()!=null) {
								if (!tabWayR[j][i1].getRole().equals("inner")) {
									deb = i1;
									fin = false;
								}
							}
							else if (tabWayR[j][i1].getRole()==null) {
								deb = i1;
								fin = false;
							}
						}
					}
					visite[deb]=true;
					int pro = deb;
					int sens = 1;

					for (int k=0; k<tabWayR[j][deb].getLonLatNodes().length; k++) {
						latLon.add( tabWayR[j][deb].getLonLatNodes()[k][0]);
						latLon.add( tabWayR[j][deb].getLonLatNodes()[k][1]);
					}
					//fin init
					float min = 9999F;
					while (fin!=true) {
						// Vérifiez si une demande d'interruption a été reçue
			            if (Thread.currentThread().isInterrupted()) {
			                System.out.println("Interruption reçue. Sortie de la méthode run()...");
			                return;
			            }
			            
			            // Suite du traitement du thread
						min = 9999F;
						fin=true;
						boolean quitter = false;

						//Verification de quelle membre prendre
						for (int i=0; i<tabWayR[j].length && tabWayR[j][i]!=null && tabWayR[j][i].getLonLatNodes().length>0 && quitter==false; i++) {
							if (visite[i]==false) {
								//System.out.println("t ; "+tabWayW[j].length);
								if (tabWayR[j][i].getRole()!=null) {
									if (tabWayR[j][i].getRole().equals("inner")) {
										visite[i]=true;
									}
								}

								if (sens == 1) {

									if (tabWayR[j][deb].getLonLatNodes()[tabWayR[j][deb].getLonLatNodes().length-1][0] == tabWayR[j][i].getLonLatNodes()[0][0]){
										pro = i;
										sens = 1;
										quitter = true;
									}
									else if ( tabWayR[j][deb].getLonLatNodes()[tabWayR[j][deb].getLonLatNodes().length-1][0] == tabWayR[j][i].getLonLatNodes()[tabWayR[j][i].getLonLatNodes().length-1][0]) {
										sens = 0;
										pro = i;
										quitter = true;
									}
									float diffx = tabWayR[j][deb].getLonLatNodes()[tabWayR[j][deb].getLonLatNodes().length-1][0] - tabWayR[j][i].getLonLatNodes()[0][0];
									if (diffx<0) {diffx=-diffx;}
									if (diffx<min) {
										min = diffx;
										pro = i;

									}
									diffx = tabWayR[j][deb].getLonLatNodes()[tabWayR[j][deb].getLonLatNodes().length-1][0] - tabWayR[j][i].getLonLatNodes()[tabWayR[j][i].getLonLatNodes().length-1][0];
									if (diffx<0) {diffx=-diffx;}
									if (diffx<min) {
										min = diffx;
										pro = i;

									}
								}
								else {
									float diffx = tabWayR[j][deb].getLonLatNodes()[0][0] - tabWayR[j][i].getLonLatNodes()[0][0];
									if (diffx<0) {diffx=-diffx;}
									if (diffx<min) {
										min = diffx;
										pro = i;

									}
									diffx = tabWayR[j][deb].getLonLatNodes()[0][0] - tabWayR[j][i].getLonLatNodes()[tabWayR[j][i].getLonLatNodes().length-1][0];
									if (diffx<0) {diffx=-diffx;}
									if (diffx<min) {
										min = diffx;
										pro = i;

									}
									if (tabWayR[j][deb].getLonLatNodes()[0][0] == tabWayR[j][i].getLonLatNodes()[0][0]){
										pro = i;
										sens = 1;
										quitter = true;
									}
									else if ( tabWayR[j][deb].getLonLatNodes()[0][0] == tabWayR[j][i].getLonLatNodes()[tabWayR[j][i].getLonLatNodes().length-1][0]) {
										sens = 0;
										pro = i;
										quitter = true;
									}
								}
								fin=false;
							}
						}
						if (deb==pro) {
							fin=true;
						}
						deb=pro;

						visite[deb]=true;
						//on rempli la liste
						if (sens == 0) {
							for (int k=tabWayR[j][deb].getLonLatNodes().length-1; k>=0 && fin==false; k=k-1) {
								latLon.add( tabWayR[j][deb].getLonLatNodes()[k][0]);
								latLon.add( tabWayR[j][deb].getLonLatNodes()[k][1]);
							}
						}
						else if (sens == 1) {
							for (int k=0; k<tabWayR[j][deb].getLonLatNodes().length && fin==false; k=k+1) {
								latLon.add( tabWayR[j][deb].getLonLatNodes()[k][0]);
								latLon.add( tabWayR[j][deb].getLonLatNodes()[k][1]);
							}
						}


					}

					Float [] l = new Float[latLon.size()];
					latLon.toArray(l);
					traitsNodesRemplit(gc,l,"residential"); 			


				}
			}
		});

		//Thread Industriel
		Thread industrial = new Thread(new Runnable() {
			public void run() {


				//Residentiel
				List<?> list = RequeteAPi.requeteIndustrial(test[1], test[0], test[3], test[2]);
				//nbWay = Water.getLonLatNodes().length;
				Relation [][] tabWayR = new Relation[list.size()][100000];
				list.toArray(tabWayR);

				for (int j=0; j<tabWayR.length ;j++) {

					//initialisation
					List<Float> latLon = new ArrayList<Float>();
					boolean [] visite = new boolean [tabWayR[j].length];
					for (int i1=0; i1<tabWayR[j].length; i1++) {
						visite[i1]=false;
						if ( tabWayR[j][i1] == null) {
							visite[i1]=true;
						}
					}
					boolean fin = true;
					int deb = 0;
					for (int i1=0; i1<tabWayR[j].length && fin==true; i1++) {
						if (tabWayR[j][i1]!=null) {
							if (tabWayR[j][i1].getRole()!=null) {
								if (!tabWayR[j][i1].getRole().equals("inner")) {
									deb = i1;
									fin = false;
								}
							}
							else if (tabWayR[j][i1].getRole()==null) {
								deb = i1;
								fin = false;
							}
						}
					}
					visite[deb]=true;
					int pro = deb;
					int sens = 1;

					for (int k=0; k<tabWayR[j][deb].getLonLatNodes().length; k++) {
						latLon.add( tabWayR[j][deb].getLonLatNodes()[k][0]);
						latLon.add( tabWayR[j][deb].getLonLatNodes()[k][1]);
					}
					//fin init
					float min = 9999F;
					while (fin!=true) {
						// Vérifiez si une demande d'interruption a été reçue
			            if (Thread.currentThread().isInterrupted()) {
			                System.out.println("Interruption reçue. Sortie de la méthode run()...");
			                return;
			            }
			            
			            // Suite du traitement du thread
						min = 9999F;
						fin=true;
						boolean quitter = false;

						//Verification de quelle membre prendre
						for (int i=0; i<tabWayR[j].length && tabWayR[j][i]!=null && tabWayR[j][i].getLonLatNodes().length>0 && quitter==false; i++) {
							if (visite[i]==false) {
								//System.out.println("t ; "+tabWayW[j].length);
								if (tabWayR[j][i].getRole()!=null) {
									if (tabWayR[j][i].getRole().equals("inner")) {
										visite[i]=true;
									}
								}

								if (sens == 1) {

									if (tabWayR[j][deb].getLonLatNodes()[tabWayR[j][deb].getLonLatNodes().length-1][0] == tabWayR[j][i].getLonLatNodes()[0][0]){
										pro = i;
										sens = 1;
										quitter = true;
									}
									else if ( tabWayR[j][deb].getLonLatNodes()[tabWayR[j][deb].getLonLatNodes().length-1][0] == tabWayR[j][i].getLonLatNodes()[tabWayR[j][i].getLonLatNodes().length-1][0]) {
										sens = 0;
										pro = i;
										quitter = true;
									}
									float diffx = tabWayR[j][deb].getLonLatNodes()[tabWayR[j][deb].getLonLatNodes().length-1][0] - tabWayR[j][i].getLonLatNodes()[0][0];
									if (diffx<0) {diffx=-diffx;}
									if (diffx<min) {
										min = diffx;
										pro = i;

									}
									diffx = tabWayR[j][deb].getLonLatNodes()[tabWayR[j][deb].getLonLatNodes().length-1][0] - tabWayR[j][i].getLonLatNodes()[tabWayR[j][i].getLonLatNodes().length-1][0];
									if (diffx<0) {diffx=-diffx;}
									if (diffx<min) {
										min = diffx;
										pro = i;

									}
								}
								else {
									float diffx = tabWayR[j][deb].getLonLatNodes()[0][0] - tabWayR[j][i].getLonLatNodes()[0][0];
									if (diffx<0) {diffx=-diffx;}
									if (diffx<min) {
										min = diffx;
										pro = i;

									}
									diffx = tabWayR[j][deb].getLonLatNodes()[0][0] - tabWayR[j][i].getLonLatNodes()[tabWayR[j][i].getLonLatNodes().length-1][0];
									if (diffx<0) {diffx=-diffx;}
									if (diffx<min) {
										min = diffx;
										pro = i;

									}
									if (tabWayR[j][deb].getLonLatNodes()[0][0] == tabWayR[j][i].getLonLatNodes()[0][0]){
										pro = i;
										sens = 1;
										quitter = true;
									}
									else if ( tabWayR[j][deb].getLonLatNodes()[0][0] == tabWayR[j][i].getLonLatNodes()[tabWayR[j][i].getLonLatNodes().length-1][0]) {
										sens = 0;
										pro = i;
										quitter = true;
									}
								}
								fin=false;
							}
						}
						if (deb==pro) {
							fin=true;
						}
						deb=pro;

						visite[deb]=true;
						//on rempli la liste
						if (sens == 0) {
							for (int k=tabWayR[j][deb].getLonLatNodes().length-1; k>=0 && fin==false; k=k-1) {
								latLon.add( tabWayR[j][deb].getLonLatNodes()[k][0]);
								latLon.add( tabWayR[j][deb].getLonLatNodes()[k][1]);
							}
						}
						else if (sens == 1) {
							for (int k=0; k<tabWayR[j][deb].getLonLatNodes().length && fin==false; k=k+1) {
								latLon.add( tabWayR[j][deb].getLonLatNodes()[k][0]);
								latLon.add( tabWayR[j][deb].getLonLatNodes()[k][1]);
							}
						}


					}

					Float [] l = new Float[latLon.size()];
					latLon.toArray(l);
					traitsNodesRemplit(gc,l,"industrial"); 			


				}

				System.out.println("---------Fin-Industrie---------");

				forest.start();
			}
		});

		//Water
		Thread water = new Thread(new Runnable() {
			public void run() {
				//Point d'eau
				List<?> list2 = RequeteAPi.requeteWater2(test[1], test[0], test[3], test[2]);
				//nbWay = Water.getLonLatNodes().length;
				Relation [][] tabWayW = new Relation[list2.size()][100000];
				list2.toArray(tabWayW);

				for (int j=0; j<tabWayW.length ;j++) {

					//initialisation
					List<Float> latLon = new ArrayList<Float>();
					boolean [] visite = new boolean [tabWayW[j].length];
					for (int i1=0; i1<tabWayW[j].length; i1++) {
						visite[i1]=false;
						if ( tabWayW[j][i1] == null) {
							visite[i1]=true;
						}
					}
					boolean fin = true;
					int deb = 0;
					for (int i1=0; i1<tabWayW[j].length && fin==true; i1++) {
						if (tabWayW[j][i1]!=null) {
							if (tabWayW[j][i1].getRole()!=null) {
								if (!tabWayW[j][i1].getRole().equals("inner")) {
									deb = i1;
									fin = false;
								}
							}
							else if (tabWayW[j][i1].getRole()==null) {
								deb = i1;
								fin = false;
							}
						}
					}
					visite[deb]=true;
					int pro = deb;
					int sens = 1;

					for (int k=0; k<tabWayW[j][deb].getLonLatNodes().length; k++) {
						latLon.add( tabWayW[j][deb].getLonLatNodes()[k][0]);
						latLon.add( tabWayW[j][deb].getLonLatNodes()[k][1]);
					}
					//fin init
					float min = 9999F;
					while (fin!=true) {
						// Vérifiez si une demande d'interruption a été reçue
			            if (Thread.currentThread().isInterrupted()) {
			                System.out.println("Interruption reçue. Sortie de la méthode run()...");
			                return;
			            }
			            
			            // Suite du traitement du thread
						min = 9999F;
						fin=true;
						boolean quitter = false;

						//Verification de quelle membre prendre
						for (int i=0; i<tabWayW[j].length && tabWayW[j][i]!=null && tabWayW[j][i].getLonLatNodes().length>0 && quitter==false; i++) {
							if (visite[i]==false) {
								if (tabWayW[j][i].getRole()!=null) {
									if (tabWayW[j][i].getRole().equals("inner")) {
										visite[i]=true;
									}
								}

								if (sens == 1) {

									if (tabWayW[j][deb].getLonLatNodes()[tabWayW[j][deb].getLonLatNodes().length-1][0] == tabWayW[j][i].getLonLatNodes()[0][0]){
										pro = i;
										sens = 1;
										quitter = true;
									}
									else if ( tabWayW[j][deb].getLonLatNodes()[tabWayW[j][deb].getLonLatNodes().length-1][0] == tabWayW[j][i].getLonLatNodes()[tabWayW[j][i].getLonLatNodes().length-1][0]) {
										sens = 0;
										pro = i;
										quitter = true;
									}
									float diffx = tabWayW[j][deb].getLonLatNodes()[tabWayW[j][deb].getLonLatNodes().length-1][0] - tabWayW[j][i].getLonLatNodes()[0][0];
									if (diffx<0) {diffx=-diffx;}
									if (diffx<min) {
										min = diffx;
										pro = i;

									}
									diffx = tabWayW[j][deb].getLonLatNodes()[tabWayW[j][deb].getLonLatNodes().length-1][0] - tabWayW[j][i].getLonLatNodes()[tabWayW[j][i].getLonLatNodes().length-1][0];
									if (diffx<0) {diffx=-diffx;}
									if (diffx<min) {
										min = diffx;
										pro = i;

									}
								}
								else {
									float diffx = tabWayW[j][deb].getLonLatNodes()[0][0] - tabWayW[j][i].getLonLatNodes()[0][0];
									if (diffx<0) {diffx=-diffx;}
									if (diffx<min) {
										min = diffx;
										pro = i;

									}
									diffx = tabWayW[j][deb].getLonLatNodes()[0][0] - tabWayW[j][i].getLonLatNodes()[tabWayW[j][i].getLonLatNodes().length-1][0];
									if (diffx<0) {diffx=-diffx;}
									if (diffx<min) {
										min = diffx;
										pro = i;

									}
									if (tabWayW[j][deb].getLonLatNodes()[0][0] == tabWayW[j][i].getLonLatNodes()[0][0]){
										pro = i;
										sens = 1;
										quitter = true;
									}
									else if ( tabWayW[j][deb].getLonLatNodes()[0][0] == tabWayW[j][i].getLonLatNodes()[tabWayW[j][i].getLonLatNodes().length-1][0]) {
										sens = 0;
										pro = i;
										quitter = true;
									}
								}
								fin=false;
							}
						}
						if (deb==pro) {
							fin=true;
						}
						deb=pro;

						visite[deb]=true;
						//on rempli la liste
						if (sens == 0) {
							for (int k=tabWayW[j][deb].getLonLatNodes().length-1; k>=0 && fin==false; k=k-1) {
								latLon.add( tabWayW[j][deb].getLonLatNodes()[k][0]);
								latLon.add( tabWayW[j][deb].getLonLatNodes()[k][1]);
							}
						}
						else if (sens == 1) {
							for (int k=0; k<tabWayW[j][deb].getLonLatNodes().length && fin==false; k=k+1) {
								latLon.add( tabWayW[j][deb].getLonLatNodes()[k][0]);
								latLon.add( tabWayW[j][deb].getLonLatNodes()[k][1]);
							}
						}


					}

					Float [] l = new Float[latLon.size()];
					latLon.toArray(l);
					traitsNodesRemplit(gcEau,l,"outer"); 			


				}
				System.out.println("---------Fin-PointD'eau-Outer---------");

				//Points d'eau inner
				List<?> list3 = RequeteAPi.requeteWater2(test[1], test[0], test[3], test[2]);
				//nbWay = Water.getLonLatNodes().length;
				tabWayW = new Relation[list3.size()][10000];
				list3.toArray(tabWayW);

				List<Float[]> inner = new ArrayList<Float[]>();
				for (int j=0; j<tabWayW.length && tabWayW[j][0] != null ;j++) {
					for(int i=0; i<tabWayW[j].length && tabWayW[j][i] != null ;i++) {
						if (tabWayW[j][i].getRole()!=null) {
							if (tabWayW[j][i].getRole().equals("inner")) {
								Float [] l = new Float[tabWayW[j][i].getLonLatNodes().length*2];
								int m=0;
								for (int k=0; k<tabWayW[j][i].getLonLatNodes().length; k++) {
									// Vérifiez si une demande d'interruption a été reçue
						            if (Thread.currentThread().isInterrupted()) {
						                System.out.println("Interruption reçue. Sortie de la méthode run()...");
						                return;
						            }
						            
						            // Suite du traitement du thread
									l[m] = tabWayW[j][i].getLonLatNodes()[k][0];
									l[m+1] = tabWayW[j][i].getLonLatNodes()[k][1];
									m=m+2;

								}
								inner.add(l);
							}
						}

					}


					for (int i=0; i<inner.size(); i++) {
						traitsNodesRemplit(gcEau,inner.get(i),"inner"); 
					}
				}
				System.out.println("---------Fin-PointD'eau-Inner---------");
			}
		});

		//On arrete tout les anciens threads
		for (Thread thread : threads) {
		    thread.interrupt();
		}

		
		//On commence à charger l'affichage si la zone à bien été trouvée
		if ( test[0]!=0 && test[1]!=0 && test[2]!=0 && test[3]!=0) {
			threads.add(fond);fond.start();
			threads.add(route);route.start();
			dimAff = convertionCanvas();
			if(dimAff[0]<=0.15 && dimAff[1]<=0.15) {threads.add(building);building.start();}
			else {threads.add(buildingOpti);buildingOpti.start();}
			threads.add(parc);parc.start();
			threads.add(parking);parking.start();
			threads.add(residential);residential.start();
			threads.add(industrial);industrial.start();
			threads.add(water);water.start();
			threads.add(scrub);scrub.start();
			threads.add(commercial);commercial.start();
			threads.add(farmland);farmland.start();
			
			
		}
		/*System.out.println(minLat);
	System.out.println(maxLat);
	System.out.println(minLon);
	System.out.println(maxLon);*/
	}

	/*
	public static void zoomer(GraphicsContext gc,Canvas canvas,boolean z) {
		float [] t = {minLat,maxLat,minLon,maxLon};
		float [] t2 = t;
		if (z == false) {
			//dezoom
			t2[0] =(float) (t[0]-t[0]*0.25);
			t2[1]=(float) (t[1]+t[1]*0.25);
			t2[2]=(float) (t[2]-t[2]*0.25);
			t2[3]=(float) (t[3]+t[3]*0.25);
		}
		else if (z == true) {
			//zoom 

			t2[0] =(float) (t[0]+t[0]*0.25);
			t2[1]=(float) (t[1]-t[1]*0.25);
			t2[2]=(float) (t[2]+t[2]*0.25);
			t2[3]=(float) (t[3]-t[3]*0.25);		
		}
		resize(gc,);

	}*/





	//Permet de zoomer
	public static float zoomer(boolean z) {

		if (z== false && zoomMax-0.5>=1) {
			zoomMax=zoomMax/2;
			return (zoomMax);
		}
		else if (z == true) {
			zoomMax=zoomMax*2;
			return (zoomMax);
		}
		return(zoomMax);
	}


}
