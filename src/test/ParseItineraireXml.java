package test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class ParseItineraireXml {

	public static boolean verifSiFichierXmlExiste(String nameFile) {

		File dir  = new File("./itineraire");
		File[] liste = dir.listFiles();
		if (liste!=null) {
			for(File item : liste){
				if(item.isFile())
				{ 
					String name = item.getName();
					if ( name.equals(nameFile) == true ) {
						return true;
					}
				} 
			}
		}
		return false;
	}

	public static void creerFichierXmlPourItineraire(Itineraire itineraire) {

		String nameNewFile = "itineraire-"+itineraire.getDepart()[1]+"-"+itineraire.getDepart()[0]+"-"+itineraire.getArrivee()[1]+"-"+itineraire.getArrivee()[0]+"-"+itineraire.getMtp()+".xml";

		PrintWriter fichier;
		try {
			fichier = new PrintWriter("./itineraire/"+nameNewFile, "UTF-8");
			fichier.println("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			fichier.println("<!DOCTYPE xml>");
			fichier.println("	<itineraire ref=\"1\" >");
			fichier.println("		<depart lat=\""+itineraire.getDepart()[1]+"\" lon=\""+itineraire.getDepart()[0]+"\" />");
			fichier.println("		<arrivee lat=\""+itineraire.getArrivee()[1]+"\" lon=\""+itineraire.getArrivee()[0]+"\" />");
			fichier.println("		<sdepart nom=\""+itineraire.getSdepart()+"\" />");
			fichier.println("		<sarrivee nom=\""+itineraire.getSArrivee()+"\" />");
			fichier.println("		<tag mtp=\""+itineraire.getMtp()+"\" temps=\""+itineraire.getTemps()+"\" taille=\""+itineraire.getTaille()+"\" />");
			
			for (int i=0; i<itineraire.getLonLatNodes().length; i++) {
				fichier.println("		<nd ref=\""+i+"\" lat=\""+itineraire.getLonLatNodes()[i][1]+"\" lon=\""+itineraire.getLonLatNodes()[i][0]+"\" />");
			}
			
			for (int i=0; i<itineraire.getEtapes().length; i++) {
				fichier.println("		<etape ref=\""+i+"\" info=\""+itineraire.getEtapes()[i]+"\" />");
			}
			
			fichier.println("	</itineraire>");
			fichier.close();
		} catch (FileNotFoundException | UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	

	public static Itineraire requeteXmlItineraire(String name) {

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder loader = null;
		try {
			loader = factory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Document document = null;
		try {
			document = loader.parse("./itineraire/"+name);

		} catch (SAXException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		final Element racine = document.getDocumentElement();

		final NodeList racineNoeuds = racine.getChildNodes();
		final int nbRacineNoeuds = racine.getChildNodes().getLength();
		// tabBuilding = tableau avec toute les routes � afficher.

		int k=0;
		
		float [] depart = new float[2];
		String[] etapes = null;
		float [] arrivee = new float[2];
		float [][] itineraire = null;
		int taille = 0;
		float temps = 0.0F;
		int mtp = 0;
		
		String sarrivee ="";
		String sdepart ="";
		
		for (int i = 0; i<nbRacineNoeuds; i++) 
		{

			if(racineNoeuds.item(i).getNodeType() == Node.ELEMENT_NODE) 
			{

				final Element Itineraire = (Element) racine.getChildNodes();


				//si j'ai un id je peux continuer sinon je passe � l'element suivant.
				if(Itineraire.getNodeName()=="itineraire") {

					final NodeList node =  Itineraire.getElementsByTagName("nd"); //recupere les nodes contenue dans le Building 
					final int nbNode = node.getLength();
					
					final NodeList etape =  Itineraire.getElementsByTagName("etape"); //recupere les nodes contenue dans le Building 
					final int nbEtape = etape.getLength();

					float [][]  tabNode = new float[nbNode][2];
					String []  tabEtape = new String[nbEtape];

					//boucle pour parcourir toute les balises nd de chaque itineraire
					for (int j = 0; j<nbNode; j++) 
					{
						final Element idNode = (Element) node.item(j);
						//  System.out.println(idNode.getNodeName()); //affiche le nom de la balise ici nd
						//System.out.println("ID : " + idNode.getAttribute("ref")); // affiche ce qui est contenu dans l'attribut ref (id) de la balise nd 

						tabNode[j][0] = Float.parseFloat(idNode.getAttribute("lon")); //recupere la lat de la node
						tabNode[j][1] = Float.parseFloat(idNode.getAttribute("lat")); // recupere la lon de la node 

					}
					itineraire = tabNode;
					
					//boucle pour parcourir toute les balises etape de chaque itineraire
					for (int j = 0; j<nbEtape; j++) 
					{
						final Element idEtape = (Element) etape.item(j);
						//  System.out.println(idNode.getNodeName()); //affiche le nom de la balise ici nd
						//System.out.println("ID : " + idNode.getAttribute("ref")); // affiche ce qui est contenu dans l'attribut ref (id) de la balise nd 

						tabEtape[j] = idEtape.getAttribute("info"); //recupere la lat de la node
					}
					etapes = tabEtape;

					//récuperer tous les tags
					final NodeList tag = Itineraire.getElementsByTagName("tag");
					final int nbTag = tag.getLength();

					for(int v= 0;v<nbTag;v++) {
						final Element attrTag = (Element) tag.item(v);
						taille = Integer.valueOf(attrTag.getAttribute("taille"));
						mtp = Integer.valueOf(attrTag.getAttribute("mtp"));
						temps = Float.parseFloat(attrTag.getAttribute("temps"));
						
					}
					
					//récuperer coordonnées arrivée
					final NodeList arr = Itineraire.getElementsByTagName("arrivee");
					final int nbArr = tag.getLength();

					for(int v= 0;v<nbArr;v++) {
						final Element attrArr = (Element) arr.item(v);
						arrivee[0] = Float.parseFloat(attrArr.getAttribute("lon"));
						arrivee[1] = Float.parseFloat(attrArr.getAttribute("lat"));
					}
					
					//récuperer coordonnées départ
					final NodeList dep = Itineraire.getElementsByTagName("depart");
					final int nbDep = dep.getLength();

					for(int v= 0;v<nbDep;v++) {
						final Element attrDep = (Element) dep.item(v);
						depart[0] = Float.parseFloat(attrDep.getAttribute("lon"));
						depart[1] = Float.parseFloat(attrDep.getAttribute("lat"));
					}
					
					//récuperer nom arrivée
					final NodeList sarr = Itineraire.getElementsByTagName("sarrivee");
					final int snbArr = tag.getLength();

					for(int v= 0;v<snbArr;v++) {
						final Element attrArr = (Element) sarr.item(v);
						sarrivee = attrArr.getAttribute("nom");
					}
					
					//récuperer nom départ
					final NodeList sdep = Itineraire.getElementsByTagName("sdepart");
					final int snbDep = dep.getLength();

					for(int v= 0;v<snbDep;v++) {
						final Element attrDep = (Element) sdep.item(v);
						sdepart = attrDep.getAttribute("nom");
					}

					k=k+1;
				}
			}
		}   	

		Itineraire res = new Itineraire(depart, etapes, arrivee,itineraire, taille, temps, mtp);
		res.setSdepart(sdepart);
		res.setSArrivee(sarrivee);
		return res;



	}
}
