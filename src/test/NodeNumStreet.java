package test;


//sert uniquement pour liée les numeros de batiments aux routes 
public class NodeNumStreet {
	
	private float[] lonLatNodes;
	
	private String num;
	
	private String rue;

	public NodeNumStreet() {}
	public float[] getLonLatNodes() {
		return lonLatNodes;
	}

	public void setLonLatNodes(float[] lonLatNodes) {
		this.lonLatNodes = lonLatNodes;
	}

	public String getNum() {
		return num;
	}

	public void setNum(String num) {
		this.num = num;
	}

	public String getRue() {
		return rue;
	}

	public void setRue(String rue) {
		this.rue = rue;
	}
}
