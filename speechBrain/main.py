import http.server
import socketserver

from speechbrain.pretrained import EncoderASR
from speechbrain.pretrained import SpeakerRecognition
import torchaudio
import pyaudio
import wave
import os

PORT = 8000


def getfixture(param):
    pass


def fonctionecoute():
    # parametre de l'audio en entrée
    CHUNK = 1024
    FORMAT = pyaudio.paInt16
    CHANNELS = 1
    RATE = 16000
    RECORD_SECONDS = 5
    WAVE_OUTPUT_FILENAME = "voix/sortie.wav"

    # init du pyaudio module
    p = pyaudio.PyAudio()

    # ouverture du flux audio

    stream = p.open(format=FORMAT,
                    channels=CHANNELS,
                    rate=RATE,
                    input=True,
                    frames_per_buffer=CHUNK)

    # commencement de l'enregistrement
    print("Enregistrement......")
    frames = []

    for i in range(0, int(RATE / CHUNK * RECORD_SECONDS)):
        data = stream.read(CHUNK)
        frames.append(data)

    # Arret de lecture du flux

    stream.stop_stream()
    stream.close()
    p.terminate()

    # sauvegarde du flux

    wf = wave.open(WAVE_OUTPUT_FILENAME, 'wb')
    wf.setnchannels(CHANNELS)
    wf.setsampwidth(p.get_sample_size(FORMAT))
    wf.setframerate(RATE)
    wf.writeframes(b''.join(frames))
    wf.close()

    print("Done.")
    tmpdir = getfixture("tmpdir")
    verification = SpeakerRecognition.from_hparams(source="speechbrain/spkrec-ecapa-voxceleb",
                                                   savedir=tmpdir, )

    chemin = 'C:/Users/gadre/Documents/cour_L3/Projet gloogle/gloogle-groupe-1-s2/speechBrain/voix'

    nom_fichier = os.listdir(chemin)

    print(nom_fichier)
    signalCompare, fs = torchaudio.load("voix/sortie.wav")
    for i in nom_fichier:
        signal, fs = torchaudio.load("voix/" + i)
        val, val1 = verification.verify_batch(signal, signalCompare)
        if i != "sortie.wav" and val1[0] != False:
            print("bonjour " + i)
        
            name = i.split('.')[0]
            return "bonjour " + name
    return "Utilisateur non reconnu"

class RequestHandler(http.server.BaseHTTPRequestHandler):
    def do_GET(self):
        if self.path != '/favicon.ico' :
            self.send_response(200)
            self.send_header('Content-type', 'text/html')
            self.end_headers()
            message = fonctionecoute()  # Appeler la fonction
            self.wfile.write(bytes(message, "utf8"))


with socketserver.TCPServer(("", PORT), RequestHandler) as httpd:
    print("Serveur en cours d'exécution sur le port :", PORT)
    httpd.serve_forever()
